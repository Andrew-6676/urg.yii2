<?php

namespace common\components;

use Yii;
use yii\db\Query;
use yii\db\QueryBuilder;

class myModel extends \yii\db\ActiveRecord
{
	//private $defaultOrder = 'np';

	// из GET['filter'] вида 'поле=значение,поле2=значение2....' формируем запрос к БД
	public static function filter($params)
	{
		$parsedParams = static::parseParams($params);
		//print_r($parsedParams);

		return static::createQuery($parsedParams);
	}

	/*-------------------------------------------------------------------*/
	static function parseParams($params) {
		// парсим запрос вида ?filter[]=family==Иванов;family=@Ша&filter[]=id_unit==1&order=imya
		// отдельные filter[] соединяются and, всё что внутри filter[] - or
		$and = array();
		// условия, которые соединяем через and
		foreach ($params as $param) {
			$or = [];
			// разбиваем условия по , (это будут условия, которые соединяем через or)
			$p = explode(';',$param);
			foreach ($p as $condition) {
				// выделяем из условие поле, операцию, значение
				$res = preg_split('/(=([=@><]*))/', $condition, -1, PREG_SPLIT_DELIM_CAPTURE);
				$or[] = $res;
			}
			$and[] = $or;
		}

		return($and);
	}

	/*-------------------------------------------------------------------*/
	public static function find() {
		$sql = parent::find();
		return $sql;
	}
	/*-------------------------------------------------------------------*/
	// формируем запрос
	protected static function createQuery($params) {
//		$operator = [
//			'=@' => ' like ', // %3D@
//			'==' => '='     //%3D%3D
//		];

		$where = ['and'];
		// перебираем массив с параметрами - первый уровень будет связываться and
		foreach ($params as $_and) {
			$or = array();
			$or[] = 'or';
			// перебираем второй уровень - будет связываться or
			foreach ($_and as $_or) {
				// получаем массив вида ['or', $c1, $c2];
				$or[] = static::prepareCondition($_or);
			}
			// получаем массив вида ['and', $or1, $or2]
			$where[] = $or;
		}

		//print_r($where);

		$query = static::find();

//		$c1 = ['like', 'family', 'Ш%', false];
//		$c2 = ['family'=>'Иванов'];
//		$or1 = ['or', $c2, $c1];
//
//		$c1 = ['id_unit'=>[1,2]];
//		$c2 = ['id_post'=>5];
//		$or2 = ['or', $c1, $c2];
//
//		$query->where(['and', $or1, $or2]);

		//echo "\n----------etalon----------\n";
		//print_r(['and', $or1, $or2]);
		//print_r($query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql);

		$query->where($where);

		//print_r($query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql);
		return $query;
	}

	/** формируем условие для вставки в запрос
	 *	из [field, ==/=@/=>/=</=<=/=>=, операнд, value]
	 *   получаем [field=>value] или [like, field. value, false] или ['>', field, value]
	 */
	private static function prepareCondition($condition) {
		//echo "\n----------------------\n";
		//print_r($condition);
		$res = array();
		switch ($condition[2]) {
			case '@':
				$res[] = 'like';
				$res[] = $condition[0];
				$res[] = $condition[3];
				$res[] = false;
				break;
			case '=':
				if ($condition[3]=='null' || $condition[3]=='NULL') {
					$res[] = 'is';
					$res[] = $condition[0];
					$res[] = null;
				} else {
					$res[$condition[0]] = $condition[3];
				}
				break;
			default:
				$res[] = $condition[2];
				$res[] = $condition[0];
				$res[] = $condition[3];
		}
		//print_r($res);
		return $res ;
	}
	/*-------------------------------------------------------------------*/
	/*-------------------------------------------------------------------*/


	/*-------------------------------------------------------------------*/

}