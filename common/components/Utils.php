<?php

namespace common\components;

use app\models\ngas\ExRate;
use app\models\ngas\CaloricVal;
use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;

class Utils extends Component {
	public static $ru_month = array(
								 '1'=>'январь',
								 '2'=>'февраль',
								 '3'=>'март',
								 '4'=>'апрель',
								 '5'=>'май',
								 '6'=>'июнь',
								 '7'=>'июль',
								 '8'=>'август',
								 '9'=>'сентябрь',
								 '10'=>'октябрь',
								 '11'=>'ноябрь',
								 '12'=>'декабрь');
	public static $ru_month2 = array(
								'1'=>'январь',
								'01'=>'январь',
								'2'=>'февраль',
								'02'=>'февраль',
								'3'=>'март',
								'03'=>'март',
								'4'=>'апрель',
								'04'=>'апрель',
								'5'=>'май',
								'05'=>'май',
								'6'=>'июнь',
								'06'=>'июнь',
								'7'=>'июль',
								'07'=>'июль',
								'8'=>'август',
								'08'=>'август',
								'9'=>'сентябрь',
								'09'=>'сентябрь',
								'10'=>'октябрь',
								'11'=>'ноябрь',
								'12'=>'декабрь');
	public static $ru_month3 = array(
								'1'=>'января',
								'01'=>'января',
								'2'=>'февраля',
								'02'=>'февраля',
								'3'=>'марта',
								'03'=>'марта',
								'4'=>'апреля',
								'04'=>'апреля',
								'5'=>'мая',
								'05'=>'мая',
								'6'=>'июня',
								'06'=>'июня',
								'7'=>'июля',
								'07'=>'июля',
								'8'=>'августа',
								'08'=>'августа',
								'9'=>'сентября',
								'09'=>'сентября',
								'10'=>'октября',
								'11'=>'ноября',
								'12'=>'декабря');
	public static $ru_month4 = array(
								'1'=>'январе',
								'01'=>'январе',
								'2'=>'феврале',
								'02'=>'феврале',
								'3'=>'марте',
								'03'=>'марте',
								'4'=>'апреле',
								'04'=>'апреле',
								'5'=>'мае',
								'05'=>'мае',
								'6'=>'июне',
								'06'=>'июне',
								'7'=>'июле',
								'07'=>'июле',
								'8'=>'августе',
								'08'=>'августе',
								'9'=>'сентябре',
								'09'=>'сентябре',
								'10'=>'октябре',
								'11'=>'ноябре',
								'12'=>'декабре');
	public static $ru_dow_arr = array(
		4=>'Понедельник',//4  1
		3=>'Вторник',   //3   2
		2=>'Среда',     //2   3
		1=>'Четверг',   //1   4
		0=>'Пятница',   //0   5
		6=>'Суббота',   //6   6
		5=>'Воскресенье',//5  7
	);

/*------------------------------------------------------------------------*/
	public static function ru_dow($curr_date) {
		return self::$ru_dow_arr[abs(jddayofweek(strtotime($curr_date)))];
	}
/*------------------------------------------------------------------------*/
	public static function is_weekend($curr_date) {
		$dow = jddayofweek(strtotime($curr_date));
		return ((array_search($curr_date, ['2015-12-25','2016-01-01'])!==false) || ($dow==6 || $dow==5)) ;

	}
	/*------------------------------------------------------------------------*/
	public static function getMonthName($month)
	{
		return self::$ru_month2[$month];
	}
/*------------------------------------------------------------------------*/
	public static function getMonthList()
	{
		return self::$ru_month;
	}
/*------------------------------------------------------------------------*/
	public static function print_r($var)
	{
		echo '<pre>'."\n";
		print_r($var);
		echo '</pre>';
	}
/*------------------------------------------------------------------------*/
	public static function format_date($date, $full='') {
		$d = explode('-', $date);
		if ($full=='full') {
			return $d[2].' '.self::$ru_month3[$d[1]].' '.$d[0].'г.';
		} else {
			return date('d.m.Y', mktime(0, 0, 0, $d[1], $d[2], $d[0]));
		}
	}
/*------------------------------------------------------------------------*/
	/**
	 * Возвращает сумму прописью
	 * @author runcore
	 * @uses morph(...)
	 */
	public static function num2str ($num){
			$nul='ноль';
		$ten=array(
			array('','один','два','три','четыре','пять','шесть','семь', 'восемь','девять'),
			array('','одна','две','три','четыре','пять','шесть','семь', 'восемь','девять'),
		);
		$a20=array('десять','одиннадцать','двенадцать','тринадцать','четырнадцать' ,'пятнадцать','шестнадцать','семнадцать','восемнадцать','девятнадцать');
		$tens=array(2=>'двадцать','тридцать','сорок','пятьдесят','шестьдесят','семьдесят' ,'восемьдесят','девяносто');
		$hundred=array('','сто','двести','триста','четыреста','пятьсот','шестьсот', 'семьсот','восемьсот','девятьсот');
		$unit=array( // Units
			array('копейка' ,'копейки' ,'копеек',	 1),
			array('рубль'   ,'рубля'   ,'рублей'    ,0),
			array('тысяча'  ,'тысячи'  ,'тысяч'     ,1),
			array('миллион' ,'миллиона','миллионов' ,0),
			array('миллиард','милиарда','миллиардов',0),
		);
		//
		list($rub,$kop) = explode('.',sprintf("%015.2f", floatval($num)));
		$out = array();
		if (intval($rub)>0) {
			foreach(str_split($rub,3) as $uk=>$v) { // by 3 symbols
				if (!intval($v)) continue;
				$uk = sizeof($unit)-$uk-1; // unit key
				$gender = $unit[$uk][3];
				list($i1,$i2,$i3) = array_map('intval',str_split($v,1));
				// mega-logic
				$out[] = $hundred[$i1]; # 1xx-9xx
				if ($i2>1) $out[]= $tens[$i2].' '.$ten[$gender][$i3]; # 20-99
				else $out[]= $i2>0 ? $a20[$i3] : $ten[$gender][$i3]; # 10-19 | 1-9
				// units without rub & kop
				if ($uk>1) $out[]= self::morph($v,$unit[$uk][0],$unit[$uk][1],$unit[$uk][2]);
			} //foreach
		}
		else $out[] = $nul;
		$out[] = self::morph(intval($rub), $unit[1][0],$unit[1][1],$unit[1][2]); // rub
		//$out[] = $kop.' '.morph($kop,$unit[0][0],$unit[0][1],$unit[0][2]); // kop

		return trim(preg_replace('/ {2,}/', ' ', join(' ',$out)));
	}

	/**
	 * Склоняем словоформу
	 * @ author runcore
	 */
	public static function morph($n, $f1, $f2, $f5) {
		$n = abs(intval($n)) % 100;
		if ($n>10 && $n<20) return $f5;
		$n = $n % 10;
		if ($n>1 && $n<5) return $f2;
		if ($n==1) return $f1;
		return $f5;
	}
/*------------------------------------------------------------------------*/
	public static function countCena($cena) {
		$koeff = self::getCaloricval(Yii::$app->session['workdate']);
		//return var_dump($koeff);
		//на последний день предыдущего месяца
		$date = mktime(0, 0, 0, Yii::$app->formatter->asDate(Yii::$app->session['workdate'], 'MM'), 0, Yii::$app->formatter->asDate(Yii::$app->session['workdate'], 'y'));
		return $cena['cena']*(self::getRate(date('Y-m-d', $date))/self::getRate())*($koeff/7900) * (1+$cena['nds']/100);
		//return self::getRate();
	}
/*------------------------------------------------------------------------*/
	public static function getRate($date=null) {

		if ($date) {
			$r = ExRate::find()->where('r_date=:date', ['date' => $date])->one()->rate;
			return $r ? $r : -1;
		} else {
			return 10950;
		}
	}
/*------------------------------------------------------------------------*/
	public static function getCaloricval($date=null) {
		if ($date) {
			$cv = CaloricVal::find()->where('c_date like :date', ['date' => Yii::$app->formatter->asDate($date, 'y-MM-%').''])->one();
			return $cv ? $cv->c_value : -1;
		} else {
			return 8128;
		}
	}
/*------------------------------------------------------------------------*/
	public static function getExRateFromNBRB($date) {
		$auth = base64_encode('andrew:flatron');

		$aContext = array(
			'http' => array(
				'proxy' => 'tcp://192.168.100.1:3128',
				'request_fulluri' => true,
				'header' => "Proxy-Authorization: Basic $auth",
			),
		);
		$cxContext = stream_context_create($aContext);

		$sFile = file_get_contents("http://www.nbrb.by/Services/XmlExRates.aspx?ondate=".Yii::$app->formatter->asDate($date, 'MM/dd/y'), False, $cxContext);

		preg_match_all('/<Currency.*Id="145">.*Rate>(.*)<\/Rate.*<\/Currency>/Ums', $sFile, $arr);
		return $arr[1][0];
	}
/*------------------------------------------------------------------------*/

/*------------------------------------------------------------------------*/
/*------------------------------------------------------------------------*/
/*------------------------------------------------------------------------*/
/*------------------------------------------------------------------------*/
/*------------------------------------------------------------------------*/
}

