<?php

namespace common\components;

use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;

class ldap extends Component {

	private $ldapserver = "192.168.100.1";
	private $binddn = "uid=perladmin,ou=Vitebskoblgas,o=gas";
	private $passw = "perladmin";
	public  $link;
	public  $mess;
	public  $user = false;
/*------------------------------------------------------------------------*/
	public function __construct()
	{
		$this->link = ldap_connect($this->ldapserver);
		if (!$this->link){
			$this->mess[] = "error ldap_connect";
			return false;
		}
		$this->mess[] = "ldap_connect successfully";
		$this->user = new \stdclass;
		return true;
	}
/*------------------------------------------------------------------------*/
/*------------------------------------------------------------------------*/
	public function authfromldap($uid, $conf)
	{
		//	print_r($conf);
//		$ldapserver = "192.168.100.1";
//		$binddn = "uid=perladmin,ou=Vitebskoblgas,o=gas";
//		$bindpassw = "perladmin";
//		$link = ldap_connect($ldapserver);

		ldap_set_option($this->link, LDAP_OPT_PROTOCOL_VERSION, 3);
		if (!ldap_bind($this->link, $this->binddn, $this->passw)){
			$this->mess[] = "error ldap_bind";
			return false;
		}
		$this->mess[] = 'ldap_bind successfully';

		if (!$res = ldap_search($this->link, "o=gas", "(uid=$uid)", array('dn', 'confValue', 'cn'))){
			$this->mess = "Error in search";
			return false;
		}
		$this->mess[] = 'ldap_search successfully';

		$info = ldap_get_entries($this->link, $res);
		if ($info['count'] == 0) return false;
		//\app\components\Utils::print_r($info);

		for($i=0; $i<$info['count']; $i++){

			$this->user = new \stdclass;
			//$this->user->info[$i] = $info[$i];
			if (isset($info[$i]["confvalue"])){
				for ($j=0; $j<$info[$i]['confvalue']['count']; $j++){
					if (strstr($info[$i]['confvalue'][$j], $conf) !== false){
						//нашли confValue
						//Utils::print_r($info);
						$mas = explode("_", $info[$i]['confvalue'][$j]);
						//$this->user->role = 'user';
						//if (count($mas) == 2)
						//	$this->user->role = $mas[1];
						$this->user->roles = $mas;
						$this->user->name = $uid;
						$this->user->username = $info[$i]['cn'][0];
						$this->user->areas = $this->getareafromldap($info[$i]['dn']);
						$this->user->area = (int)$this->user->areas[0];
						$this->user->dn = $info[$i]['dn'];
						//return $this->user;
					}
				}
			}
		}
		return false;
	}
/*------------------------------------------------------------------------*/
	public function getareafromldap($dn) {
		$arraydn = ldap_explode_dn($dn,1);
	//echo '$arraydn='.print_r($arraydn,true)."\n";
		// если count=3, значит самая вышестоящая организация - есть только подчинённые
		// если count=4 - есть вышестоящая (не интересно), но так же могут быть подчинённые
		// если count=5 - есть вышестоящая (не интересно), на данный момент подчинённых нету
		if ($arraydn['count'] == 3)
		{
			return 28;
		}

		$org = $arraydn[2];
		$orgdn = "ou=".$arraydn[2];//.",o=gas";

		if (!($arraydn[3]=="gas")){
			$orgdn.= ",ou=".$arraydn[3];
		}

		$orgdn .= ",o=gas";
		//echo $orgdn."\n";
		//узнать номер профицентра
		$result = ldap_search($this->link, $orgdn, "l=*", array('l'));
		if (!$result){
			//echo "error ldap_read";
			return false;
		}

		$arr = array();
		$ldap_res = ldap_first_entry($this->link, $result);

		do {
			$dn=ldap_get_dn($this->link, $ldap_res);
			//$arr[ldap_get_values($link, $ldap_res, 'l')[0]] =  preg_split('/[=,]/', $dn, -1, PREG_SPLIT_DELIM_CAPTURE)[1];

			$npc = ldap_get_values($this->link, $ldap_res, 'l')[0];
			//echo $npc."\n";
			if ($npc == 'Витебскгах механ мастерские') {

			} else {
				$arr[] = $npc;
			}
		} while ($ldap_res = ldap_next_entry($this->link, $ldap_res));

		//if (count($arr)==1) {
		//	return (int)$arr[0];
		//} else {
		return $arr;
		//}
	}
/*--
/*------------------------------------------------------------------------*/
/*------------------------------------------------------------------------*/
/*------------------------------------------------------------------------*/
/*------------------------------------------------------------------------*/
/*------------------------------------------------------------------------*/
/*------------------------------------------------------------------------*/
/*------------------------------------------------------------------------*/
/*------------------------------------------------------------------------*/
/*------------------------------------------------------------------------*/
/*------------------------------------------------------------------------*/
}

