<?php

namespace common\models\urg;

use Yii;

/**
 * This is the model class for table "urg_instrumentation".
 *
 * @property integer $id_obj
 * @property integer $id_type
 * @property integer $id_make
 * @property integer $quantity
 * @property string $desct
 *
 * @property Obj $idObj
 */
class Instrumentation extends \common\components\myModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'urg_instrumentation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_obj'], 'required'],
            [['id_obj', 'id_type', 'id_make', 'quantity'], 'integer'],
            [['descr'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_obj' => 'Id Obj',
            'id_type' => 'Id Type',
            'id_make' => 'Id Make',
            'quantity' => 'Quantity',
            'desct' => 'Desct',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdObj()
    {
        return $this->hasOne(Obj::className(), ['id_obj' => 'id_obj']);
    }

	/*----------------------------------------------------------------------------------*/
	public function getId() {
		return (string)$this->id_obj;
	}
	/*----------------------------------------------------------------------------------*/
	// link_obj - если УРГ чей-то родитель
	public function getLinkObjAsParent() {
		return $this->hasMany(LinkObj::className(), ['id_obj1' => 'id_obj']);
	}
	/*----------------------------------------------------------------------------------*/
	// файлы
	public function getFiles() {
		return $this->hasMany(File::className(), ['id_obj' => 'id_obj2'])
			->via('linkObjAsParent');
	}

	/*----------------------------------------------------------------------------------*/
	// link_obj - если оборудование чей-то дети
	public function getLinkObjAsChild() {
		return $this->hasOne(LinkObj::className(), ['id_obj2' => 'id_obj']);
	}
	/*----------------------------------------------------------------------------------*/
	public function delete() {
		// Удалить обхект полностью: из link_obj, urg_equipment и obj
		// TODO: удалить и прикреплённые файлы
		return Obj::del($this->id);
	}
	/*----------------------------------------------------------------------------------*/
	public function fields()
	{
		$fields = parent::fields();
		$fields['id'] = function() {
			return $this->id;
		};
		unset($fields['id_obj']);
		return  $fields;
	}

	/*----------------------------------------------------------------------------------*/
	public function extraFields() {
		$fields = parent::extraFields();

		$fields['files'] = function() {
			return $this->files;
		};

		return $fields;
	}
}
