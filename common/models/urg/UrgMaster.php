<?php

namespace common\models\urg;

use Yii;

/**
 * This is the model class for table "urg_master".
 *
 * @property integer $id
 * @property string $date
 * @property integer $id_urg
 * @property string $post
 * @property string $name
 *
 * @property Urg $idUrg
 */
class UrgMaster extends \common\components\myModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'urg_master';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date'], 'safe'],
            [['id_urg'], 'required'],
            [['id_urg'], 'integer'],
            [['post', 'name'], 'string', 'max' => 100],
            [['id_urg'], 'exist', 'skipOnError' => true, 'targetClass' => Urg::className(), 'targetAttribute' => ['id_urg' => 'id_obj']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date' => 'Date',
            'id_urg' => 'Id Urg',
            'post' => 'Post',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUrg()
    {
        return $this->hasOne(Urg::className(), ['id_obj' => 'id_urg']);
    }

}
