<?php

namespace common\models\urg;

use Yii;
use yii\db\Query;

/**
 * This is the model class for table "urg_ring".
 *
 * @property integer $id_urg
 * @property integer $id_ring
 */
class Ring extends \yii\db\ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'urg_ring';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['id_urg', 'id_ring'], 'required'],
			[['id_urg', 'id_ring'], 'integer'],
			[['id_urg'], 'unique']
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id_urg' => 'Id Urg',
			'id_ring' => 'Id Ring',
		];
	}

	/* ------------------------------------------------------------------------------------ */
	public function getUrg() {
		return $this->hasMany(Urg::className(), ['id_obj'=>'id_urg']);
	}
	/* ------------------------------------------------------------------------------------ */
	public function getOneUrg() {
		return $this->hasOne(Urg::className(), ['id_obj'=>'id_urg']);
	}
	/* ------------------------------------------------------------------------------------ */
	public function extraFields() {
		$fields = parent::extraFields();

		$fields['urg'] = function () {
			return self::getRingsList();
		};
		$fields['data'] = function () {
			return [
				'type' => $this->oneUrg->type->name,
				'num' => $this->oneUrg->num,
				'id_unit' => $this->oneUrg->id_unit,
			];
		};

		return $fields;
	}
	/* ------------------------------------------------------------------------------------ */
	public static function getRingsList() {
		$res = Ring::find()
			->select('id_ring')
			->distinct()
			->column();

		return $res;
	}
	/* ------------------------------------------------------------------------------------ */
	public static function getNewRingId() {
		return Ring::find()->select('max(id_ring)+1')->scalar();
	}
	/* ------------------------------------------------------------------------------------ */
	public static function makeRing($id_current_urg, $ids_other_urg) {
			// создать кольцо из переданных id
			// $id_current_urg и $ids_other_urg  -  все впихиваем в одно кольцо
		$new_ring = Ring::getNewRingId();
		$ids_all_urgs = $ids_other_urg;
		if (array_search($id_current_urg, $ids_other_urg)===false)
			$ids_all_urgs[] = $id_current_urg;

		$res = [
			'status' => 'unknown',
			'message'=> '-',
		];

		$res['newRingId'] = $new_ring;
		$res['message'] = 'MAKE TEST PASSED!';
		$res['data'] = [$id_current_urg, $ids_other_urg];

		$inRing = Ring::find()->select('id_urg')->where(['in', 'id_urg', $ids_all_urgs])->column();
		$outOfRing = array_values(array_diff($ids_all_urgs, $inRing));

		foreach ($outOfRing as &$item) {
			$item = [$item];
		}

		if (count($outOfRing)>0) {
			(new Query())->createCommand()
				->batchInsert(
					'urg_ring',
					['id_urg'],
					$outOfRing)
				->execute();
		}

		$res['ids_all_urgs'] = $ids_all_urgs;
		$res['inRing'] = $inRing;
		$res['outOfRing'] = $outOfRing;

		$res['upd_res'] = Ring::updateAll(['id_ring'=>$new_ring], ['in', 'id_urg', $ids_all_urgs]);


		return $res;
	}
	/* ------------------------------------------------------------------------------------ */
	public static function addToRing($id_current_urg, $id_other_urg) {
		//	объединить два УРГ в кольцо. при этом $id_other_urg может уже быть в кольце
		$res = [
			'status' => 'unknown',
			'message'=> '-',
		];

			// проверяем закольцованность для двух УРГ
		$urg_curr  = Ring::findOne($id_current_urg);
		$urg_other = Ring::findOne($id_other_urg);

			// если $id_other_urg не в кольце - создаём новое кольцо
		if (!$urg_other) {
			$new_ring = Ring::getNewRingId();
			$new_ring = $new_ring ? $new_ring : 1;
			$res['message'] = '$id_other_urg не в кольце - создаём новое кольцо ='.$new_ring;

			// в таблице Ring создаём две новые записи с одинаковым новым id_urg
			if (!$urg_curr) {
				$urg_curr = new Ring();
				$urg_curr->id_urg  = $id_current_urg;
			}
			$urg_curr->id_ring = $new_ring;
			$urg_curr->save();

			$urg_other = new Ring();
			$urg_other->id_urg  = $id_other_urg;
			$urg_other->id_ring = $new_ring;
			$urg_other->save();

			$res['status'] = 'ok';
			return $res;
		}

			// если оба в одном кольце - ничё не делаем
		if (($urg_curr && $urg_other) && ($urg_curr->id_ring == $urg_other->id_ring)) {
			$res['message'] = 'оба в одном кольце';
		} else {
			// если $id_other_urg уже в кольце - $id_current_urg запихиваем в это же кольцо
			if ($urg_other) {
				$res['message'] = 'текущий УРГ запихиваем в кольцо выбранного УРГ ='.$urg_other->id_ring;
				$urg_curr->id_ring = $urg_other->id_ring;
				$urg_curr->save();

			}
		}


		$res[] = [$urg_curr, $urg_other];
		return $res;
	}
}