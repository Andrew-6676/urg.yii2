<?php

namespace common\models\urg;

use Yii;
use yii\helpers\Json;

/**
 * This is the model class for table "psk".
 *
 * @property integer $id_obj
 * @property integer $id_model
 * @property integer $id_diameter
 * @property integer $id_type
 * @property integer $id_manufacturer
 * @property integer $year_release
 * @property string $date_starting
 * @property integer $lifetime
 * @property string $serial_number
 * @property double $trigger_setting
 *
 * @property PskSprModel $idModel
 * @property PskSprType $idType
 * @property SprManufacturer $idManufacturer
 * @property TubeSprDiameter $idDiameter
 */
class Psk extends \common\components\myModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'psk';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
	    return [
		    [['id_model', 'id_diameter', 'id_type', 'id_manufacturer', 'year_release', 'lifetime'], 'integer'],
		    [['date_starting', 'pressure_data'], 'safe'],
		    [['trigger_setting'], 'number'],
		    [['serial_number'], 'string', 'max' => 30]
	    ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
	    return [
		    'id_obj' => 'Id Obj',
		    'id_model' => 'Id Model',
		    'id_diameter' => 'Id Diameter',
		    'id_type' => 'Id Type',
		    'id_manufacturer' => 'Id Manufacturer',
		    'year_release' => 'Year Release',
		    'date_starting' => 'Date Starting',
		    'lifetime' => 'Lifetime',
		    'serial_number' => 'Serial Number',
		    'trigger_setting' => 'Trigger Setting',
	    ];
    }
	/*----------------------------------------------------------------------------------*/
	public function getId() {
		return (string)$this->id_obj;
	}
	/*----------------------------------------------------------------------------------*/
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModel()
    {
        return $this->hasOne(PskSprModel::className(), ['id_model' => 'id_model']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(PskSprType::className(), ['id_type' => 'id_type']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManufacturer()
    {
        return $this->hasOne(SprManufacturer::className(), ['id_manufacturer' => 'id_manufacturer']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDiameter()
    {
        return $this->hasOne(TubeSprDiameter::className(), ['id_diameter' => 'id_diameter']);
    }

	/*------------------------------------*/

	public function fields()
	{
		$fields = parent::fields();

		$fields['id'] = function () {
			return (string)$this->id_obj;
		};
		unset($fields['id_obj']);


		$fields['diameter'] = function () {
			return [
				'id'=>$this->id_diameter,
				'diameter'=>$this->diameter->diameter,
			];
		};
		$fields['type'] = function () {
			return [
				'id'=>$this->id_type,
				'type'=>$this->type->type,
				'id_make'=>$this->model->id_make,
			];
		};
		$fields['model'] = function () {
			return [
				'id'=>$this->id_model,
				'model'=>$this->model->model,
				'id_make'=>$this->model->id_make,
			];
		};
		$fields['manufacturer'] = function () {
			return [
				'id'=>$this->id_manufacturer,
				'manufacturer'=>$this->manufacturer->manufacturer,
			];
		};

		$fields['pressure_data'] = function () {
			return Json::decode($this->pressure_data ? $this->pressure_data : "null", true);
			//return  $this->pressure_data;
		};
//		unset($fields['id_diameter']);
//		unset($fields['id_type']);
//		unset($fields['id_model']);
//		unset($fields['id_manufacturer']);

		return $fields;
	}

	/*--------------------------------------*/
//	static public function find()
//	{
//		return parent::find()->with(['diameter', 'manufacturer', 'model', 'type']);
//	}
}
