<?php

namespace common\models\urg;

use Yii;

/**
 * This is the model class for table "link_unit".
 *
 * @property integer $parent
 * @property integer $child
 */
class LinkUnit extends \common\components\myModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'link_unit';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent', 'child'], 'required'],
            [['parent', 'child'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'parent' => 'Parent',
            'child' => 'Child',
        ];
    }

	public function getParentUnit()	{
		return $this->hasOne(Units::className(), ['id_obj' => 'parent']);
	}

}
