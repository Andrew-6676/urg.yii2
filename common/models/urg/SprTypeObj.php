<?php

namespace common\models\urg;

use Yii;

/**
 * This is the model class for table "spr_type_obj".
 *
 * @property integer $id_type_obj
 * @property string $type_obj
 * @property string $name_table
 *
 * @property LinkObj[] $linkObjs
 * @property LinkObj[] $linkObjs0
 * @property Obj[] $objs
 */
class SprTypeObj extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'spr_type_obj';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_obj', 'name_table'], 'required'],
            [['type_obj'], 'string', 'max' => 50],
            [['name_table'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_type_obj' => 'Id Type Obj',
            'type_obj' => 'Type Obj',
            'name_table' => 'Name Table',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLinkObjs()
    {
        return $this->hasMany(LinkObj::className(), ['id_type_obj1' => 'id_type_obj']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLinkObjs0()
    {
        return $this->hasMany(LinkObj::className(), ['id_type_obj2' => 'id_type_obj']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObjs()
    {
        return $this->hasMany(Obj::className(), ['id_type_obj' => 'id_type_obj']);
    }

    /*------------------------------------------------------------------*/
    public function fields() {
    	$fields = [];

    	$fields['id'] = function () {
    	    return (string)$this->id_type_obj;
	    };
	    $fields['name'] = function () {
		    return $this->type_obj;
	    };
	    $fields['table'] = function () {
		    return $this->name_table;
	    };
//	    $fields['table_name'] = function () {
//		    return $this->name_table;
//	    };
    	return $fields;
    }
    /*------------------------------------------------------------------*/
//    public static function find() {
//    	return parent::find()->where(['in', 'id_type_obj', [1,4,31,32,80,81,82]]);
//    }
}
