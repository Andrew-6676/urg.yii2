<?php

namespace common\models\urg;

use Yii;
use yii\helpers\Json;

/**
 * This is the model class for table "pc".
 *
 * @property integer $id_obj
 * @property integer $id_model
 * @property integer $id_manufacturer
 * @property integer $year_release
 * @property string $date_starting
 * @property string $serial_number
 * @property integer $id_flue
 * @property integer $id_type
 * @property double $capacity
 * @property double $pressure_range_min
 * @property double $pressure_range_max
 * @property double $pressure_setup
 * @property string $flow_direction
 * @property integer $id_diameter
 * @property integer $lifetime
 *
 * @property Obj $idObj
 * @property PcSprModel $idModel
 */
class Pc extends \common\components\myModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pc';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {

	    return [
		    [['id_model', 'id_manufacturer', 'year_release', 'id_flue', 'id_type', 'id_diameter', 'lifetime'], 'integer'],
		    [['date_starting', 'pressure_data'], 'safe'],
		    [['capacity', 'pressure_range_min', 'pressure_range_max', 'pressure_setup'], 'number'],
		    [['serial_number'], 'string', 'max' => 30],
		    [['flow_direction'], 'string', 'max' => 50]
	    ];

    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_obj' => 'Id Obj',
            'id_model' => 'Id Model',
            'id_manufacturer' => 'Производитель',
            'year_release' => 'Year Release',
            'date_starting' => 'Date Starting',
            'serial_number' => 'Serial Number',
            'id_flue' => 'Id Flue',
            'capacity' => 'Пропускная способность',
            'id_type' => 'Id Type',
            'exit_pressure' => 'Настройка выходного давления',
        ];
    }

	/*----------------------------------------------------------------------------------*/
	public function getId() {
		return (string)$this->id_obj;
	}
	/*----------------------------------------------------------------------------------*/

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModel()
    {
        return $this->hasOne(PcSprModel::className(), ['id_model' => 'id_model']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(PcSprType::className(), ['id_type' => 'id_type']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManufacturer()
    {
        return $this->hasOne(SprManufacturer::className(), ['id_manufacturer' => 'id_manufacturer']);
    }

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getDiameter()
	{
		return $this->hasOne(TubeSprDiameter::className(), ['id_diameter' => 'id_diameter']);
	}
    /*----------------------------------------------------------------------*/
	public function fields()
	{
		$fields = parent::fields();

		$fields['id'] = function () {
			return (string)$this->id_obj;
		};
		unset($fields['id_obj']);

		$fields['diameter'] = function () {
			return [
				'id'=>$this->id_diameter,
				'diameter'=>$this->diameter->diameter,
			];
		};
		$fields['type'] = function () {
			return [
				'id'=>$this->id_type,
				'type'=>$this->type->type,
			];
		};
		$fields['model'] = function () {
			return [
				'id'=>$this->id_model,
				'model'=>$this->model->model,
				'id_make' => $this->model->id_make,
			];
		};
		$fields['manufacturer'] = function () {
			return [
				'id'=>$this->id_manufacturer,
				'manufacturer'=>$this->manufacturer->manufacturer,
			];
		};

		$fields['pressure_data'] = function () {
			return Json::decode($this->pressure_data ? $this->pressure_data : "null", true);
			//return  $this->pressure_data;
		};
//		unset($fields['id_diameter']);
//		unset($fields['id_type']);
//		unset($fields['id_model']);
//		unset($fields['id_manufacturer']);

		return $fields;
	}

	/*--------------------------------------*/
	public function extraFields()
	{
		$fields = parent::extraFields();
		$fields['diameter'] = function () {
			return [
				'id'=>$this->id_diameter,
				'diameter'=>$this->diameter->diameter,
			];
		};
		$fields['type'] = function () {
			return [
				'id'=>$this->id_type,
				'type'=>$this->type->type,
			];
		};
		$fields['model'] = function () {
			return [
				'id'=>$this->id_model,
				'model'=>$this->model->model,
			];
		};
		$fields['manufacturer'] = function () {
			return [
				'id'=>$this->id_manufacturer,
				'manufacturer'=>$this->manufacturer->manufacturer,
			];
		};
		$fields['pressure_data'] = function () {
			return $this->pressure_data;
		};


		return $fields;
	}

	/*--------------------------------------*/
//	static public function find()
//	{
//		return parent::find()->with(['diameter', 'manufacturer', 'model', 'type']);
//	}

}
