<?php

namespace common\models\urg;

use Yii;

/**
 * This is the model class for table "filter_spr_make".
 *
 * @property integer $id_make
 * @property string $make
 *
 * @property FilterSprModel[] $filterSprModels
 */
class FilterSprMake extends \common\components\myModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'filter_spr_make';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['make'], 'required'],
            [['make'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_make' => 'Id Make',
            'make' => 'Make',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFilterSprModels()
    {
        return $this->hasMany(FilterSprModel::className(), ['id_make' => 'id_make']);
    }
}
