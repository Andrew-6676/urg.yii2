<?php

namespace common\models\urg;

use Yii;

/**
 * This is the model class for table "psk_spr_model".
 *
 * @property integer $id_model
 * @property string $model
 * @property integer $id_make
 * @property integer $id_type
 *
 * @property Psk[] $psks
 * @property PskSprMake $idMake
 * @property PskSprType $idType
 */
class PskSprModel extends \common\components\myModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'psk_spr_model';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['model', 'id_make'], 'required'],
            [['id_make', 'id_type'], 'integer'],
            [['model'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_model' => 'Id Model',
            'model' => 'Model',
            'id_make' => 'Id Make',
            'id_type' => 'Id Type',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPsks()
    {
        return $this->hasMany(Psk::className(), ['id_model' => 'id_model']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdMake()
    {
        return $this->hasOne(PskSprMake::className(), ['id_make' => 'id_make']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdType()
    {
        return $this->hasOne(PskSprType::className(), ['id_type' => 'id_type']);
    }
}
