<?php

namespace common\models\urg;

use Yii;

/**
 * This is the model class for table "meter_spr_make".
 *
 * @property integer $id_make
 * @property string $make
 * @property string $number
 * @property integer $id_dimension
 * @property integer $period
 *
 * @property MeterSprModel[] $meterSprModels
 */
class MeterSprMake extends \common\components\myModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'meter_spr_make';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['make', 'number', 'id_dimension', 'period'], 'required'],
            [['id_dimension', 'period'], 'integer'],
            [['make', 'number'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_make' => 'Id Make',
            'make' => 'Make',
            'number' => 'номер в гос. реестре',
            'id_dimension' => 'ед. измерения',
            'period' => 'период проверки в днях',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMeterSprModels()
    {
        return $this->hasMany(MeterSprModel::className(), ['id_make' => 'id_make']);
    }
}
