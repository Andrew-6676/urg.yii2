<?php

namespace common\models\urg;

use Yii;

/**
 * This is the model class for table "files".
 *
 * @property integer $id_obj
 * @property string $path
 * @property string $name
 * @property string $title
 * @property string $md5sum
 */
class File extends \common\components\myModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'files';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_obj', 'path', 'name', 'title', 'md5sum'], 'required'],
            [['id_obj', 'id_category'], 'integer'],
            [['path', 'title'], 'string', 'max' => 100],
            [['name', 'md5sum'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_obj' => 'Id Obj',
            'path' => 'Path',
            'name' => 'Name',
            'title' => 'Title',
            'md5sum' => 'Md5sum',
	        'id_category' => 'Категория'
        ];
    }

	/* --------------------------------------------------------------------------------- */

	public function getId() {
		return (string)$this->id_obj;
	}
	/* --------------------------------------------------------------------------------- */

	public function getId_obj() {
		return (string)$this->id_obj;
	}

    /* --------------------------------------------------------------------------------- */
    /* --------------------------------------------------------------------------------- */

    static public function add($urg, $path, $name, $title, $md5, $id_category = 0) {
			// добавляем файл в БД
	    $id_obj = Obj::addFile();
	    
	    $f = new File();
	    $f->id_obj  = $id_obj;
	    $f->path    = $path;
	    $f->name    = $name;
	    $f->title   = $title;
	    $f->md5sum  = $md5;
	    $f->id_category  = $id_category;

	        // сохраняем в БД
	    if ($f->save()) {
		        // добавляем связь файла с УРГ
		    return (LinkObj::add($urg, $id_obj)); 
	    } else {
	    	print_r($f->errors);
		    return false;
	    }
    }

	/* --------------------------------------------------------------------------------- */
//	public function delete() {
//
//	}

	/* --------------------------------------------------------------------------------- */
	public static function del($id) {
			// если на один и тот же файл есть в двух и более записях files - удаляем только из БД
		$res = null;
		$file = self::findOne($id);
		if (!$file) {
			Yii::$app->getResponse()->setStatusCode(404);
		} else {
				// проверяем ссылки из других мест
			$files_count = File::find()->where(['path'=>$file->path, 'name'=>$file->name])->count();

			if ($files_count>1){
					// удаляем только из БД
				$res = Obj::del($id);
			} else {
					// удаляем файл, а затем и из БД
				if (file_exists($file->path . '/' . $file->name)) {
					//$res = 'exist';
					// удалить файл с диска
					if (unlink($file->path . '/' . $file->name)) {
						// удалить файл из БД
						$d = Obj::del($id);
						if ($d>0) {
							$res['status'] = 'ok';
							$res['message'] = 'Файл удалён.';
						} else {
							$res['status'] = 'error';
							$res['message'] = 'Не удалось удалить файл из БД';
						}
					} else {
						$res['status'] = 'error';
						$res['message'] = 'Не удалось удалить файл с диска';
					}
				} else {
					$d = Obj::del($id);
					if ($d>0) {
						$res['status'] = 'ok';
						$res['message'] = 'Файл удалён из БД. Файл на диске отсутствовал.';
					} else {
						$res['status'] = 'error';
						$res['message'] = 'Не удалось удалить файл из БД. Файл на диске отсутствует.';
					}
				}
			}
		}

		return $res;
	}
	/* --------------------------------------------------------------------------------- */
	/* --------------------------------------------------------------------------------- */
	public function fields()
	{
		//$fields = parent::fields();


		$fields['id'] = function () { return (string)$this->id_obj; };
		$fields[] = 'title';
		$fields['id_category'] = function() {
			return $this->id_category ? $this->id_category : '0';
		};
		$fields['ext'] = function () {
			return pathinfo($this->path.'/'.$this->name, PATHINFO_EXTENSION);
		};

		$fields['link'] = function (){
				// возвращаем часть пути после /var/www/scheme
			return Yii::$app->params['download_dir'].'/'.preg_split('#'.Yii::$app->params['uploads_dir'].'[/]*#', $this->path, -1, PREG_SPLIT_NO_EMPTY)[0].'/'.$this->name;
		};

		if (file_exists($this->path.'/'.$this->name)) {
			$fields['size'] = function (){
				$size = filesize($this->path.'/'.$this->name);
				if ($size<1024*1024)
					$size = round($size/1024, 1).' Kb';
				else
					$size = round($size/1024/1024, 1).' Mb';
				return $size;
			};
			$fields['exist'] = function (){
				return true;
			};
		} else {
			$fields['size'] = function (){
				return 0;
			};
			$fields['exist'] = function (){
				return false;
			};
		}

		return $fields;
	}
	/* --------------------------------------------------------------------------------- */

	/* --------------------------------------------------------------------------------- */
}
