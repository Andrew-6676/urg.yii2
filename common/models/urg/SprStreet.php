<?php

namespace common\models\urg;

use common\components\myModel;
use Yii;

/**
 * This is the model class for table "spr_street".
 *
 * @property integer $id_street
 * @property integer $uid_street
 * @property string $street
 * @property integer $id_type_street
 * @property integer $id_np
 * @property integer $actual
 * @property integer $source
 *
 * @property Address[] $addresses
 */
class SprStreet extends myModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'spr_street';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['uid_street', 'street', 'id_type_street', 'id_np'], 'required'],
            [['uid_street', 'id_type_street', 'id_np', 'actual', 'source'], 'integer'],
            [['street'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_street' => 'ID',
            'uid_street' => 'Uid Street',
            'street' => 'Street',
            'id_type_street' => 'Id Type Street',
            'id_np' => 'Id Np',
            'actual' => 'Actual',
            'source' => 'Source',
        ];
    }

    public function getId() {
        return (string)$this->id_street;
    }
    public function getName() {
        return $this->street;
    }
	/*----------------------------------------------------------------------------------*/
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddresses()
    {
        return $this->hasMany(Address::className(), ['id_street' => 'id_street']);
    }
	/*----------------------------------------------------------------------------------*/
	public function getType()
	{
		return $this->hasOne(SprTypeStreet::className(), ['id_type_street' => 'id_type_street']);
	}
    /*----------------------------------------------------------------------------------*/
    public function fields()
    {
        //$fields = parent::fields();

        $fields['id']   = function () {
            return (string)$this->id_street;
        };
        $fields['name'] = function () {
            return $this->type->short_type.' '.$this->street;
        };
        //$fields['id_street'] = function() {
        //    return (string)$this->id_street;
        //};
        return $fields;
    }
    /*----------------------------------------------------------------------------------*/
    public function extraFields()
    {
        $extraFields = parent::extraFields();

        $extraFields['id'] = function() {
            return (string)$this->id_street;
        };
        $extraFields['name1'] = 'street';
        //$extraFields['name'] = function () { return $this->street.' ('.mb_strtolower($this->type->type_street).')'; };
        $extraFields['name'] = function () { return $this->type->short_type.' '.$this->street; };


        return $extraFields;
    }
    /*----------------------------------------------------------------------------------*/
	public static function find() {
		return parent::find()
			->with(['type']);
	}
    /*----------------------------------------------------------------------------------*/
}
