<?php

namespace common\models\urg;

use common\components\myModel;
use Yii;

/**
 * This is the model class for table "spr_np" - населённые пункты.
 *
 * @property integer $id_np
 * @property string $np
 * @property integer $id_type_np
 * @property integer $source
 * @property integer $id_region
 * @property integer $id_selsov
 *
 * @property Address[] $addresses
 */
class SprNp extends myModel
{
    //private $defaultOrder = 'np';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'spr_np';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['np', 'id_type_np', 'id_region', 'id_selsov'], 'required'],
            [['id_type_np', 'source', 'id_region', 'id_selsov'], 'integer'],
            [['np'], 'string', 'max' => 40]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_np' => 'Id Np',
            'np' => 'Np',
            'id_type_np' => 'Id Type Np',
            'source' => 'Source',
            'id_region' => 'Id Region',
            'id_selsov' => 'Id Selsov',
        ];
    }

	/*----------------------------------------------------------------------------------*/

	public function getName() {
		return $this->np;
	}
	public function getId() {
		return (string)$this->id_np;
	}
    /*----------------------------------------------------------------------------------*/

	public function getAddresses() {
		return $this->hasMany(Address::className(), ['id_np' => 'id_np']);
	}

	public function getRegion() {
		return $this->hasOne(SprRegion::className(), ['id_region' => 'id_region']);
	}

	public function getType() {
		return $this->hasOne(SprTypeNp::className(), ['id_type_np' => 'id_type_np']);
	}
	/*----------------------------------------------------------------------------------*/
	public function fields()
	{
		//$fields = parent::fields();

		$fields['name'] = function() {
			return $this->type->type_np.' '.$this->np;
		};;
		$fields['id'] = function() {
			return (string)$this->id_np;
		};

		return $fields;
	}

	/*----------------------------------------------------------------------------------*/
	/*----------------------------------------------------------------------------------*/
	public function extraFields()
	{
		$extraFields = parent::extraFields();

		$extraFields['id'] = function() {
			return (string)$this->id_np;
		};
		$extraFields['name1'] = 'np';
		$extraFields['name2'] = function () { return $this->np.' ('.$this->type->type_np.')'; };
		$extraFields['name'] = function () { return $this->type->type_np.' '.$this->np; };
		$extraFields['region'] = function () { return $this->region->region; };//
		$extraFields['region_full'] = function () { return ['id'=>1, 'name'=>$this->region->region]; };//

		return $extraFields;
	}

/*------------------------------------------------------------------------------------------*/
	public static function find() {
		return parent::find()
			->with(['type', 'region']);
	}
/*------------------------------------------------------------------------------------------*/
//
//    public static function filter($params) {
//        $params = explode(',',$_GET['filter']);
//        $query = self::find()->where(1);
//        foreach ($params as $param) {
//            //$p = explode('=',$param);
//            $query->andWhere($param);
//        }
//        return $query;
//    }

        public function getUnit() {
            
        }
//    public static function getNpByUnit($region) {
//        return self::find()->where('');
//    }

}
