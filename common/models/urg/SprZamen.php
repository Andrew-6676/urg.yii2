<?php

namespace common\models\urg;

use Yii;

/**
 * This is the model class for table "spr_zamen".
 *
 * @property string $id_zamen
 * @property string $day_r
 * @property string $day_z
 * @property string $star
 */
class SprZamen extends \common\components\myModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'spr_zamen';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['day_r', 'day_z'], 'safe'],
            [['star'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_zamen' => 'Id Zamen',
            'day_r' => 'Day R',
            'day_z' => 'Day Z',
            'star' => 'Star',
        ];
    }
}
