<?php

namespace common\models\urg;

use Yii;

/**
 * This is the model class for table "urg_room".
 *
 * @property integer $id
 * @property integer $id_urg
 * @property integer $id_type
 * @property double $area
 * @property string $descr
 *
 * @property Urg $id0
 */
class UrgRoom extends \common\components\myModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'urg_room';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
	        [['id_urg', 'id_type'], 'integer'],
            [['area'], 'number'],
            [['descr'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
	        'id_urg' => 'Id Urg',
            'id_type' => 'Id Type',
            'area' => 'Area',
            'descr' => 'Descr',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUrg()
    {
        return $this->hasOne(Urg::className(), ['id_obj' => 'id_urg']);
    }
}
