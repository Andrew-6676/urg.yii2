<?php

namespace common\models\urg;

use Yii;

/**
 * This is the model class for table "worker_confvalue".
 *
 * @property integer $id_worker
 * @property string $confvalue
 * @property string $confvalue_urg
 * @property integer $id_unit
 */
class WorkerConfvalue extends \common\components\myModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'worker_confvalue';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_worker', 'confvalue', 'confvalue_urg', 'id_unit'], 'required'],
            [['id_worker', 'id_unit'], 'integer'],
            [['confvalue', 'confvalue_urg'], 'string', 'max' => 40]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_worker' => 'Id Worker',
            'confvalue' => 'Confvalue',
            'confvalue_urg' => 'Confvalue Urg',
            'id_unit' => 'Id Unit',
        ];
    }
}
