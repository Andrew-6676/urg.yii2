<?php

namespace common\models\urg;

use Yii;
use yii\db\Query;

/**
 * This is the model class for table "link_obj".
 *
 * @property integer $id_link_obj
 * @property integer $id_obj1
 * @property integer $id_obj2
 * @property integer $id_type_obj1
 * @property integer $id_type_obj2
 *
 * @property Obj $idObj2
 * @property Obj $idObj1
 * @property SprTypeObj $idTypeObj1
 * @property SprTypeObj $idTypeObj2
 */
class LinkObj extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'link_obj';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_obj1', 'id_obj2', 'id_type_obj1', 'id_type_obj2'], 'required'],
            [['id_obj1', 'id_obj2', 'id_type_obj1', 'id_type_obj2'], 'integer'],
            [['id_obj2'], 'exist', 'skipOnError' => true, 'targetClass' => Obj::className(), 'targetAttribute' => ['id_obj2' => 'id_obj']],
            [['id_obj1'], 'exist', 'skipOnError' => true, 'targetClass' => Obj::className(), 'targetAttribute' => ['id_obj1' => 'id_obj']],
            [['id_type_obj1'], 'exist', 'skipOnError' => true, 'targetClass' => SprTypeObj::className(), 'targetAttribute' => ['id_type_obj1' => 'id_type_obj']],
            [['id_type_obj2'], 'exist', 'skipOnError' => true, 'targetClass' => SprTypeObj::className(), 'targetAttribute' => ['id_type_obj2' => 'id_type_obj']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_link_obj' => 'Id Link Obj',
            'id_obj1' => 'Id Obj1',
            'id_obj2' => 'Id Obj2',
            'id_type_obj1' => 'Id Type Obj1',
            'id_type_obj2' => 'Id Type Obj2',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnitAsParent()
    {
        return $this->hasOne(LinkUnit::className(), ['parent' => 'id_obj1']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdObj1()
    {
        return $this->hasOne(Obj::className(), ['id_obj' => 'id_obj1']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdTypeObj1()
    {
        return $this->hasOne(SprTypeObj::className(), ['id_type_obj' => 'id_type_obj1']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdTypeObj2()
    {
        return $this->hasOne(SprTypeObj::className(), ['id_type_obj' => 'id_type_obj2']);
    }

    /*---------------------------------------------------------------------------*/
//        // удалить связи для указанного объекта
//    static public function delByChild($id=null) {
//    	$res = 0;
//    	if ($id) {
//		    $res = self::deleteAll(['id_obj2' => $id]);
//	    }
//	    return $res;
//    }
//	static public function delByParent($id=null) {
//		$res = 0;
//		if ($id) {
//			$res = self::deleteAll(['id_obj1' => $id]);
//		}
//		return $res;
//	}
    /*---------------------------------------------------------------------------*/
    static public function remove($id_1, $id_2) {
    	return LinkObj::deleteAll(['id_obj1'=>$id_1, 'id_obj2'=>$id_2]);
    }
    /*---------------------------------------------------------------------------*/
	static public function add($id_1, $id_2) {
			// определить тип объекта и вставить в БД
	    $lo = new LinkObj();
	    $lo->id_obj1    = $id_1;
	    $lo->id_obj2    = $id_2;
	    $lo->id_type_obj1 = Obj::findOne($id_1)->id_type_obj;
	    $lo->id_type_obj2 = Obj::findOne($id_2)->id_type_obj;

		return $lo->save();
	}
}
