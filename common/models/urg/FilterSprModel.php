<?php

namespace common\models\urg;

use Yii;

/**
 * This is the model class for table "filter_spr_model".
 *
 * @property integer $id_model
 * @property string $model
 * @property integer $id_make
 *
 * @property Filter[] $filters
 * @property FilterSprMake $idMake
 */
class FilterSprModel extends \common\components\myModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'filter_spr_model';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['model', 'id_make'], 'required'],
            [['id_make'], 'integer'],
            [['model'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_model' => 'Id Model',
            'model' => 'Model',
            'id_make' => 'Id Make',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFilters()
    {
        return $this->hasMany(Filter::className(), ['id_model' => 'id_model']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdMake()
    {
        return $this->hasOne(FilterSprMake::className(), ['id_make' => 'id_make']);
    }
}
