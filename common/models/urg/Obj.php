<?php

namespace common\models\urg;

use Yii;

/**
 * This is the model class for table "obj".
 *
 * @property integer $id_obj
 * @property integer $id_type_obj
 *
 * @property Address $address
 * @property Juridical $juridical
 * @property LinkObj[] $linkObjs
 * @property LinkObj[] $linkObjs0
 * @property SprTypeObj $idTypeObj
 */
class Obj extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'obj';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_type_obj'], 'required'],
            [['id_type_obj'], 'integer'],
            [['id_type_obj'], 'exist', 'skipOnError' => true, 'targetClass' => SprTypeObj::className(), 'targetAttribute' => ['id_type_obj' => 'id_type_obj']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_obj' => 'Id Obj',
            'id_type_obj' => 'Id Type Obj',
        ];
    }

	/* ----------------------------------------------------------------------------------------- */
	public static function del($id) {
		$res = Yii::$app->db->createCommand("CALL deleteObj($id)")->execute();
		return $res;
	}
	/* ----------------------------------------------------------------------------------------- */

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddress()
    {
        return $this->hasOne(Address::className(), ['id_obj' => 'id_obj']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJuridical()
    {
        return $this->hasOne(Juridical::className(), ['id_obj' => 'id_obj']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLinkObjs()
    {
        return $this->hasMany(LinkObj::className(), ['id_obj2' => 'id_obj']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLinkObjs0()
    {
        return $this->hasMany(LinkObj::className(), ['id_obj1' => 'id_obj']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTypeObj()
    {
        return $this->hasOne(SprTypeObj::className(), ['id_type_obj' => 'id_type_obj']);
    }

    /*-------------------------------------------------------------------------------*/
		// link_obj - если УРГ чей-то родитель
	public function getLinkObjAsParent() {
		return $this->hasMany(LinkObj::className(), ['id_obj1' => 'id_obj']);
	}
	/*----------------------------------------------------------------------------------*/
		// файлы
	public function getFiles() {
		return $this->hasMany(File::className(), ['id_obj' => 'id_obj2'])
			->via('linkObjAsParent');
	}
    /*-------------------------------------------------------------------------------*/
	/**
	 * @return int
	 */
	static public function createOb($type) {
		$o = new Obj();
		$o->id_type_obj = SprTypeObj::findOne(['name_table' => $type])->id_type_obj;
		if ($o->save()){
			return $o->id_obj;
		} else {
			return -1;
		}
	}
	static public function createObjByIdType($id_type_obj) {
		$o = new Obj();
		$o->id_type_obj = $id_type_obj;
		if ($o->save()){
			return $o->id_obj;
		} else {
			return false;
		}
	}
	/*-------------------------------------------------------------------------------*/
	static public function addFilter() {
		return self::createOb('filter');
	}
	static public function addPzk() {
		return self::createOb('pzk');
	}
	static public function addPsk() {
		return self::createOb('psk');
	}
	static public function addBoiler() {
		return self::createOb('boiler');
	}
	static public function addLock() {
		return self::createOb('locks');
	}
	static public function addPc() {
		return self::createOb('pc');
	}
	static public function addMeter() {
		return self::createOb('meter');
	}

	static public function addFile() {
		return self::createOb('files');
	}

	static public function addAddress() {
		return self::createOb('address');
	}

	static public function addUrg() {
		return self::createOb('urg');
	}

	static public function addEquipment() {
		return self::createOb('urg_equipment');
	}
	static public function addInstrumentation() {
		return self::createOb('urg_instrumentation');
	}
	/*-------------------------------------------------------------------------------*/
}

