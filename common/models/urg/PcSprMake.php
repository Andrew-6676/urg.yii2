<?php

namespace common\models\urg;

use Yii;

/**
 * This is the model class for table "pc_spr_make".
 *
 * @property integer $id_make
 * @property string $make
 *
 * @property PcSprModel[] $pcSprModels
 */
class PcSprMake extends \common\components\myModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pc_spr_make';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['make'], 'required'],
            [['make'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_make' => 'Id Make',
            'make' => 'Make',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPcSprModels()
    {
        return $this->hasMany(PcSprModel::className(), ['id_make' => 'id_make']);
    }
}
