<?php

namespace common\models\urg;

use Yii;

/**
 * This is the model class for table "works_report".
 *
 * @property integer $id
 * @property integer $id_obj
 * @property string $num
 * @property string $work_date
 * @property string $data
 * @property boolean $complete
 *
 * @property Urg $idObj
 */
class WorksReport extends \common\components\myModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'works_report';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_obj'], 'integer'],
            [['work_date'], 'safe'],
            [['data'], 'string'],
            [['complete'], 'boolean'],
            [['num'], 'string', 'max' => 20],
            [['id_obj'], 'exist', 'skipOnError' => true, 'targetClass' => Urg::className(), 'targetAttribute' => ['id_obj' => 'id_obj']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_obj' => 'объкет обхода',
            'num' => 'номер',
            'work_date' => 'дата выполнения работы',
            'data' => 'содержимое рапорта',
            'complete' => 'отметка о выполнении',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUrg()
    {
        return $this->hasOne(Urg::className(), ['id_obj' => 'id_obj']);
    }
}
