<?php

namespace common\models\urg;

use Yii;

/**
 * This is the model class for table "urg_help".
 *
 * @property integer $id
 * @property string $index
 * @property string $title
 * @property string $text
 * @property integer $id_parent
 */
class Help extends \common\components\myModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'urg_help';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['index', 'title', 'text'], 'required'],
            [['text'], 'string'],
            [['id_parent'], 'integer'],
            [['index'], 'string', 'max' => 100],
            [['title'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'index' => 'для поиска нужной статьи',
            'title' => 'заголовок',
            'text' => 'текст справки',
            'id_parent' => 'ссылка на родительский элемент',
        ];
    }
}
