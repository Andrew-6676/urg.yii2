<?php

namespace common\models\urg;

use Yii;

/**
 * This is the model class for table "locks_spr_model".
 *
 * @property integer $id_model
 * @property string $model
 * @property integer $id_make
 */
class LocksSprModel extends \common\components\myModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'locks_spr_model';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['model', 'id_make'], 'required'],
            [['id_make'], 'integer'],
            [['model'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_model' => 'Id Model',
            'model' => 'Model',
            'id_make' => 'Id Make',
        ];
    }
}
