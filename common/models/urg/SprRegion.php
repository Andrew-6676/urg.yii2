<?php

namespace common\models\urg;

use common\components\myModel;
use Yii;

/**
 * This is the model class for table "spr_region" - административные районы.
 *
 * @property integer $id
 * @property integer $id_region
 * @property string $region
 * @property integer $input_address
 * @property integer $source
 * @property integer $id_unit
 */
class SprRegion extends myModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'spr_region';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_region', 'id_unit'], 'required'],
            [['id_region', 'input_address', 'source', 'id_unit'], 'integer'],
            [['region'], 'string', 'max' => 40],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_region' => 'Id Region',
            'region' => 'Region',
            'input_address' => 'Input Address',
            'source' => 'Source',
            'id_unit' => 'Id Unit',
        ];
    }

    public function getUnit()
    {
        return $this->hasOne(Units::className(), ['id_unit' => 'id_obj']);
    }
	/*----------------------------------------------------------------------------------*/
	public function getId() {
		return $this->id_region;
	}
    /*----------------------------------------------------------------------------------*/
    public function getName() {
    	return $this->region;
    }
    /*----------------------------------------------------------------------------------*/
    public function fields()
    {
        $fields = parent::fields();

	    $fields['id'] = function () { return $this->id_region;};
        $fields['name'] = function () { return $this->region;};

        return $fields;
    }
    /*----------------------------------------------------------------------------------*/
	public function extraFields()
	{
		$extraFields = parent::extraFields();

		return $extraFields;
	}

	/*----------------------------------------------------------------------------------*/
	
}
