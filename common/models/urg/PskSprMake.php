<?php

namespace common\models\urg;

use Yii;

/**
 * This is the model class for table "psk_spr_make".
 *
 * @property integer $id_make
 * @property string $make
 *
 * @property PskSprModel[] $pskSprModels
 */
class PskSprMake extends \common\components\myModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'psk_spr_make';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['make'], 'required'],
            [['make'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_make' => 'Id Make',
            'make' => 'Make',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPskSprModels()
    {
        return $this->hasMany(PskSprModel::className(), ['id_make' => 'id_make']);
    }
}
