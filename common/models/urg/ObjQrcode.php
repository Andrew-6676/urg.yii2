<?php

namespace common\models\urg;

use Yii;

/**
 * This is the model class for table "obj_qrcode".
 *
 * @property integer $id_obj
 * @property string $qrcode
 */
class ObjQrcode extends \common\components\myModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'obj_qrcode';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_obj', 'qrcode'], 'required'],
            [['id_obj'], 'integer'],
            [['qrcode'], 'string', 'max' => 34],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_obj' => 'Id Obj',
            'qrcode' => 'Qrcode',
        ];
    }
}
