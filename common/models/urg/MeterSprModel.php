<?php

namespace common\models\urg;

use Yii;

/**
 * This is the model class for table "meter_spr_model".
 *
 * @property integer $id_model
 * @property string $model
 * @property integer $id_make
 * @property string $measure_min
 * @property string $measure_max
 *
 * @property Meter[] $meters
 * @property MeterSprMake $idMake
 */
class MeterSprModel extends \common\components\myModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'meter_spr_model';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['model', 'id_make', 'measure_min', 'measure_max'], 'required'],
            [['id_make'], 'integer'],
            [['measure_min', 'measure_max'], 'number'],
            [['model'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_model' => 'Id Model',
            'model' => 'Model',
            'id_make' => 'Id Make',
            'measure_min' => 'минимум диапазона измерений',
            'measure_max' => 'максимум диапазона измерений',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMeters()
    {
        return $this->hasMany(Meter::className(), ['id_model' => 'id_model']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdMake()
    {
        return $this->hasOne(MeterSprMake::className(), ['id_make' => 'id_make']);
    }
}
