<?php

namespace common\models\urg;

use Yii;

/**
 * This is the model class for table "tube_spr_diameter".
 *
 * @property integer $id_diameter
 * @property integer $id_material
 * @property integer $diameter
 * @property string $standart
 * @property string $type
 * @property string $l_lock
 *
 * @property Filter[] $filters
 * @property Psk[] $psks
 * @property Pzk[] $pzks
 */
class TubeSprDiameter extends \common\components\myModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tube_spr_diameter';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_material', 'diameter'], 'required'],
            [['id_material', 'diameter'], 'integer'],
            [['standart', 'type', 'l_lock'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_diameter' => 'Id Diameter',
            'id_material' => 'Id Material',
            'diameter' => 'Diameter',
            'standart' => 'Standart',
            'type' => 'Type',
            'l_lock' => 'L Lock',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFilters()
    {
        return $this->hasMany(Filter::className(), ['id_diameter' => 'id_diameter']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPsks()
    {
        return $this->hasMany(Psk::className(), ['id_diameter' => 'id_diameter']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPzks()
    {
        return $this->hasMany(Pzk::className(), ['id_diameter' => 'id_diameter']);
    }

	public function getId() {
    	return $this->id_diameter;
	}

	public function fields()
	{
		//$fields = parent::fields();
		$fields['id'] = function () {
			return $this->id_diameter;
		};
		$fields['diameter'] = function () {
			return $this->diameter;
		};
		return $fields;
	}
}
