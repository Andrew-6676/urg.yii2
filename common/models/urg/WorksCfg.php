<?php

namespace common\models\urg;

use Yii;

/**
 * This is the model class for table "works_cfg".
 *
 * @property integer $id_work
 * @property integer $id_type_urg
 * @property string $config
 */
class WorksCfg extends \common\components\myModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'works_cfg';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_work', 'id_type_urg'], 'required'],
            [['id_work', 'id_type_urg'], 'integer'],
            [['config'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_work' => 'Id Work',
            'id_type_urg' => 'Id Type Urg',
            'config' => 'Config',
        ];
    }
}
