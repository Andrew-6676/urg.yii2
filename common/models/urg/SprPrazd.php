<?php

namespace common\models\urg;

use Yii;

/**
 * This is the model class for table "spr_prazd".
 *
 * @property integer $id_prazd
 * @property string $name_prazd
 * @property string $star
 */
class SprPrazd extends \common\components\myModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'spr_prazd';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name_prazd', 'star'], 'required'],
            [['name_prazd'], 'safe'],
            [['star'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_prazd' => 'Id Prazd',
            'name_prazd' => 'Name Prazd',
            'star' => 'Star',
        ];
    }
}
