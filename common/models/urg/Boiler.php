<?php

namespace common\models\urg;

use Yii;

/**
 * This is the model class for table "boiler".
 *
 * @property integer $id_obj
 * @property integer $id_model
 * @property integer $year_release
 * @property string $date_starting
 * @property string $serial_number
 * @property integer $id_flue
 * @property integer $id_type
 * @property integer $lifetime
 *
 * @property BoilerSprModel $idModel
 * @property Obj $idObj
 */
class Boiler extends \common\components\myModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'boiler';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_obj'], 'required'],
            [['id_obj', 'id_model', 'year_release', 'id_flue'], 'integer'],
            [['date_starting', 'id_type', 'lifetime'], 'safe'],
            [['serial_number'], 'string', 'max' => 30]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_obj' => 'Id Obj',
            'id_model' => 'Id Model',
            'year_release' => 'Year Release',
            'date_starting' => 'Date Starting',
            'serial_number' => 'Serial Number',
            'id_flue' => 'Id Flue',
            'id_type' => 'тип',
            'id_lifetime' => 'срок службы',
        ];
    }
	/*----------------------------------------------------------------------------------*/
	public function getId() {
		return (string)$this->id_obj;
	}
	/*----------------------------------------------------------------------------------*/
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdModel()
    {
        return $this->hasOne(BoilerSprModel::className(), ['id_model' => 'id_model']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdObj()
    {
        return $this->hasOne(Obj::className(), ['id_obj' => 'id_obj']);
    }

    /*-----------------------------------------------------------------------*/
	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getModel()
	{
		return $this->hasOne(BoilerSprModel::className(), ['id_model' => 'id_model']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getType()
	{
		return $this->hasOne(BoilerSprType::className(), ['id_type' => 'id_type']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getManufacturer()
	{
		return $this->hasOne(BoilerSprMake::className(), ['id_make' => 'id_make'])
			->via('model');
	}


	/**
	 * @return \yii\db\ActiveQuery
	 */

	/*----------------------------------------------------------------------*/
	public function fields()
	{
		$fields = parent::fields();

		$fields['id'] = function () {
			return (string)$this->id_obj;
		};
		unset($fields['id_obj']);

		$fields['type'] = function () {
			return [
				'id'=>$this->id_type,
				'type'=>$this->type->type,
			];
		};
		$fields['model'] = function () {
			return [
				'id'=>$this->id_model,
				'model'=>$this->model->model,
				'id_make'=>$this->model->id_make,
			];
		};
		$fields['manufacturer'] = function () {
			return [
				'id'=>-null,
				'manufacturer'=>null,
			];
		};
		$fields['diameter'] = function () {
			return [
				'id'=>null,
				'diameter'=>null,
			];
		};

//		unset($fields['id_type']);
//		unset($fields['id_model']);


		return $fields;
	}

	/*--------------------------------------*/
	static public function find()
	{
		return parent::find()->with(['model', 'type', 'manufacturer']);
	}


}
