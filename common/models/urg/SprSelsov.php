<?php

namespace common\models\urg;

use Yii;

/**
 * This is the model class for table "spr_selsov".
 *
 * @property integer $id_selsov
 * @property string $selsov
 */
class SprSelsov extends \common\components\myModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'spr_selsov';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['selsov'], 'required'],
            [['selsov'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_selsov' => 'Id Selsov',
            'selsov' => 'Selsov',
        ];
    }
	/*----------------------------------------------------------------------------------*/
	public function fields()
	{
		$fields = parent::fields();

		$fields['id']   = function () { return $this->id_selsov;};
		$fields['name'] = function () { return $this->selsov;};

		return $fields;
	}
	/*----------------------------------------------------------------------------------*/
}
