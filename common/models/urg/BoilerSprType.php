<?php

namespace common\models\urg;

use Yii;

/**
 * This is the model class for table "boiler_spr_type".
 *
 * @property integer $id_type
 * @property string $type
 */
class BoilerSprType extends \common\components\myModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'boiler_spr_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type'], 'required'],
            [['type'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_type' => 'Id Type',
            'type' => 'Type',
        ];
    }
}
