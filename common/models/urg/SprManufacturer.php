<?php

namespace common\models\urg;

use Yii;

/**
 * This is the model class for table "spr_manufacturer".
 *
 * @property integer $id_manufacturer
 * @property string $manufacturer
 * @property integer $id_country
 * @property string $address
 *
 * @property Filter[] $filters
 * @property Pc[] $pcs
 * @property Psk[] $psks
 * @property Pzk[] $pzks
 */
class SprManufacturer extends \common\components\myModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'spr_manufacturer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['manufacturer', 'id_country', 'address'], 'required'],
            [['id_country'], 'integer'],
            [['manufacturer', 'address'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_manufacturer' => 'Id Manufacturer',
            'manufacturer' => 'Manufacturer',
            'id_country' => 'Id Country',
            'address' => 'Address',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFilters()
    {
        return $this->hasMany(Filter::className(), ['id_manufacturer' => 'id_manufacturer']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPcs()
    {
        return $this->hasMany(Pc::className(), ['id_manufacturer' => 'id_manufacturer']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPsks()
    {
        return $this->hasMany(Psk::className(), ['id_manufacturer' => 'id_manufacturer']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPzks()
    {
        return $this->hasMany(Pzk::className(), ['id_manufacturer' => 'id_manufacturer']);
    }

/*-----------------------------------------------------------------------*/
	public function getId() {
		return $this->id_manufacturer;
	}

	public function fields()
	{
		//$fields = parent::fields();
		$fields['id'] = function () {
			return $this->id_manufacturer;
		};
		$fields['name'] = function () {
			return $this->manufacturer;
		};
//		$fields['country'] = function () {
//			return $this->id_country;
//		};
		return $fields;
	}

}
