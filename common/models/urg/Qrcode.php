<?php

namespace common\models\urg;

use Yii;

/**
 * This is the model class for table "qrcode".
 *
 * @property integer $id
 * @property string $qrcode
 * @property string $date_create
 * @property string $uid
 */
class Qrcode extends \common\components\myModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'qrcode';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['qrcode', 'date_create', 'uid'], 'required'],
            [['date_create'], 'safe'],
            [['qrcode'], 'string', 'max' => 34],
            [['uid'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'qrcode' => 'Qrcode',
            'date_create' => 'Date Create',
            'uid' => 'Uid',
        ];
    }
}
