<?php

namespace common\models\urg;

use Yii;

/**
 * This is the model class for table "pzk_spr_model".
 *
 * @property integer $id_model
 * @property string $model
 * @property integer $id_make
 *
 * @property Pzk[] $pzks
 * @property PzkSprMake $idMake
 */
class PzkSprModel extends \common\components\myModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pzk_spr_model';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['model', 'id_make'], 'required'],
            [['id_make'], 'integer'],
            [['model'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_model' => 'Id Model',
            'model' => 'Model',
            'id_make' => 'Id Make',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPzks()
    {
        return $this->hasMany(Pzk::className(), ['id_model' => 'id_model']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdMake()
    {
        return $this->hasOne(PzkSprMake::className(), ['id_make' => 'id_make']);
    }
}
