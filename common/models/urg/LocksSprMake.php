<?php

namespace common\models\urg;

use Yii;

/**
 * This is the model class for table "locks_spr_make".
 *
 * @property integer $id_make
 * @property string $make
 * @property integer $id_country
 * @property string $address
 */
class LocksSprMake extends \common\components\myModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'locks_spr_make';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['make', 'id_country', 'address'], 'required'],
            [['id_country'], 'integer'],
            [['make', 'address'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_make' => 'Id Make',
            'make' => 'Make',
            'id_country' => 'Id Country',
            'address' => 'Address',
        ];
    }
}
