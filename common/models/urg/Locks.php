<?php

namespace common\models\urg;

use Yii;

/**
 * This is the model class for table "locks".
 *
 * @property integer $id_obj
 * @property integer $id_type
 * @property integer $id_laying
 * @property string $l_ball
 * @property string $number
 * @property string $date_install
 * @property integer $id_type_install
 * @property string $type_install
 * @property integer $id_material
 * @property integer $id_diameter
 * @property string $date_dismantling
 * @property integer $id_type_compensator
 * @property string $year_production
 * @property integer $id_functionality
 * @property integer $id_sump
 * @property integer $id_worker
 * @property string $placement
 * @property integer $id_make
 * @property string $model
 * @property integer $lifetime
 * @property integer $id_owned
 * @property string $stock
 * @property string $period_to
 * @property integer $id_model
 */
class Locks extends \common\components\myModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'locks';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
	    return [
		    [['id_obj', 'id_type', 'id_laying', 'number', 'id_type_install', 'type_install', 'id_material', 'id_diameter', 'id_type_compensator', 'id_functionality', 'id_sump', 'id_worker', 'placement', 'id_make', 'id_owned'], 'required'],
		    [['id_obj', 'id_type', 'id_laying', 'id_type_install', 'id_material', 'id_diameter', 'id_type_compensator', 'id_functionality', 'id_sump', 'id_worker', 'id_make', 'lifetime', 'id_owned', 'id_model'], 'integer'],
		    [['l_ball', 'type_install', 'period_to'], 'string'],
		    [['date_install', 'date_dismantling', 'year_production', 'id_manufacturer'], 'safe'],
		    [['number'], 'string', 'max' => 20],
		    [['placement'], 'string', 'max' => 200],
		    [['model'], 'string', 'max' => 110],
		    [['stock'], 'string', 'max' => 100]
	    ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
	    return [
		    'id_obj' => 'Id Obj',
		    'id_type' => 'Id Type',
		    'id_laying' => 'Id Laying',
		    'l_ball' => 'L Ball',
		    'number' => 'Number',
		    'date_install' => 'Date Install',
		    'id_type_install' => 'Id Type Install',
		    'type_install' => 'Type Install',
		    'id_material' => 'Id Material',
		    'id_diameter' => 'Id Diameter',
		    'date_dismantling' => 'Date Dismantling',
		    'id_type_compensator' => 'Id Type Compensator',
		    'year_production' => 'Year Production',
		    'id_functionality' => 'Id Functionality',
		    'id_sump' => 'Id Sump',
		    'id_worker' => 'Id Worker',
		    'placement' => 'Placement',
		    'id_make' => 'Id Make',
		    'model' => 'Model',
		    'lifetime' => 'срок службы в годах',
		    'id_owned' => 'Id Owned',
		    'stock' => 'Stock',
		    'period_to' => 'период ТО в месяцах',
		    'id_model' => 'Id Model',
	    ];
    }


	/*----------------------------------------------------------------------------------*/
	public function init()
	{
		parent::init();

		$this->id_functionality     = 0;
		$this->id_laying            = 2;
		$this->id_make              = -2;
		$this->id_material          = 1;
		$this->id_owned             = 0;
		$this->id_sump              = 0;
		$this->id_type_compensator  = 0;
		$this->id_type_install      = 0;
		$this->id_worker            = 0;
		$this->model                = '-';
		$this->number               = '-';
		$this->placement            = '-';
		$this->type_install         = 'F';
	}

	/*----------------------------------------------------------------------------------*/
	public function getId() {
		return (string)$this->id_obj;
	}
	/*----------------------------------------------------------------------------------*/
	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getModel2()
	{
		return $this->hasOne(LocksSprModel::className(), ['id_model' => 'id_model']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getType()
	{
		return $this->hasOne(LocksSprType::className(), ['id_type' => 'id_type']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getManufacturer()
	{
		return $this->hasOne(SprManufacturer::className(), ['id_manufacturer' => 'id_manufacturer']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getDiameter()
	{
		return $this->hasOne(TubeSprDiameter::className(), ['id_diameter' => 'id_diameter']);
	}
	/*--------------------------------------*/
	public function getDate_starting() {
		return $this->date_install;
	}
	/*------------------------------------*/

	public function fields()
	{
		$fields = parent::fields();

		$fields['id'] = function () {
			return (string)$this->id_obj;
		};
		unset($fields['id_obj']);

		$fields['diameter'] = function () {
			return [
				'id'=>$this->id_diameter,
				'diameter'=>$this->diameter->diameter,
			];
		};
		$fields['type'] = function () {
			return [
				'id'=>$this->id_type,
				'type'=>$this->type->title,
			];
		};
		$fields['model'] = function () {
			return [
				'id'=>$this->id_model,
				'model'=>$this->model2->model,
			];
		};
		$fields['manufacturer'] = function () {
			return [
				'id'=>$this->id_manufacturer,
				'manufacturer'=>$this->manufacturer->manufacturer,
			];
		};
		$fields['date_starting'] = function () {
			return $this->date_install;
		};
		$fields['serial_number'] = function () {
			return $this->number;
		};
		$fields['year_release'] = function () {
			return $this->year_production;
		};

		unset($fields['year_production']);
		unset($fields['date_install']);
		unset($fields['number']);
//		unset($fields['id_diameter']);
//		unset($fields['id_model']);
//		unset($fields['id_make']);
//		unset($fields['id_type']);

		return $fields;
	}

	/*--------------------------------------*/
//	static public function find() {
//		return parent::find()->with(['diameter', 'manufacturer', 'model2', 'type']);
//	}

}
