<?php

namespace common\models\urg;

use Yii;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "workers".
 *
 * @property integer $id_worker
 * @property string $family
 * @property string $imya
 * @property string $otch
 * @property integer $id_post
 * @property string $id_unit
 * @property string $login
 * @property string $password
 * @property string $token
 * @property integer $token_created
 */
class Workers extends \common\components\myModel implements IdentityInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'workers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['family', 'imya', 'otch', 'id_post', 'id_unit', 'login', 'password'], 'required'],
            [['id_post', 'token_created'], 'integer'],
            [['family'], 'string', 'max' => 40],
            [['imya', 'otch', 'login'], 'string', 'max' => 28],
            [['token'], 'string', 'max' => 32],
            [['password'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_worker' => 'Id Worker',
            'family' => 'Family',
            'imya' => 'Imya',
            'otch' => 'Otch',
            'id_post' => 'Id Post',
            'id_unit' => 'Id Unit',
            'login' => 'Login',
            'password' => 'Password',
            'token' => 'token',
            'token_created' => 'token_created',
        ];
    }

    /*-----------------------------------------------*/
    public function getUsername() {
    	return $this->login;
    }
    /*-----------------------------------------------*/
    public function getRoles() {
    	return $this->hasOne(WorkerConfvalue::className(), ['id_worker'=>'id_worker']);
    }
    /*-----------------------------------------------*/
    public function fields()
    {
//    	if (!isset(Yii::$app->session['tmp']) ) {
//		    Yii::$app->session['tmp'] = date('H:i:s');
//	    }

    	$fields = parent::fields();;
		$fields['id'] = function () {
			return $this->id_worker;
		};
	    $fields['id_unit'] = function () {
		    return (string)$this->id_unit;
	    };
		$fields['fio'] = function () {
			return $this->family.' '.$this->imya.' '.$this->otch;
		};
	    $fields['roles'] = function () {
		    return trim($this->roles->confvalue_urg)!='' ? explode('_',$this->roles->confvalue_urg) : false;
	    };
//		$fields['session'] = function () {
//			//return Yii::$app->session['tmp'];
//			return print_r($_SESSION, true);
//		};
	    unset(
	    	$fields['id_worker'],
	    	$fields['family'],
	    	$fields['imya'],
	    	$fields['otch'],
	    	$fields['password']
	    );

	    return $fields; //
    }

    /*----------------------------------------------------------------------*/
    /*----------------------------------------------------------------------*/

	public static function findIdentity($id) {
		//echo '------------------'.$id;
		return Workers::findOne($id);
	}

	/**
	 * Finds an identity by the given token.
	 * @param mixed $token the token to be looked for
	 * @param mixed $type the type of the token. The value of this parameter depends on the implementation.
	 * For example, [[\yii\filters\auth\HttpBearerAuth]] will set this parameter to be `yii\filters\auth\HttpBearerAuth`.
	 * @return IdentityInterface the identity object that matches the given token.
	 * Null should be returned if such an identity cannot be found
	 * or the identity is not in an active state (disabled, deleted, etc.)
	 */
	public static function findIdentityByAccessToken($token, $type = null) {
		return Workers::findOne(['token'=>$token]);
	}

	/**
	 * Returns an ID that can uniquely identify a user identity.
	 * @return string|integer an ID that uniquely identifies a user identity.
	 */
	public function getId() {
		return $this->id_worker;
	}

	/**
	 * Returns a key that can be used to check the validity of a given identity ID.
	 *
	 * The key should be unique for each individual user, and should be persistent
	 * so that it can be used to check the validity of the user identity.
	 *
	 * The space of such keys should be big enough to defeat potential identity attacks.
	 *
	 * This is required if [[User::enableAutoLogin]] is enabled.
	 * @return string a key that is used to check the validity of a given identity ID.
	 * @see validateAuthKey()
	 */
	public function getAuthKey() {
		return Yii::$app->session->get('asses_token');
	}

	/**
	 * Validates the given auth key.
	 *
	 * This is required if [[User::enableAutoLogin]] is enabled.
	 * @param string $authKey the given auth key
	 * @return boolean whether the given auth key is valid.
	 * @see getAuthKey()
	 */
	public function validateAuthKey($authKey) {
		return true;
	}
}
