<?php

namespace common\models\urg;

use Yii;

/**
 * This is the model class for table "boiler_spr_model".
 *
 * @property integer $id_model
 * @property string $model
 * @property integer $id_make
 *
 * @property Boiler[] $boilers
 * @property BoilerSprMake $idMake
 */
class BoilerSprModel extends \common\components\myModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'boiler_spr_model';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['model', 'id_make'], 'required'],
            [['id_make'], 'integer'],
            [['model'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_model' => 'Id Model',
            'model' => 'Model',
            'id_make' => 'Id Make',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBoilers()
    {
        return $this->hasMany(Boiler::className(), ['id_model' => 'id_model']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdMake()
    {
        return $this->hasOne(BoilerSprMake::className(), ['id_make' => 'id_make']);
    }
}
