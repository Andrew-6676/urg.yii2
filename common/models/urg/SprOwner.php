<?php

namespace common\models\urg;

use Yii;

/**
 * This is the model class for table "tube_spr_owned".
 *
 * @property integer $id_owned
 * @property string $owned
 * @property string $l_outside
 */
class SprOwner extends \common\components\myModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tube_spr_owned';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['owned', 'l_outside'], 'required'],
            [['l_outside'], 'string'],
            [['owned'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_owned' => 'Id Owned',
            'owned' => 'Owned',
            'l_outside' => 'L Outside',
        ];
    }

	/*--------------------------------------------------------------------------*/

	public function getId() {
		return $this->id_owned;
	}
	public function getName() {
		return $this->owned;
	}
	/*--------------------------------------------------------------------------*/
	public function fields() {
		//$fields = parent::fields();
		$fields['id'] = function() {
			return (string)$this->id_owned;
		};
		$fields['name'] = function() {
			return $this->owned;
		};
		return $fields;
	}
	/*--------------------------------------------------------------------------*/
	/*--------------------------------------------------------------------------*/
}
