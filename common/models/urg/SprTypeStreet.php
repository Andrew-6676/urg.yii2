<?php

namespace common\models\urg;

use Yii;

/**
 * This is the model class for table "spr_type_street".
 *
 * @property integer $id_type_street
 * @property string $type_street
 * @property string $short_type
 */
class SprTypeStreet extends \common\components\myModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'spr_type_street';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_street', 'short_type'], 'required'],
            [['type_street', 'short_type'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_type_street' => 'Id Type Street',
            'type_street' => 'Type Street',
            'short_type' => 'Short Type',
        ];
    }
}
