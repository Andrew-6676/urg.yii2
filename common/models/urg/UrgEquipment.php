<?php

namespace common\models\urg;

use Yii;
use yii\db\Query;

/**
 * This is the model class for table "urg_equipment".
 *
 * @property integer $id_obj
 * @property integer $id_eq_type
 * @property string $eq_table
 * @property string $mount_point
 * @property string $schema_number
 * @property string $descr
 * @property integer $order
 *
 * @property Obj $idObj
 * @property SprTypeObj $idEqType
 */
class UrgEquipment extends \common\components\myModel
{

	static public $spr_type_obj = [
		1=> [
			'type_obj' => 'Котел',
			'name_table' => 'boiler',
		],
		4=>[
			'type_obj' => 'Запорное устройство',
			'name_table' => 'locks',
		],
		31=>[
			'type_obj' => 'счетчик',
			'name_table' => 'meter',
		],
		32=>[
			'type_obj' => 'РД',
			'name_table' => 'pc',
		],
		41=>[
			'type_obj' => 'пломба',
			'name_table' => 'plomba',
		],
		80=>[
			'type_obj' => 'Фильтр',
			'name_table' => 'filter',
		],
		81=>[
			'type_obj' => 'ПЗК',
			'name_table' => 'pzk',
		],
		82=>[
			'type_obj' => 'ПСК',
			'name_table' => 'psk',
		],
	];


	/**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'urg_equipment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
	    return [
		    [['id_obj', 'id_eq_type', 'mount_point'], 'required'],
		    [['id_obj', 'id_eq_type', 'order'], 'integer'],
		    [['mount_point', 'schema_number', 'eq_table'], 'string', 'max' => 20],
		    [['descr'], 'string', 'max' => 255]
	    ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
	    return [
		    'id_obj' => 'Id Obj',
		    'id_eq_type' => 'Вид оборудования',
		    'eq_table' => 'Таблица с оборудованием',
		    'mount_point' => 'Место установки',
		    'schema_number' => 'Номер на схеме',
		    'descr' => 'Примечание',
		    'order' => 'Сортировка',
	    ];
    }
	/*----------------------------------------------------------------------------------*/
	public function getId() {
		return (string)$this->id_obj;
	}
	/*----------------------------------------------------------------------------------*/
		// link_obj - если УРГ чей-то родитель
	public function getLinkObjAsParent() {
		return $this->hasMany(LinkObj::className(), ['id_obj1' => 'id_obj']);
	}
	/*----------------------------------------------------------------------------------*/
		// файлы
	public function getFiles() {
		return $this->hasMany(File::className(), ['id_obj' => 'id_obj2'])
			->via('linkObjAsParent');
	}
	/*----------------------------------------------------------------------------------*/

	/*----------------------------------------------------------------------------------*/
	public function getData() {
			// загружаем данные по оборудованию из нужной таблицы
		$table = self::$spr_type_obj[$this->id_eq_type]['name_table'];
		//$table = $this->eq_table;
		$fc = mb_strtoupper(mb_substr($table, 0, 1));
		$class_name = 'common\models\urg\\'.$fc.mb_substr($table, 1);
		return $this->hasOne($class_name, ['id_obj' => 'id_obj']);
	}
//	public function getFilter() {
//		// загружаем данные по оборудованию из нужной таблицы
//		$table = self::$spr_type_obj[$this->id_eq_type]['name_table'];
//		$fc = mb_strtoupper(mb_substr($table, 0, 1));
//		$class_name = 'common\models\urg\\'.$fc.mb_substr($table, 1);
//		return $this->hasOne($class_name, ['id_obj' => 'id_obj']); //->with(['manufacturer3']);
//	}
	/*----------------------------------------------------------------------------------*/
	static public function add($urg, $data) {
		$res = [
			'id_urg' => $urg,
			'status' => 'unknown',
			'message'=> '-',
		];

		// добавляем оборудование в БД
		$id_obj = Obj::addEquipment();
		$eq = new UrgEquipment();
		$eq->id_obj  = $id_obj;
		$eq->load($data, '');


		if ($eq->save() !== false) {
			if (LinkObj::add($urg, $id_obj)) {
				$res['status'] = 'ok';
				$res['message'] = 'Оборудование добавлено успешно';
				$res['id'] = $eq->id;
			} else {
				$res['status'] = 'error';
				$res['message'] = 'Во время сохранения связи с оборудованием возникла ошибка.';
				$res['errors'] = $eq->errors;
			}

		} else {
			// если новое оборудование не создалось - удаляем объект из БД
			Obj::del($id_obj);
			$res['status'] = 'error';
			$res['message'] = 'Во время сохранения оборудования возникла ошибка.';
			$res['errors'] = $eq->errors;
		}


//			// сохраняем в БД
//		if ($eq->save()) {
//			// добавляем связь файла с УРГ
//			return (LinkObj::add($urg, $id_obj));
//		} else {
//			return false;
//		}

		return $res;
	}
	/*----------------------------------------------------------------------------------*/
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdObj()
    {
        return $this->hasOne(Obj::className(), ['id_obj' => 'id_obj']);
    }
	/*----------------------------------------------------------------------------------*/
    /**
     * @return \yii\db\ActiveQuery
     */
//    public function getIdEqType() {
//        return $this->hasOne(UrgRefbook::className(), ['id' => 'id_eq_type']);
//    }
	/*----------------------------------------------------------------------------------*/
	// link_obj - если оборудование чей-то дети
	public function getLinkObjAsChild() {
		return $this->hasOne(LinkObj::className(), ['id_obj2' => 'id_obj']);
	}
	/*----------------------------------------------------------------------------------*/
	/*----------------------------------------------------------------------------------*/
		// УРГ-хозяин текущего оборудования
	public function getAddress() {
		return $this->hasOne(Urg::className(), ['id_obj' => 'id_obj1'])
			->via('linkObjAsChild');
	}
	/*----------------------------------------------------------------------------------*/
	public function getQrCode() {
		return $this->hasOne(ObjQrcode::className(), ['id_obj' => 'id_obj']);
	}
	/*----------------------------------------------------------------------------------*/
	public function delete() {
		// Удалить обхект полностью: из link_obj, urg_equipment и obj
		// TODO: удалить и прикреплённые файлы
		return Obj::del($this->id);
	}
	/*----------------------------------------------------------------------------------*/
	/*----------------------------------------------------------------------------------*/
	/*----------------------------------------------------------------------------------*/
	public function fields() {
		$fields = parent::fields();
		$fields['id'] = function () {
			return (string)$this->id_obj;
		};

//			$fields['id_obj1'] = function () {
//				return $this->linkObjAsChild->id_obj1;
//			};
		unset($fields['id_obj']);
		return $fields;
	}
	/*----------------------------------------------------------------------------------*/
	public function extraFields() {
		$fields = parent::extraFields();

		$fields['files'] = function() {
			return $this->files;
		};

		$fields['qr_code'] = function () {
			return $this->qrCode->qrcode;
		};

		$fields['data'] = function () {
			//$table = self::$spr_type_obj[$this->id_eq_type]['name_table'];
			//return $this->{$table};
			return $this->data;
		};
		return $fields;
	}
}
