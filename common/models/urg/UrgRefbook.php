<?php

namespace common\models\urg;

use Yii;

/**
 * This is the model class for table "urg_refbook".
 *
 * @property integer $id
 * @property integer $id_parent
 * @property string $name
 * @property string $descr
 */
class UrgRefbook extends \common\components\myModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'urg_refbook';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
          //  [['id'], 'required'],
            [['id', 'id_parent'], 'integer'],
            [['name'], 'string', 'max' => 100],
            [['descr'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_parent' => 'Id Parent',
            'name' => 'Name',
            'descr' => 'Descr',
        ];
    }

    public static function find()
    {
	    $sql = parent::find();
	    //$sql = $sql->where([self::tableName().'.deleted'=>false]);
	    return $sql;
    }

	protected static function createQuery($params) {
		$sql = parent::createQuery($params);
		//$sql = $sql->andWhere([self::tableName().'.deleted'=>false]);
		return $sql;
	}
}
