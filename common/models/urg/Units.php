<?php

namespace common\models\urg;

use common\components\myModel;
use Yii;

/**
 * This is the model class for table "units" - облгазы и РГСы.
 *
 * @property integer $id_obj
 * @property string $unit
 * @property string $l_inspection
 * @property string $l_area
 */
class Units extends myModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'units';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_obj', 'unit'], 'required'],
            [['id_obj'], 'integer'],
            [['l_inspection', 'l_area', 'id_gasb'], 'string'],
            [['unit'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_obj' => 'Id Obj',
            'unit' => 'Unit',
            'l_inspection' => 'L Inspection',
            'l_area' => 'L Area',
	        'id_gasb' => 'id_gasb'
        ];
    }
    /*----------------------------------------------------------------------------------*/
    public function getObj() {
	    return $this->hasOne(Obj::className(), ['id_obj' => 'id_obj']);
    }
    /*----------------------------------------------------------------------------------*/
	// link_obj - если unit чей-то дети
	public function getLinkObjAsChild() {
		return $this->hasOne(LinkObj::className(), ['id_obj2' => 'id_obj']);
	}
	/*----------------------------------------------------------------------------------*/
	// link_obj - если unit чей-то родитель
	public function getLinkObjAsParent() {
		return $this->hasMany(LinkObj::className(), ['id_obj1' => 'id_obj']);
	}
	/*----------------------------------------------------------------------------------*/
	// Подчинённые юниты
	public function getLinkUnitAsParent() {
		return $this->hasMany(LinkUnit::className(),  ['parent' => 'id_obj']);
	}
    /*----------------------------------------------------------------------------------*/
	public function getSubUnits() {
		return $this->hasMany(Units::className(),  ['id_obj' => 'child'])
			//->joinWith('obj')
			//->where('id_type_obj=3')
			->via('linkUnitAsParent');
	}
    /*----------------------------------------------------------------------------------*/
    public function fields()
    {
        $fields = parent::fields();
		//unset($fields['id_obj']);
		//unset($fields['unit']);

	    $fields['id'] = function () { return $this->id_obj;};
        $fields['name'] = function () { return $this->unit;};

        return $fields;
    }
    /*----------------------------------------------------------------------------------*/
    /*----------------------------------------------------------------------------------*/
    public function extraFields()
    {
        $extraFields = parent::extraFields();

	    $extraFields['subUnits'] = function() {
		    $res = [];

		    foreach ($this->subUnits as $subUnit) {
			    $subSub = [];
			    foreach ($subUnit->subUnits as $subSubUnit) {
				    $subSub[] = [
					    'id'   => $subSubUnit->id_obj,
					    'name' => $subSubUnit->unit,
				    ];
			    }
			    $res[] = [
			    	'id'   => $subUnit->id_obj,
				    'name' => $subUnit->unit,
				    'rgs'  => $subSub,
			    ];
		    }
		    return $res;
	    };

        return $extraFields;
    }
}
