<?php

namespace common\models\urg;

use Yii;

/**
 * This is the model class for table "locks_spr_type".
 *
 * @property integer $id_type
 * @property string $type
 * @property string $title
 * @property integer $period_to
 */
class LocksSprType extends \common\components\myModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'locks_spr_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'title', 'period_to'], 'required'],
            [['period_to'], 'integer'],
            [['type'], 'string', 'max' => 20],
            [['title'], 'string', 'max' => 30]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_type' => 'Id Type',
            'type' => 'Type',
            'title' => 'Title',
            'period_to' => 'период между ТО в днях',
        ];
    }
}
