<?php

namespace common\models\urg;

use Yii;

/**
 * This is the model class for table "meter".
 *
 * @property integer $id_obj
 * @property integer $id_model
 * @property integer $year_release
 * @property string $date_starting
 * @property string $serial_number
 * @property integer $id_flue
 * @property string $range_uncer
 * @property string $reestr
 * @property integer $id_manufacturer
 * @property integer $id_diameter
 * @property integer $lifetime
 *
 * @property MeterSprModel $idModel
 * @property Obj $idObj
 */
class Meter extends \common\components\myModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'meter';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_obj'], 'required'],
            [['id_obj', 'id_model', 'year_release', 'id_flue', 'id_manufacturer', 'id_diameter', 'lifetime', 'id_type'], 'integer'],
            [['date_starting'], 'safe'],
            [['range_uncer'], 'number'],
            [['serial_number'], 'string', 'max' => 30],
            [['reestr'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_obj' => 'Id Obj',
            'id_model' => 'Id Model',
            'year_release' => 'Year Release',
            'date_starting' => 'Date Starting',
            'serial_number' => 'Serial Number',
            'id_flue' => 'Id Flue',
            'range_uncer' => 'Range Uncer',
            'reestr' => 'Reestr',
            'id_manufacturer' => 'производитель',
            'id_diameter' => 'диаметр',
            'lifetime' => 'срок службы',
        ];
    }
	/*----------------------------------------------------------------------------------*/
	public function init()
	{
		parent::init();

		$this->range_uncer = 0;
		$this->reestr = '-';
	}
	/*----------------------------------------------------------------------------------*/
	public function getId() {
		return (string)$this->id_obj;
	}
	/*----------------------------------------------------------------------------------*/

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdObj()
    {
        return $this->hasOne(Obj::className(), ['id_obj' => 'id_obj']);
    }

    /*-----------------------------------------------------------*/
	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getModel()
	{
		return $this->hasOne(MeterSprModel::className(), ['id_model' => 'id_model']);
	}


	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getManufacturer()
	{
		return $this->hasOne(SprManufacturer::className(), ['id_manufacturer' => 'id_manufacturer']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getDiameter()
	{
		return $this->hasOne(TubeSprDiameter::className(), ['id_diameter' => 'id_diameter']);
	}

	/*------------------------------------*/

	public function fields()
	{
		$fields = parent::fields();

		$fields['id'] = function () {
			return (string)$this->id_obj;
		};
		unset($fields['id_obj']);

		$fields['diameter'] = function () {
			return [
				'id'=>$this->id_diameter,
				'diameter'=>$this->diameter->diameter,
			];
		};
		$fields['model'] = function () {
			return [
				'id'=>$this->id_model,
				'model'=>$this->model->model,
				'id_make'=>$this->model->id_make,
			];
		};
		$fields['manufacturer'] = function () {
			return [
				'id'=>$this->id_manufacturer,
				'manufacturer'=>$this->manufacturer->manufacturer,
			];
		};
		$fields['type'] = function () {
			return [
				'id'=>null,
				'type'=>null,
			];
		};

//		unset($fields['id_diameter']);
//		unset($fields['id_model']);
//		unset($fields['id_manufacturer']);

		return $fields;
	}

	/*--------------------------------------*/
	static public function find()
	{
		return parent::find()->with(['diameter', 'manufacturer', 'model']);
	}
}
