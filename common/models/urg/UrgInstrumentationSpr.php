<?php

namespace common\models\urg;

use Yii;

/**
 * This is the model class for table "urg_instrumentation_spr".
 *
 * @property integer $id
 * @property integer $id_parent
 * @property string $name
 */
class UrgInstrumentationSpr extends \common\components\myModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'urg_instrumentation_spr';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_parent'], 'integer'],
            [['name'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_parent' => 'Id Parent',
            'name' => 'Name',
        ];
    }
}
