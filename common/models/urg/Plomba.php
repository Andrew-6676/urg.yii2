<?php

namespace common\models\urg;

use Yii;

/**
 * This is the model class for table "plomba".
 *
 * @property integer $id_obj
 * @property string $number
 */
class Plomba extends \common\components\myModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'plomba';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_obj', 'number'], 'required'],
            [['id_obj'], 'integer'],
            [['number'], 'string', 'max' => 20],
            [['id_obj'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_obj' => 'Id Obj',
            'number' => 'Number',
        ];
    }
}
