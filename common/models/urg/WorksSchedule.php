<?php

namespace common\models\urg;

use Yii;

/**
 * This is the model class for table "works_schedule".
 *
 * @property integer $id
 * @property string $type
 * @property string $title
 * @property string $objects
 * @property string $period
 * @property string $data
 * @property string $id_unit
 * @property string $author
 * @property string $post
 */
class WorksSchedule extends \common\components\myModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'works_schedule';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'objects', 'period', 'data', 'id_unit'], 'required'],
            [['type', 'objects', 'data', 'post'], 'string'],
            [['id_unit'], 'integer'],
            [['period'], 'safe'],
            [['type'], 'string', 'max' => 1],
            [['title', 'author', 'post'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'пользовательский заголовок',
            'objects' => 'список объектов',
            'period' => 'период в графике',
            'data' => 'точки в графике',
        ];
    }

    /*-------------------------------------------------------------------------*/
    public function getData() {
    	return json_decode($this->data);
    }
	/*-------------------------------------------------------------------------*/
	public function getObjects() {
		return json_decode($this->data);
	}
	/*-------------------------------------------------------------------------*/
	public function fields() {
		$fields = parent::fields();

		$fields['objects'] = function () {
			return json_decode($this->objects);
		};

		$fields['data'] = function () {
			return json_decode($this->data);
		};

		return $fields;
	}
	/*-------------------------------------------------------------------------*/
	public function extraFields() {
		$fields = parent::extraFields();

		$fields['obj_count'] = function () {
			return count(json_decode($this->objects));
		};

		return $fields;
	}
}
