<?php

namespace common\models\urg;

use app\modules\printer\models\UrgReports;
use Yii;

/**
 * This is the model class for table "urg".
 *
 * @property integer $id_obj
 * @property integer $id_type_urg
 * @property integer $num_unique
 * @property integer $id_unit
 * @property string $num
 * @property integer $num_sort
 * @property string $num_archiv
 * @property string $num_inventory
 * @property string $num_inventory_eq
 * @property string $address_descr
 * @property string $date_acceptance
 * @property string $date_starting
 * @property string $date_startup_gas
 * @property string $gps
 * @property string $osm_id
 * @property integer $id_telemetry
 * @property integer $id_building_type
 * @property integer $id_power_supply
 * @property integer $id_light
 * @property integer $id_manufacturer
 * @property integer $id_lighting_protection
 * @property string $lighting_protection_resistance
 * @property integer $id_heating
 * @property integer $id_communication
 * @property integer $id_floor_material
 * @property integer $id_owner
 * @property integer $barrier
 * @property integer $id_extinguishing
 * @property integer $id_ventilation
 * @property double $dist_enter
 * @property double $dist_exit
 * @property string $calc_capacity
 * @property string $descr
 * @property boolean $deleted
 * @property double $pressure_in_1
 * @property double $pressure_in_2
 * @property double $pressure_in_3
 * @property double $pressure_in_4
 * @property double $pressure_out_1
 * @property double $pressure_out_2
 * @property double $pressure_out_3
 * @property double $pressure_out_4
 * @property string $periodicity
 */
class Urg extends \common\components\myModel {
	const SCENARIO_VIEW = 'view';
	const SCENARIO_EDIT = 'edit';

	public $id_ring;
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'urg';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
	        [['id_obj', 'date_acceptance', 'date_starting', 'gps'], 'required'],
	        [['id_obj', 'id_type_urg', 'num_sort', 'num_unique', 'id_unit', 'osm_id', 'id_telemetry', 'id_building_type', 'id_power_supply', 'id_light', 'id_manufacturer', 'id_lighting_protection', 'id_heating', 'id_communication', 'id_floor_material', 'id_owner', 'id_extinguishing', 'id_ventilation'], 'integer'],
	        [['date_acceptance', 'date_starting', 'date_startup_gas', 'barrier'], 'safe'],
	        //[['gps'], 'string'],
	        [['dist_enter', 'dist_exit', 'calc_capacity', 'pressure_in_1', 'pressure_in_2', 'pressure_in_3', 'pressure_in_4', 'pressure_out_1', 'pressure_out_2', 'pressure_out_3', 'pressure_out_4'], 'number'],
	        [['deleted'], 'boolean'],
	        [['num_archiv'], 'string', 'max' => 25],
	        [['num'], 'string', 'max' => 10],
	        [['num_inventory', 'num_inventory_eq'], 'string', 'max' => 50],
	        [['address_descr'], 'string', 'max' => 100],
	        [['lighting_protection_resistance'], 'string', 'max' => 10],
	        [['descr', 'periodicity'], 'string', 'max' => 255],
	        [['id_obj'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
	        'id_obj' => 'Id Obj',
	        'id_type_urg' => 'Вид УРГ',
	        'num_unique' => 'Уникальный номер УРГ',
	        'id_unit' => 'чей УРГ',
	        'num' => 'Номер УРГ',
	        'num_sort' => 'Номер для сортировки',
	        'num_archiv' => 'Архивный номер',
	        'num_inventory' => 'Инвентарный номер здания',
	        'num_inventory_eq' => 'Инвентарный номер оборудования',
	        'address_descr' => 'Примечание к адресу',
	        'date_acceptance' => 'Дата приёмки',
	        'date_starting' => 'Дата ввода в эксплуатацию',
	        'date_startup_gas' => 'Дата первичного пуска газа',
	        'gps' => 'GPS координаты',
	        'id_telemetry' => 'Телеметрия',
	        'id_building_type' => 'Вид здания',
	        'id_power_supply' => 'Электроснабжение',
	        'id_light' => 'Система освещения',
	        'id_manufacturer' => 'Изготовитель',
	        'id_lighting_protection' => 'Тип молниезащиты',
	        'lighting_protection_resistance' => 'Сопротивление молниезащиты',
	        'id_heating' => 'Отопление',
	        'id_communication' => 'Система связи',
	        'id_floor_material' => 'Материал пола',
	        'id_owner' => 'Балансовая принадлежность',
	        'barrier' => 'Наличие ограждения',
	        'id_extinguishing' => 'Система пожаротушения',
	        'id_ventilation' => 'Вентиляция',
	        'dist_enter' => 'Расстояние до наружного отключающего устройства на входе',
	        'dist_exit' => 'Расстояние до наружного отключающего устройства на выходе',
	        'master' => 'Ответственнон лицо',
	        'calc_capacity' => 'Расчётная пропускная способность',
	        'descr' => 'Примечание',
	        'deleted' => 'Пометка на удаление',
	        'pressure_in_1' => 'Давление на входе 1',
	        'pressure_in_2' => 'Давление на входе 2',
	        'pressure_out_1' => 'Давление на выходе 1',
	        'pressure_out_2' => 'Давление на выходе 2',
	        'periodicity'    => 'индивидуальная переодичность обходов',
        ];
    }
	/*----------------------------------------------------------------------------------*/
//	public function scenarios()	{
//		return [
//			self::SCENARIO_VIEW => ['location'],
//			self::SCENARIO_EDIT => ['username', 'email', 'password'],
//		];
//	}
	/*----------------------------------------------------------------------------------*/
	public function delete() {
			// TODO  -  как быть с адресом?
			// удалить всё оборудование
		$this->clearEquipment();
			// удалить файлы вместе с УРГ
		$this->clearFiles();
			// Удалить обхект полностью: из link_obj, urg и obj (кольца и прочее удалится каскадом)
		return Obj::del($this->id);
	}
	/*----------------------------------------------------------------------------------*/

	public function getId() {
		return (string)$this->id_obj;
	}
	public function getName() {
		return $this->type->name . $this->num;
	}
	/*----------------------------------------------------------------------------------*/
		// link_obj - если УРГ чей-то дети
	public function getLinkObjAsChild() {
		return $this->hasOne(LinkObj::className(), ['id_obj2' => 'id_obj']);
	}
	/*----------------------------------------------------------------------------------*/
		// link_obj - если УРГ чей-то родитель
	public function getLinkObjAsParent() {
		return $this->hasMany(LinkObj::className(), ['id_obj1' => 'id_obj']);
	}
	/*----------------------------------------------------------------------------------*/
		// получаем UNIT-родителя для текущеко УРГ ()
	public function getLinkUnit() {
		return $this->hasOne(LinkUnit::className(),  ['child' => 'id_obj1'])
			->via('linkObjAsChild', function ($query){
				return $query->where(['id_type_obj2'=>1]);
			});
	}
	/*----------------------------------------------------------------------------------*/
		// производитель
	public function getManufacturer() {
		return $this->hasOne(UrgRefbook::className(), ['id' => 'id_manufacturer'])
			->select(['id', 'name']);
	}
	/*----------------------------------------------------------------------------------*/
		// телеметрия
	public function getTelemetry() {
		return $this->hasOne(UrgRefbook::className(), ['id' => 'id_telemetry'])
			->select(['id', 'name']);
	}
	/*----------------------------------------------------------------------------------*/
		// тип узла
	public function getType() {
		return $this->hasOne(UrgRefbook::className(), ['id' => 'id_type_urg'])
			->select(['id', 'name']);
	}
	/*----------------------------------------------------------------------------------*/
		// тип здания
	public function getBuildingType() {
		return $this->hasOne(UrgRefbook::className(), ['id' => 'id_building_type'])
			->select(['id', 'name']);
	}
	/*----------------------------------------------------------------------------------*/
		// материфл пола
	public function getFloorMaterial() {
		return $this->hasOne(UrgRefbook::className(), ['id' => 'id_floor_material'])
			->select(['id', 'name']);
	}
	/*----------------------------------------------------------------------------------*/
		// адрес
	public function getAddress() {
		return $this->hasOne(Address::className(), ['id_obj' => 'id_obj2'])
					->via('linkObjAsParent', function ($query){
						return $query->where(['id_type_obj2'=>5]);
					});
	}
	/*----------------------------------------------------------------------------------*/
		// файлы
	public function getFiles() {
		return $this->hasMany(File::className(), ['id_obj' => 'id_obj2'])
			->via('linkObjAsParent', function ($query){
				return $query->where(['id_type_obj2'=>2]);
			});
	}
	/*----------------------------------------------------------------------------------*/
	public function getInOneRing() {
		return $this->hasMany(Ring::className(), ['id_ring' => 'id_ring'])
			//->select('id_urg')
			->via('ring')
			->where('id_urg<>'.$this->id);
	}
	/*----------------------------------------------------------------------------------*/
	public function getRing() {
		return $this->hasOne(Ring::className(), ['id_urg' => 'id_obj']);
	}
	/*----------------------------------------------------------------------------------*/
		// закользованные c текущим УРГ
	public function getRingInUrg() {
		return $this->hasMany(Ring::className(), ['id_ring' => 'id_ring'])
			->select('id_urg')
			->via('ring')
			->where('id_urg<>'.$this->id)
			->column();
	}
	/*----------------------------------------------------------------------------------*/
		// отопление
	public function getHeating() {
		return $this->hasOne(UrgRefbook::className(), ['id' => 'id_heating'])
			->select(['id', 'name']);
	}
	/*----------------------------------------------------------------------------------*/
		// пожаротушение
	public function getExtinguishing() {
		return $this->hasOne(UrgRefbook::className(), ['id' => 'id_extinguishing'])
			->select(['id', 'name']);
	}
	/*----------------------------------------------------------------------------------*/
		// вентиляция
	public function getVentilation() {
		return $this->hasOne(UrgRefbook::className(), ['id' => 'id_ventilation'])
			->select(['id', 'name']);
	}
	/*----------------------------------------------------------------------------------*/
		// связь
	public function getCommunication() {
		return $this->hasOne(UrgRefbook::className(), ['id' => 'id_communication'])
			->select(['id', 'name']);
	}
	/*----------------------------------------------------------------------------------*/
			// молниезащита
	public function getLightingprotection() {
		return $this->hasOne(UrgRefbook::className(), ['id' => 'id_lighting_protection'])
			->select(['id', 'name']);
	}
	/*----------------------------------------------------------------------------------*/
		// электричество
	public function getPower_supply() {
		return $this->hasOne(UrgRefbook::className(), ['id' => 'id_power_supply'])
			->select(['id', 'name']);
	}
	/*----------------------------------------------------------------------------------*/
		// освещение
	public function getLight() {
		return $this->hasOne(UrgRefbook::className(), ['id' => 'id_light'])
			->select(['id', 'name']);
	}
	/*----------------------------------------------------------------------------------*/
		// Балансовая принадлежность
	public function getOwner() {
		return $this->hasOne(SprOwner::className(), ['id_owned' => 'id_owner']);
	}
	/*----------------------------------------------------------------------------------*/
		// Помещения
	public function getRooms() {
		return $this->hasMany(UrgRoom::className(), ['id_urg' => 'id_obj']);
	}
	/*----------------------------------------------------------------------------------*/
		// ответственный
	public function getMaster() {
		return $this->hasOne(UrgMaster::className(), ['id_urg' => 'id_obj'])
			->orderBy('date DESC');
	}
	/*----------------------------------------------------------------------------------*/
		// история ответственных
	public function getMastersHistory() {
		return $this->hasMany(UrgMaster::className(), ['id_urg' => 'id_obj'])
			->orderBy('date DESC');
	}
	/*----------------------------------------------------------------------------------*/
		// рапорта по объекту
	public function getWorks() {
		return $this->hasMany(WorksReport::className(), ['id_obj' => 'id_obj'])
			->where('complete=1')
			->orderBy('work_date DESC');
	}
	/*----------------------------------------------------------------------------------*/
	public function getQrCode() {
		return $this->hasOne(ObjQrcode::className(), ['id_obj' => 'id_obj']);
	}
	/*----------------------------------------------------------------------------------*/
	public function clearAddress() {
		// аддрес оставляем в БД - удаляем только link
		$res = LinkObj::remove($this->id, $this->address->id);
		if ($res)
			return ['status'=>'ok', 'message'=>'Адрес очищен'];
		else
			return ['status'=>'error', 'message'=>'При удалении адреса возникла ошибка'];
	}
	/*----------------------------------------------------------------------------------*/
	public function clearEquipment() {
			// удалить оборудование из Obj и каждое из совей таблицы (pc, locks, pzk, psk, filter)
			// удалить файлы для каждого оборудования
			// не трогаем адреса
		/*select id_obj2
		from link_obj
		where id_obj1=2000000000001000182 and id_type_obj2<>5*/

		$eq_ids = LinkObj::find()
			->select('id_obj2')
			->where('id_type_obj2<>5')
			->andWhere(['id_obj1'=>$this->id])->all();
		//print_r($eq_ids);
		foreach ($eq_ids as $eid) {
			$dres = Obj::del($eid->id_obj2);
		}

		$res = ['status'=>'ok'];
//		foreach ($this->equipments as $file) {
//			$sub_res = File::del($file->id_obj);
//			$res['status'] = $sub_res['status'] ? 'ok' : 'error';
//			$res['files'][] = $sub_res;
//			//$file->delete();
//		}
		return $res;
	}
	/*----------------------------------------------------------------------------------*/
	public function clearFiles() {
		$res = ['status'=>'ok'];
		foreach ($this->files as $file) {
			$sub_res = File::del($file->id_obj);
			$res['status'] = $sub_res['status'] ? 'ok' : 'error';
			$res['files'][] = $sub_res;
			//$file->delete();
		}
		return $res;
	}
	/*----------------------------------------------------------------------------------*/
	public function fields() {
//		print_r($this->scenario);
//		if ($this->scenario == self::SCENARIO_DEFAULT) {
//			return parent::fields();
//		}
		$mode = $_GET['mode'] ? $_GET['mode'] : 'default';
		switch ($mode) {
			case 'view':
				//$fields = parent::fields();
				//unset($fields['id_obj']);

				$fields['id'] = function () {
					return (string)$this->id_obj;
				};

				$fields[] = 'num_unique';
				$fields[] = 'num';

				$fields[] = 'date_acceptance';
				$fields[] = 'date_starting';
				$fields[] = 'date_startup_gas';
				$fields[] = 'calc_capacity';

				$fields['type'] = function () {
					return [
						'id' => $this->type->id,
						'name' => $this->type->name
					];
				};
				$fields[] = 'address_descr';
				$fields[] = 'descr';
				$fields['barrier'] = function () {
					return $this->barrier==0 ? false : true;
				} ;
				//$fields[] = 'id_ring';
				break;
            case 'search':
                break;
			case 'default':
			default:
				$fields = parent::fields();
				$fields['id'] = function() {
					return (string)$this->id_obj;
				};
                $fields['id_unit'] = function () {
                    return (string)$this->id_unit;
                };
				//unset($fields['id_obj']);
		}

//        $fields['type'] = function () {
//            return [
//                'id' => $this->type->id,
//                'name' => $this->type->name
//            ];
//        };

		$fields['gps'] = function () {
			$gps = explode(',', $this->gps);
			return ['X' => (real)$gps[0], 'Y' => (real)$gps[1]];
		};
		$fields['osm_id'] = function () {
			return $this->osm_id;
		};
		$fields['id_ring'] = function () {
			return $this->id_ring;
		};
		unset($fields['id_obj']);

		return $fields;
	}
	/*----------------------------------------------------------------------------------*/

	public function extraFields() {
		$fields = parent::extraFields();

		$fields[] = 'num_archiv';
		$fields[] = 'num_inventory';
		$fields[] = 'id_telemetry';
		$fields[] = 'barrier';

		$fields['master'] = function () {
			$m = $this->master;;
			return $m ? $m : [
				'id'=> -1,
				'id_urg' => $this->id,
				'name' => '- - - ',
				'post' => '- - - ',
				'date' => null
			];
		};
		$fields['mastersHistory'] = function () {
			return $this->mastersHistory;
		};

//		$fields['unit'] = function () {
//			return $this->linkUnit;
//		};

//            'id_lighting_protection' => 'Тип молниезащиты',
//            'lighting_protection_resistance' => 'Сопротивление молниезащиты',

//            'id_communication' => 'Система связи',

		$fields['qr_code'] = function () {
			return $this->qrCode->qrcode;
		};

		$fields['full_address'] = function () {
			return  ($this->address->np->id>0 ? $this->address->np->type->type_np . ' ' . $this->address->np->name : '')
					.($this->address->street->id>0 ? ', '.$this->address->street->type->short_type . ' ' . $this->address->street->name : '')
					.($this->address->house ? ', '.$this->address->house : '')
					.($this->address->building ? ', '.$this->address->building : '');
					//.', '.$this->address->np->region->name.' район';
		};
		$fields['address'] = function () {
			if ($this->address)
				return [
					'id' => $this->address->id,
//					'np_type' => $this->address->np->type->type_np,
					'region' => [
						'id' => (string)$this->address->np->region->id,
						'name' => $this->address->np->region->name
					],
					'np' => [
						'id' => (string)$this->address->np->id,
						'name' => $this->address->np->type->type_np . ' ' . $this->address->np->name
					],
//					//'street_type' => $this->address->street->type->short_type,
					'street' => $this->address->street ? [
						'id' => (string)$this->address->street->id,
						'name' => $this->address->street->type->short_type . ' ' . $this->address->street->name,
					] : [
						'id' => null,
						'name' => null,
					],
					'house' => $this->address->house,// ? $this->address->house : '-',
					'building' => $this->address->building,// ? $this->address->building : '-',
					'flat' => $this->address->flat,// ? $this->address->flat : '-',
				];
			else
				return [
					'id' => -1,
					//'np_type' => $this->address->np->type->type_np,
					'region' => [
						'id' => '-2',
						'name' => null
					],
					'np' => [
						'id' => '-2',
						'name' => null
					],
					//'street_type' => $this->address->street->type->short_type,
					'street' => [
						'id' => '-2',
						'name' => null,
					],
					'house' => null,
					'building' => null,
					'flat' => null,
				];
		};

		$arr = [
			'type',
			'buildingType',
			'floorMaterial',
			'ventilation',
			'power_supply',
			'light',
			'communication',
			'manufacturer',
			'telemetry',
			'heating',
			'extinguishing',
		];

		foreach ($arr as $f) {
			$fields[$f] = function() use ($f) {
				return [
					'id'=>$this->{$f}->id,
					'name'=>$this->{$f}->name
				];
			};
		}

		$fields['lightingprotection'] = function() {
			return [
				'id'=>$this->lightingprotection->id,
				'name'=>$this->lightingprotection->name,
				'resistance' => $this->lighting_protection_resistance
			];
		};

		$fields['owner'] = function() {
			if ($this->owner) {
				return [
					'id' => $this->owner->id,
					'name' => $this->owner->name
				];
			} else {
				return [
					'id' => -1,
					'name' => '<не задан>'
				];
			}
		};

		$fields['files'] = function() {
			return $this->files;
		};

		$fields['ring'] = function() {
			if ($this->ringInUrg)
				return Urg::find()->where('id_obj in ('.implode(',', $this->ringInUrg).')')->all();
			else
				return [];
		};

		$fields['ring2'] = function() {
			$arr = [];
			if ($this->ringInUrg) {
				$inRing = Urg::find()->with('address')->where('id_obj in (' . implode(',', $this->ringInUrg) . ')')->all();

				foreach ($inRing as $item) {
					$arr[] = [
						'id'   => $item->id,
						'type' => $item->type,
						'num'  => $item->num,
						'urg'  => $item->type->name.' №'.$item->num
					];
				}
			}
			return $arr;
		};

		$fields['rooms'] = function() {
			if ($this->rooms)
				return $this->rooms;
			else
				return [];
		};

		$fields['works'] = function() {
			return $this->works;
		};

		$fields['lastwork'] = function () {
			return $this->works[0]->work_date;
		};
		return $fields;
	}
	/*----------------------------------------------------------------------------------*/
	public function setAddress($new_addr) {
		/* 1. ищем добавляемый или изменяемый адрес в БД
		 * 2. если не находим - создаём новый
		 * 3. если находим - меняем связь в link_obj
		 */

		if ($new_addr['np']['id']<0 || $new_addr['np']['id']=="") {
			// пустой адрес - так нельзя
			$res['status'] = 'error';
			$res['message'] = 'Адрес не указан.';
			$res['errors'] = ['Адрес не может быть пустым!'];

			return $res;
		}

		$new_addr['house']    = trim($new_addr['house'])=='' ? null : $new_addr['house'];
		$new_addr['building'] = trim($new_addr['building'])=='' ? null : $new_addr['building'];
		$new_addr['flat']     = trim($new_addr['flat'])=='' ? null : $new_addr['flat'];

			// ищем $new_addr в БД
		$addr = Address::find()
			->where([
				'id_np'=>$new_addr['np']['id'],
				'id_street'=>$new_addr['street']['id'],
				'house'=>$new_addr['house'],
				'building'=>$new_addr['building'],
				'flat'=>$new_addr['flat'],
			])
			->one();

		if ($addr) {
			// адрес уже существует - правим/добавляем линк
			// 1. находим линк текущего УРГ c адресом
			$link = $this->getLinkObjAsParent()->where(['id_type_obj2'=>'5'])->one();
			if ($link) {
				$link->id_obj2 = $addr->id;
				if ($link->save()) {
					$res['status'] = 'ok';
				} else {
					$res['status'] = 'error';
					$res['message'] = 'Ошибка изменения связи УРГ->Адрес';
					$res['errors'] = $link->errors;
				}
			} else {
				if (LinkObj::add($this->id, $addr->id)) {
					$res['status'] = 'ok';
					$res['id_addr'] = $addr->id;
				} else {
					$res['status'] = 'error';
					$res['message'] = 'Ошибка создания связи УРГ->Адрес';
				};
			}
		} else {
				// адрес не найден - добавляем новый
			$res['addr'] = 'new address';

			$id_obj = Obj::addAddress();
			$a = new Address();

			$a->id_obj    = $id_obj;
			$a->id_np     = trim($new_addr['np']['name'])=='' ? null : trim($new_addr['np']['id']);
			$a->id_street = trim($new_addr['street']['name'])=='' ? null : trim($new_addr['street']['id']);
			$a->house     = trim($new_addr['house'])=='' ? null : trim($new_addr['house']);
			$a->building  = trim($new_addr['building'])=='' ? null : trim($new_addr['building']);
			$a->flat      = trim($new_addr['flat'])=='' ? null : trim($new_addr['flat']);

			// сохраняем адрес в БД
			if ($a->save()) {
					// правим/добавляем связь адреса с УРГ
				$link = $this->getLinkObjAsParent()->where(['id_type_obj2'=>'5'])->one();
				if ($link) {
					$link->id_obj2 = $a->id;
					if ($link->save()) {
						$res['status'] = 'ok';
					} else {
						$res['status'] = 'error';
						$res['message'] = 'Ошибка изменения связи УРГ->Адрес';
						$res['errors'] = $link->errors;
					}
				} else {
					if (LinkObj::add($this->id, $id_obj)) {
						$res['status'] = 'ok';
						$res['id_addr'] = $a->id;
					} else {
						$res['status'] = 'error';
						$res['message'] = 'Ошибка создания связи УРГ->Адрес';
					};
				}
			} else {
				$res['status'] = 'error';
				$res['message'] = 'Ошибка сохранения адреса';
				$res['errors'] = $a->errors;
			}
		}
		/*--------------*/


		return $res;
	}
	/*----------------------------------------------------------------------------------*/
	public function beforeSave($insert) {
		$tmp = $this->gps;
		$this->num_sort = $this->num*1;
		$this->gps = new \yii\db\Expression("GeomFromText(:point)", [
			':point'=>'POINT('. $tmp['X'].' '.$tmp['Y'].')'
		]);

		return parent::beforeSave($insert);
	}

	/*----------------------------------------------------------------------------------*/
		// переопределяем выборку поля gps
	public static function find() {
		return parent::find()
			->select(['*', 'CONCAT(X(gps),\',\', Y(gps)) as gps', 'id_ring'])
			->with('master')
			->joinWith('ring');
	}
//INSERT INTO `geo` (`place_name`, `coordinates`) VALUES ('Apremont', PointFromText('POINT(49.2343503980067 2.52738212494082)'));
}

