<?php

namespace common\models\urg;

use Yii;

/**
 * This is the model class for table "filter_spr_type".
 *
 * @property integer $id_type
 * @property string $type
 *
 * @property Filter[] $filters
 */
class FilterSprType extends \common\components\myModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'filter_spr_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type'], 'required'],
            [['type'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_type' => 'Id Type',
            'type' => 'Type',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFilters()
    {
        return $this->hasMany(Filter::className(), ['id_type' => 'id_type']);
    }
}
