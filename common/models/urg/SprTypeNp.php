<?php

namespace common\models\urg;

use common\components\myModel;
use Yii;

/**
 * This is the model class for table "spr_type_np".
 *
 * @property integer $id_type_np
 * @property string $type_np
 */
class SprTypeNp extends myModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'spr_type_np';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_np'], 'required'],
            [['type_np'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_type_np' => 'Id Type Np',
            'type_np' => 'Type Np',
        ];
    }
    /*----------------------------------------------------------------------------------*/
    public function fields()
    {
        $fields = parent::fields();

        $fields['id']   = function () { return $this->id_type_np;};
        $fields['name'] = function () { return $this->type_np;};

        return $fields;
    }
}
