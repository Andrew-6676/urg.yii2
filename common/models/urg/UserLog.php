<?php

namespace common\models\urg;

use Yii;

/**
 * This is the model class for table "user_log".
 *
 * @property integer $id_log
 * @property string $uid
 * @property string $date_log
 * @property string $remark
 * @property string $confvalue
 * @property integer $id_unit
 * @property string $name_table
 * @property integer $id_obj
 */
class UserLog extends \common\components\myModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['uid', 'date_log', 'remark', 'confvalue', 'id_unit', 'name_table', 'id_obj'], 'required'],
            [['date_log'], 'safe'],
            [['remark'], 'string'],
            [['id_unit', 'id_obj'], 'integer'],
            [['uid'], 'string', 'max' => 130],
            [['confvalue'], 'string', 'max' => 50],
            [['name_table'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_log' => 'Id Log',
            'uid' => 'Uid',
            'date_log' => 'Date Log',
            'remark' => 'Remark',
            'confvalue' => 'Confvalue',
            'id_unit' => 'Id Unit',
            'name_table' => 'Name Table',
            'id_obj' => 'Id Obj',
        ];
    }
}
