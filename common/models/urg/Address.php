<?php

namespace common\models\urg;

use Yii;

/**
 * This is the model class for table "address".
 *
 * @property integer $id_obj
 * @property integer $id_np
 * @property integer $id_street
 * @property string $house
 * @property string $building
 * @property string $flat
 *
 * @property Obj $idObj
 * @property SprNp $idNp
 * @property SprStreet $idStreet
 */
class Address extends \common\components\myModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'address';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_obj'], 'required'],
            [['id_obj', 'id_np', 'id_street'], 'integer'],
            [['house'], 'string', 'max' => 10],
            [['building'], 'string', 'max' => 2],
            [['flat'], 'string', 'max' => 6],
            [['id_obj'], 'exist', 'skipOnError' => true, 'targetClass' => Obj::className(), 'targetAttribute' => ['id_obj' => 'id_obj']],
            [['id_np'], 'exist', 'skipOnError' => true, 'targetClass' => SprNp::className(), 'targetAttribute' => ['id_np' => 'id_np']],
            [['id_street'], 'exist', 'skipOnError' => true, 'targetClass' => SprStreet::className(), 'targetAttribute' => ['id_street' => 'id_street']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Id',
            'id_obj' => 'Id Obj',
            'id_np' => 'Id Np',
            'id_street' => 'Id Street',
            'house' => 'House',
            'building' => 'Building',
            'flat' => 'Flat',
        ];
    }
	/* -------------------------------------------------------------------------------------- */
	public function getId() {
		return $this->id_obj;
	}
	/* -------------------------------------------------------------------------------------- */
	public function getRegion() {
		return $this->hasOne(SprNp::className(), ['id_np' => 'id_np']);
	}
    /* -------------------------------------------------------------------------------------- */
    public function getNp() {
        return $this->hasOne(SprNp::className(), ['id_np' => 'id_np']);
    }
	/* -------------------------------------------------------------------------------------- */
    public function getStreet() {
        return $this->hasOne(SprStreet::className(), ['id_street' => 'id_street']);
    }
	/* -------------------------------------------------------------------------------------- */
	public function fields() {
		$fields = parent::fields();
		unset($fields['id_obj']);
		return $fields;
	}
	/* -------------------------------------------------------------------------------------- */
	public static function find() {
		return parent::find()
			->with(['region', 'np', 'street']);
	}
}
