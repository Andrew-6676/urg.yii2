<?php

namespace common\models\urg;

use Yii;

/**
 * This is the model class for table "pc_spr_model".
 *
 * @property integer $id_model
 * @property string $model
 * @property integer $id_make
 *
 * @property Pc[] $pcs
 * @property PcSprMake $idMake
 */
class PcSprModel extends \common\components\myModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pc_spr_model';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['model', 'id_make'], 'required'],
            [['id_make'], 'integer'],
            [['model'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_model' => 'Id Model',
            'model' => 'Model',
            'id_make' => 'Id Make',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPcs()
    {
        return $this->hasMany(Pc::className(), ['id_model' => 'id_model']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdMake()
    {
        return $this->hasOne(PcSprMake::className(), ['id_make' => 'id_make']);
    }
}
