<?php

namespace common\models\urg;

use Yii;

/**
 * This is the model class for table "psk_spr_type".
 *
 * @property integer $id_type
 * @property string $type
 *
 * @property Psk[] $psks
 * @property PskSprModel[] $pskSprModels
 */
class PskSprType extends \common\components\myModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'psk_spr_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type'], 'required'],
            [['type'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_type' => 'Id Type',
            'type' => 'Type',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPsks()
    {
        return $this->hasMany(Psk::className(), ['id_type' => 'id_type']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPskSprModels()
    {
        return $this->hasMany(PskSprModel::className(), ['id_type' => 'id_type']);
    }
}
