<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
	'language' => 'ru-RU',
	'components' => [
        'db' => require(__DIR__ . '/db-gasb.php'),
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
	    'formatter' => [
		    'dateFormat' => 'dd.MM.yyyy',
		    'decimalSeparator' => '<b>.</b>',
		    'thousandSeparator' => '&nbsp;',
		    'currencyCode' => '$',
		    'nullDisplay' => '-',
	    ],
    ],
];

function getPassword() {
	if (file_exists('/etc/gasbase/gasbase')) {
		return trim(file_get_contents('/etc/gasbase/gasbase'));
	} else {
		return 'root';
	}
}