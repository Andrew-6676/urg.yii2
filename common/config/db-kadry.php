<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=192.168.100.11;dbname=kadry_ob',
    'username' => 'gasbase',
    'password' => 'gasbase',
    'charset' => 'utf8',

	'enableSchemaCache' => true,

	// Duration of schema cache.
	'schemaCacheDuration' => 3600,

	// Name of the cache component used to store schema information
	'schemaCache' => 'cache',
];
