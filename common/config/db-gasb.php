<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=gasb',
    'username' => 'gasb',
	'password' => getPassword(),
    'charset' => 'utf8',

	'enableSchemaCache' => true,

	// Duration of schema cache.
	'schemaCacheDuration' => 60*60*24,

	// Name of the cache component used to store schema information
	'schemaCache' => 'cache',
];
