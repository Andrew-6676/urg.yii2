<?php
namespace console\controllers\tools;

use common\models\urg\LinkObj;
use common\models\urg\Meter;
use common\models\urg\Obj;
use common\models\vat\Xml;
use common\components\xmlParser2;
use Yii;
use yii\base\Action;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Console;

class recreateMeterAction extends Action
{
	public function run(){
		$start = microtime(true);
		$errors = 0;
		$new = 0;
		//print_r(Yii::$app->params);
		$this->controller->stdout("PHP ".phpversion()."\n", Console::BOLD, Console::FG_CYAN);
		echo ">";
		$this->controller->stdout("recreateMeter()", Console::BOLD, Console::FG_YELLOW);
		return 0;

		//**************************//

			//1. выбираем счётчики дял пересоздания
		$query = (new Query())
			->select('m.id_obj')
			->from('meter m')
			->innerJoin('nodemet n', 'm.id_obj = n.id_obj');

		echo "\n";
		print_r($query->createCommand()->sql);
		$old_meters = $query->column();
		//print_r($old_meters);
		echo "\nстрок: ".($query->count());
		echo "\n";

			//2. создаём новые объекты и связи запоминаем их ID в массив
		$new_obj = [];
		$err = [];
		$i=0;
		foreach ($old_meters as $old_id) {
			$i++;
			$new_id = Obj::addMeter();
			if ($new_id > 0 ) {
				$new_obj[$old_id] = $new_id;
				if (LinkObj::add($old_id, $new_id)) {
					//3. заменяем id_obj в meter на новые
					$res = Yii::$app->db->createCommand('update meter set id_obj='.$new_id.', reestr=\'-\' where id_obj='.$old_id)->execute();
					if (!$res) {
						$err[] = [$old_id,'change'];
					} else {
						// TODO pover_meter
					};
				} else {
					$err[] = [$old_id,'link'];
				};
			} else {
				$err[] = [$old_id,'obj'];
			}
			//if ($i>10) break;
		}
		file_put_contents('/tmp/meter_result', print_r($new_obj, true));
		//print_r($new_obj);
		echo "ERRORS: ";
		print_r($err);




		//**************************//

		$time = microtime(true) - $start;
		$time = $this->controller->ansiFormat(round($time,3), Console::FG_BLUE, Console::BOLD);
		echo "\n";
		echo 'Скрипт выполнялся '. $time. ' сек.';
		echo "\n";

		return 0;
	}
}