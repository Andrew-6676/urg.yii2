<?php
namespace console\controllers\tools;

use common\models\urg\LinkObj;
use common\models\urg\Meter;
use common\models\urg\Obj;
use common\models\urg\SprNp;
use common\models\urg\SprStreet;

use common\components\xmlParser2;
use common\models\urg\TubeSprDiameter;
use common\models\urg\Urg;
use Yii;
use yii\base\Action;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Console;

class importGomelAction extends Action
{
	public $spr_np = [];
	public $spr_street = [];

	public function run($file, $save='false', $limit=-1){
		$start = microtime(true);
		$errors = 0;
		$new = 0;
		$dir = dirname($file);
		//print_r(Yii::$app->params);
		$this->controller->stdout("PHP ".phpversion()."\n", Console::BOLD, Console::FG_CYAN);
		echo ">";
		$this->controller->stdout("importUrgs()\n", Console::BOLD, Console::FG_YELLOW);

		//**************************//

		if (!file_exists($file)) {
			$this->controller->stdout("\nERROR open file", Console::BOLD, Console::FG_RED);
			return 11;
		}

		$json = file_get_contents($file);
		if (!$json) {
			$this->controller->stdout("\nERROR open file", Console::BOLD, Console::FG_RED);
			return 22;
		}

        $type_urg =[
            "ПГРП" => 1001,
            "ГГРП" => 1001,
            "ГРП" => 1002,
            "ШРП" => 1003,
            "ГРУ" => 1004,
            "КРД" => 1005
        ];

        $equip = [
            'pc',
            'pzk',
            'psk',
            'filter',
            'locks',
            'boiler',
            'meter',
        ];

        $eq_types = [
            "boiler"    => 1 ,
            "locks"     => 4 ,
            "meter"     => 31,
            "pc"        => 32,
            "filter"    => 80,
            "pzk"       => 81,
            "psk"       => 82,
            //"urg_instrumentation"   => 98,
        ];


		/*-----------------------------------------------*/
		$junits = file_get_contents($dir.'/sp_ceh.json');
		if (!$junits) {
			$this->controller->stdout("\nERROR open spr_ceh", Console::BOLD, Console::FG_RED);
			return 33;
		}

		$units1=json_decode($junits);
		$units = [];
		foreach ($units1 as $unit) {
			$units[$unit->ceh_id] = $unit;
		}
		//print_r($units);
		//exit;
		/*-----------------------------------------------*/
			// только гомель
            // формируем массив для получения правильного id_model для каждого оборудования
        $models = [];
        foreach ($equip as $eq) {
            $class = 'common\models\urg\\'.upperFirst($eq).'SprModel';
            $data = $class::find()->where('id_gomel is not null')->asArray()->all();

            foreach ($data as $row) {
                $gids = explode(',', $row['id_gomel']);
                foreach ($gids as $gid) {
                    $models[$eq][$gid] = $row['id_model'];
                }
            }
        }

        $diameters = [];
        $data = TubeSprDiameter::find()->where('id_material=1')->orderBy('diameter')->asArray()->all();
        foreach ($data as $row) {
            $diameters[$row['diameter']] = $row['id_diameter'];
        }
//        print_r($diameters);
//        exit;

//		$this->spr_np = SprNp::find()->where(['between', 'id_region', 25, 47])->indexBy('id_np')->asArray()->all();
//		$id_nps = SprNp::find()->where(['between', 'id_region', 25, 47])->select('id_np')->column();
//		//print_r($this->spr_np);
//		$this->spr_street = SprStreet::find()->select(['id_street','street', 'id_np'])->where(['in', 'id_np', $id_nps])->indexBy('id_street')->asArray()->all();
//		$str = [];
//		foreach () {
//
//		}
//		print_r($this->spr_street);
//		exit;
		/*-----------------------------------------------*/
			// выбираем, что уже импортировано, чтоб не задублировать
		$imported = [];
		$imp = Urg::find()->select('id_obj,descr')->where(['like', 'descr', 'imported_gomel'])->asArray()->all();
		foreach ($imp as $i) {
			$id = explode(',',$i['descr'])[1];
			$imported[$id] = $i;
		}
//		print_r($imported);
//		exit;
		/*-----------------------------------------------*/

		//print_r(Yii::$app->components['db']);



		$db = Yii::$app->db;
		$transaction = $db->beginTransaction();

        $clear_master = $db->createCommand("DELETE FROM urg_master WHERE id_urg=:id_urg");
		$update = $db->createCommand("UPDATE `urg` SET
						`id_type_urg` = :id_type_urg,
						`id_unit` = :id_unit,
						`num` = :num,
						`num_archiv` = :num_archiv,
						`num_inventory` = :num_inventory,
						`num_inventory_eq` = :num_inventory_eq,
						`address_descr` = :address_descr,
						`date_acceptance` = :date_acceptance,
						`date_starting` = :date_starting,
						`date_startup_gas` = :date_startup_gas,
						`gps` = GeomFromText(:gps),
						`id_telemetry` = :id_telemetry,
						`id_building_type` = :id_building_type,
						`id_power_supply` = :id_power_supply,
						`id_light` = :id_light,
						`id_manufacturer` = :id_manufacturer,
						`id_lighting_protection` = :id_lighting_protection,
						`lighting_protection_resistance` = :lighting_protection_resistance,
						`id_heating` = :id_heating,
						`id_communication` = :id_communication,
						`id_floor_material` = :id_floor_material,
						`id_owner` = :id_owner,
						`barrier` = :barrier,
						`id_extinguishing` = :id_extinguishing,
						`id_ventilation` = :id_ventilation,
						`dist_enter` = :dist_enter,
						`dist_exit` = :dist_exit,
						`calc_capacity` = :calc_capacity,
						`descr` = :descr,
						`deleted` = :deleted,
						`pressure_in_1` = :pressure_in_1,
						`pressure_in_2` = :pressure_in_2,
						`pressure_in_3` = :pressure_in_3,
						`pressure_in_4` = :pressure_in_4,
						`pressure_out_1` = :pressure_out_1,
						`pressure_out_2` = :pressure_out_2,
						`pressure_out_3` = :pressure_out_3,
						`pressure_out_4` = :pressure_out_4
				WHERE id_obj=:id_obj");

        $insert_log = $db->createCommand("INSERT INTO `urg_import` (id_obj, id_type_obj, id, descr) 
                                              VALUES (:id_obj, :id_type_obj, :id, :descr)");

        $insert_master = $db->createCommand("INSERT INTO urg_master (date, id_urg, post, name) 
                                                VALUES (:date, :id_urg, :post, :name) ");
		$insert = $db->createCommand("INSERT INTO `urg` (
						`id_obj`,
						`id_type_urg`,
						`id_unit`,
						`num`,
						`num_archiv`,
						`num_inventory`,
						`num_inventory_eq`,
						`address_descr`,
						`date_acceptance`,
						`date_starting`,
						`date_startup_gas`,
						`gps`,
						`id_telemetry`,
						`id_building_type`,
						`id_power_supply`,
						`id_light`,
						`id_manufacturer`,
						`id_lighting_protection`,
						`lighting_protection_resistance`,
						`id_heating`,
						`id_communication`,
						`id_floor_material`,
						`id_owner`,
						`barrier`,
						`id_extinguishing`,
						`id_ventilation`,
						`dist_enter`,
						`dist_exit`,
						`calc_capacity`,
						`descr`,
						`deleted`,
						`pressure_in_1`,
						`pressure_in_2`,
						`pressure_in_3`,
						`pressure_in_4`,
						`pressure_out_1`,
						`pressure_out_2`,
						`pressure_out_3`,
						`pressure_out_4`
					) VALUES (
						:id_obj,
						:id_type_urg,
						:id_unit,
						:num,
						:num_archiv,
						:num_inventory,
						:num_inventory_eq,
						:address_descr,
						:date_acceptance,
						:date_starting,
						:date_startup_gas,
						GeomFromText(:gps),
						:id_telemetry,
						:id_building_type,
						:id_power_supply,
						:id_light,
						:id_manufacturer,
						:id_lighting_protection,
						:lighting_protection_resistance,
						:id_heating,
						:id_communication,
						:id_floor_material,
						:id_owner,
						:barrier,
						:id_extinguishing,
						:id_ventilation,
						:dist_enter,
						:dist_exit,
						:calc_capacity,
						:descr,
						:deleted,
						:pressure_in_1,
						:pressure_in_2,
						:pressure_in_3,
						:pressure_in_4,
						:pressure_out_1,
						:pressure_out_2,
						:pressure_out_3,
						:pressure_out_4
					)");

		$insert_eq = $db->createCommand("INSERT INTO `urg_equipment` (id_obj, id_eq_type, mount_point, schema_number, descr) VALUES (:id_obj, :id_eq_type, :mount_point, :schema_number, :descr)");
        $insert_instr = $db->createCommand("INSERT INTO urg_instrumentation (id_obj, id_type, id_make, quantity, descr) VALUES (:id_obj, :id_type, :id_make, :quantity, :descr)");
		$urgs = json_decode($json);

		$types = [];
		$i=1;
		try {

			$imp_np = [];
			    // цикл по импортируемым УРГ
			foreach ($urgs as $urg) {
				//$addr = preg_split("/\.\s*/", $urg->address->address, -1, PREG_OFFSET_CAPTURE | PREG_SPLIT_NO_EMPTY);

                if ($limit>0 && $i>$limit) break;

				echo "\n", $i++,') ', $urg->id, ' => '.'', $urg->name, ' ', $urg->address->address, "";
				$num_name =  preg_split('/[\s-]/', $urg->name, -1);

					// если УРГ ненужного нам типа, например "ГРУ"
				if (!key_exists($num_name[0], $type_urg)) continue;
					// подсчёт количества обработанных УРГ с разбивкой по типам
				if (key_exists($num_name[0], $types))
					$types[$num_name[0]] = $types[$num_name[0]]+1;
				else
					$types[$num_name[0]] = 1;

				$params = [
					':id_type_urg' => $type_urg[$num_name[0]],
					':id_unit' => array_key_exists($urg->id_unit,$units) ? $units[$urg->id_unit]->gasb_id : '3000000000000000000',
					':num' => $num_name[1],
					':num_archiv' => $urg->num_archiv,
					':num_inventory' => $urg->num_inventory,
					':num_inventory_eq' => null,
					':address_descr' => $urg->address->address,
					':date_acceptance' => $urg->date_acceptance ?? '0000-00-00',
					':date_starting' => $urg->date_starting ?? '0000-00-00',
					':date_startup_gas' => $urg->date_startup_gas ?? '0000-00-00',
					':gps' => 'POINT('.($urg->gps->lon ?? 0).' '.($urg->gps->lat ?? 0).')',
					':id_telemetry' => $urg->id_telemetry,
					':id_building_type' => $urg->id_building_type,
					':id_power_supply' => $urg->id_power_supply,
					':id_light' => $urg->id_light,
					':id_manufacturer' => $urg->id_manufacturer,
					':id_lighting_protection' => $urg->id_lighting_protection,
					':lighting_protection_resistance' => $urg->lighting_protection_resistance ?? null,
					':id_heating' => $urg->id_heating,
					':id_communication' => $urg->id_communication,
					':id_floor_material' => $urg->id_floor_material,
					':id_owner' => $urg->id_owner,
					':barrier' => $urg->barrier,
					':id_extinguishing' => $urg->id_extinguishing,
					':id_ventilation' => $urg->id_ventilation,
					':dist_enter' => $urg->dist_enter ?? null,
					':dist_exit' => $urg->dist_exit ?? null,
					//':master' => $urg->master->fio ?? null,
					':calc_capacity' => null,
					':descr' => 'imported_gomel,'.$urg->id.','.$urg->name,
					':deleted' => 0,
					':pressure_in_1'  => isset($urg->pressure) ? $urg->pressure->in_1 : null,
					':pressure_in_2'  => isset($urg->pressure) ? $urg->pressure->in_2 : null,
					':pressure_in_3'  => isset($urg->pressure) ? $urg->pressure->in_3 : null,
					':pressure_in_4'  => isset($urg->pressure) ? $urg->pressure->in_4 : null,
					':pressure_out_1' => isset($urg->pressure) ? $urg->pressure->out_1 : null,
					':pressure_out_2' => isset($urg->pressure) ? $urg->pressure->out_2 : null,
					':pressure_out_3' => isset($urg->pressure) ? $urg->pressure->out_3 : null,
					':pressure_out_4' => isset($urg->pressure) ? $urg->pressure->out_4 : null
				];

//				$address = $this->normalize_address($urg->address->address);
//				print_r($address);

				if ($save=='true') {
					if (array_key_exists($urg->id, $imported)) {
                        $idObj_urg = $imported[$urg->id]['id_obj'];
						echo " - UPDATE $idObj_urg";
						$params['id_obj'] = $idObj_urg;
						$update->bindValues($params)->execute();

                        if (property_exists($urg, 'master') && property_exists($urg->master,'id')) {
                            // обновить мастера
                            $clear_master->bindValues([':id_urg' => $idObj_urg])->execute();
                            $insert_master->bindValues([
                                ':date' => '1970-01-01',
                                ':id_urg' => $idObj_urg,
                                ':post' => 'мастер',
                                ':name' => $urg->master->fio
                            ])->execute();
                        }
					}
					else {
						$idObj_urg = Obj::createOb('urg');
						$params['id_obj'] = $idObj_urg;
						echo " - INSERT id_obj=$idObj_urg\n";
						    // вставляем в БД сам УРГ
						$insert->bindValues($params)->execute();

                        // вставить мастера
                        if (property_exists($urg, 'master') && property_exists($urg->master,'id')) {
                            // обновить мастера
                            $clear_master->bindValues([':id_urg' => $idObj_urg])->execute();
                            $insert_master->bindValues([
                                ':date' => '1970-01-01',
                                ':id_urg' => $idObj_urg,
                                ':post' => 'мастер',
                                ':name' => $urg->master->fio
                            ])->execute();
                        }

						    // перебираем импортируемое оборудование (pzk, filter, pc.....)
                        foreach ($equip as $eq) {
                            if (isset($urg->{$eq}) && count($urg->{$eq})>0) {
                                echo "---", $eq, "\n";
                                //print_r($urg->{$eq});
                                foreach ($urg->{$eq} as $item) {
                                    $item_eq = json_decode(json_encode($item), true);
                                    unset($item_eq['trigger_setting']);
                                    //print_r($item_eq);
                                    // создаём объект оборудования
                                    $idObj_eq = Obj::createObjByIdType($eq_types[$eq]);
                                        // исправляем точку моньтирования оборудования из "line#grp_1" в "line#1"
                                    preg_match('/(.*)#.*(\\d+)/', $item_eq['mount_point'], $matches);
                                    $mnt = $matches[1].'#'.$matches[2];
                                    $params_eq = [
                                        ':id_obj' => $idObj_eq,
                                        ':id_eq_type' => $eq_types[$eq],
                                        ':mount_point' => $mnt,
                                        ':schema_number' => '-',
                                        ':descr' => 'imported_gomel,'.$eq
                                    ];
//                                    // добавляем запись в таблицу urg_equipment
                                    $insert_eq->bindValues($params_eq)->execute();
//                                    // добавляем сввязь УРГ и оборудования
                                    LinkObj::add($idObj_urg, $idObj_eq);
//                                    // добавляем запись в таблицу соответствующую оборудованию
                                    $equipment_class = 'common\models\urg\\'.upperFirst($eq);
                                    $equipment = new $equipment_class();
                                    $equipment->id_obj = $idObj_eq;
                                    $equipment->load($item_eq, '');
                                        // получаем id_diameter для диаметра
                                    if (array_key_exists('diameter', $item_eq) && array_key_exists($item_eq['diameter'], $diameters)) {
                                        $equipment->id_diameter = $diameters[$item_eq['diameter']];
                                    } else {
                                        //$equipment->id_diameter = null;
                                    }
                                    $equipment->id_model = $equipment->id_model ? $models[$eq][$equipment->id_model] : -2;
                                    if ($eq=='locks') {
                                        $equipment->id_type         = -2;
                                        $equipment->id_diameter     = 0;
                                        $equipment->model           = '-';
                                        //$equipment->date_install    = $item_eq['date_starting'];
                                        //$equipment->year_production = $item_eq['year_release'];
                                        //$equipment->number          = substr($item_eq['serial_number'],0,20);
                                        $equipment->placement       = 'line#1';
                                    } else {
                                        $equipment->serial_number = substr($equipment->serial_number,0,30);
                                    }

                                    //echo "-----------------";
                                    $res = $equipment->save();
                                    //echo $res ? 'saved'."\n" : 'error'."\n";
                                    if (!$res) {
                                        echo "----------------- ERROR";
                                        print_r($equipment->errors);
                                        $transaction->rollBack();
                                        echo "\n----------------- rollback! ----------------- 0\n";
                                        return 1;
                                    }
//                                    // вставляем запись в лог для оборудования
                                    $params_log = [
                                        ':id_obj' =>  $idObj_eq,
                                        ':id_type_obj' => $eq_types[$eq],
                                        ':id' => 0,
                                        ':descr' => 'gomel,eq,'.$eq
                                    ];
                                    $insert_log->bindValues($params_log)->execute();
                                }
                            }
                        }

                            // перебираем КИП
                        echo "---instrumentation";
                        foreach ($urg->instrumentation as $instr) {
                            $idObj_instr = Obj::addInstrumentation();
                            $insert_instr->bindValues([
                                ':id_obj'   => $idObj_instr,
                                ':id_type'  => $instr->id_type,
                                ':id_make'  => -2, //$instr->id_make,   // сопоставить с моим справочником
                                ':quantity' => $instr->quantity,
                                ':descr'    => 'imported_gomel'
                            ])->execute();
                                // добавляем сввязь УРГ и оборудования
                            LinkObj::add($idObj_urg, $idObj_instr);
                        }



						    // вставляем запись в лог для ург
                        $params_log = [
                            ':id_obj' =>  $idObj_urg,
                            ':id_type_obj' => 99,
                            ':id' => $urg->id,
                            ':descr' => 'gomel,urg'
                        ];
						$insert_log->bindValues($params_log)->execute();

                        //$equip
					}
				}

			}
			echo "\n----------------- commit! -----------------\n";
			$transaction->commit();
		} catch(\Exception $e) {
			echo "\n----------------- rollback! ----------------- 1\n";
			$transaction->rollBack();
			throw $e;
		} catch(\Throwable $e) {
			echo "\n----------------- rollback! ----------------- 2\n";
			print_r($e);
			$transaction->rollBack();
			return 2;
		}
		//**************************//

		print_r($types);
		echo array_sum($types);

		$time = microtime(true) - $start;
		$time = $this->controller->ansiFormat(round($time,3), Console::FG_BLUE, Console::BOLD);
		echo "\n";
		echo 'Скрипт выполнялся '. $time. ' сек.';
		echo "\n";

		return 0;
	}

		// возвроащаем массив из id города, id улицы, номера дома
	public function normalize_address($addr) {
		$address = [];
		echo "\n";
		$expl = explode(',', $addr);
		print_r($expl);

		$address['np'] = $this->findInArray($expl[0], $this->spr_np, 'np');;
			// если нашёлся населённый пункт - ищем улицы только в нём
		//$address['street'] = $this->findInArray('', $this->spr_street, 'street');;
		echo "\n";
		return $address;
	}

		// массив городов и улиц индексирован по id
	public function findInArray($needle, $arr, $field) {
		foreach ($arr as $id => $item) {
				// ищем "Новка" в "а.г. Новка" ($needle в $item[$field])
			//echo $needle." in ".$item[$field]."\n";
			if (strpos($needle, $item[$field]) !== false) {
				return $id;
			}
		}
		return -1;
	}
}

function upperFirst($str) {

    $first = mb_substr($str,0,1, 'UTF-8');//первая буква
    $last = mb_substr($str,1);//все кроме первой буквы
    $first = mb_strtoupper($first, 'UTF-8');
    $last = mb_strtolower($last, 'UTF-8');
    return $first.$last;

}