<?php
namespace console\controllers\tools;

use common\models\urg\LinkObj;
use common\models\urg\Meter;
use common\models\urg\Obj;
use common\models\urg\SprNp;
use common\models\urg\SprStreet;

use common\components\xmlParser2;
use common\models\urg\Urg;
use Yii;
use yii\base\Action;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Console;

class importGPSAction extends Action
{
	public $spr_np = [];
	public $spr_street = [];

	public function run($dir, $id_unit, $save=false){
		$start = microtime(true);
		$errors = 0;
		$new = 0;
		//print_r(Yii::$app->params);
		$this->controller->stdout("PHP ".phpversion()."\n", Console::BOLD, Console::FG_CYAN);
		echo ">";
		$this->controller->stdout("importGPS($dir, $id_unit)\n", Console::BOLD, Console::FG_YELLOW);
		echo ">";
		$this->controller->stdout("DB = ".Yii::$app->db->dsn."\n", Console::NORMAL, Console::FG_YELLOW);

		//**************************//
		//exit;
		/*-----------------------------------------------*/

		//print_r(Yii::$app->components['db']);

		$type_urg = [
			"pgrp" => 1001,
			"grp" => 1002,
			"shrp" => 1003,
			"krd" => 1005,
		];
		//**************************//

		$files = scandir($dir);

		foreach ($files as $file) {
			if (is_dir($dir.$file)) continue;
			echo $file."\n";
			$data = file($dir.$file);

			foreach ($data as $row) {
				echo $row;
				$arr = explode(';',$row);
				$gps = ['X'=>$arr[2], 'Y'=>$arr[1]];
				$reg = '/([ПГРПШКД]+)[^\d]*(\d+)/';
				$res = preg_match($reg, $arr[0], $matches);
				if ($res>0) {
					$num = $matches[2];
				} else {
					continue;
				}

				echo "[".$type_urg[$file]."] ", $num, "  -  ";
				$model = Urg::findOne(['id_type_urg'=>$type_urg[$file], 'num'=>$num, 'id_unit'=>$id_unit]);
				if ($model) {
					echo $model->id_obj, "";
					$model->gps = $gps;
					if ($model->save()) {
						$this->controller->stdout("   -  SAVED"."\n", Console::FG_GREEN);
					} else {
						$this->controller->stdout("   -  ERROR"."\n", Console::FG_RED);
					}
					echo "\n";
				} else {
					$this->controller->stdout("not found"."\n", Console::FG_RED);
				}

			}
		}


		//**************************//

		$time = microtime(true) - $start;
		$time = $this->controller->ansiFormat(round($time,3), Console::FG_BLUE, Console::BOLD);
		echo "\n";
		echo 'Скрипт выполнялся '. $time. ' сек.';
		echo "\n";

		return 0;
	}

		// возвроащаем массив из id города, id улицы, номера дома
	public function normalize_address($addr) {
		$address = [];
		echo "\n";
		$expl = explode(',', $addr);
		print_r($expl);

		$address['np'] = $this->findInArray($expl[0], $this->spr_np, 'np');;
			// если нашёлся населённый пункт - ищем улицы только в нём
		//$address['street'] = $this->findInArray('', $this->spr_street, 'street');;
		echo "\n";
		return $address;
	}

		// массив городов и улиц индексирован по id
	public function findInArray($needle, $arr, $field) {
		foreach ($arr as $id => $item) {
				// ищем "Новка" в "а.г. Новка" ($needle в $item[$field])
			//echo $needle." in ".$item[$field]."\n";
			if (strpos($needle, $item[$field]) !== false) {
				return $id;
			}
		}
		return -1;
	}
}

function upperFirst($str) {

    $first = mb_substr($str,0,1, 'UTF-8');//первая буква
    $last = mb_substr($str,1);//все кроме первой буквы
    $first = mb_strtoupper($first, 'UTF-8');
    $last = mb_strtolower($last, 'UTF-8');
    return $first.$last;

}