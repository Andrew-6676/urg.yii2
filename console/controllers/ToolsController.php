<?php

namespace console\controllers;

use yii\helpers\Console;

class ToolsController extends \yii\console\Controller
{

	public function actions()
	{
		$actions = parent::actions();
		// загрузка xml из файлов в БД
		$actions['recreateMeter'] = ['class' => 'console\controllers\tools\recreateMeterAction'];
		$actions['importGomel']   = ['class' => 'console\controllers\tools\importGomelAction'];
		$actions['importGrodno']  = ['class' => 'console\controllers\tools\importGrodnoAction'];
		$actions['importGPS']     = ['class' => 'console\controllers\tools\importGPSAction'];


		return $actions;
	}
	// The command "yii example/create test" will call "actionCreate('test')"
//	public function actionCreate($name) {
//		$name = $this->ansiFormat($name, Console::FG_RED);
//		echo "--create-- $name \n";
//		return 0;
//	}

	// The command "yii example/index city" will call "actionIndex('city', 'name')"
	// The command "yii example/index city id" will call "actionIndex('city', 'id')"
	public function actionHelp() {
		$this->stdout("Возможные команды:\n", Console::BOLD, Console::FG_YELLOW);
		echo " - ";
		$this->stdout("recreateMeter", Console::BOLD);
		$this->stdout(" <folder>", Console::FG_GREEN);
		echo " - создание из счётчиков новых объектов со связями"."\n";

		echo " - ";
		$this->stdout("importUrgs", Console::BOLD);
		$this->stdout(" <JSON file> <save=false>", Console::FG_GREEN);
		echo " - импорт УРГ из JSON-файла"."\n";

		echo " - ";
		$this->stdout("importUrgs", Console::BOLD);
		echo " - сброс кэша схемы БД"."\n";

		return 0;
	}


	public function actionResetSchemaCache() {
		\Yii::$app->db->schema->refresh();
		echo "Кэш схемы БД очищен.\n";
	}
	// The command "yii example/add test" will call "actionAdd(['test'])"
	// The command "yii example/add test1,test2" will call "actionAdd(['test1', 'test2'])"
//	public function actionXml2db($folder) {
//		echo ">";
//		$this->stdout("Xml2db(", Console::BOLD, Console::FG_YELLOW);
//		$this->stdout("$folder)\n", Console::BOLD, Console::FG_YELLOW);
//
//
//		$base_path = Yii::$app->params[$folder.'_dir'];
//
//
//
//		return 0;
//	}
}