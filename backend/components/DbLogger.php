<?php
/**
 * Created by PhpStorm.
 * User: andrew
 * Date: 23.10.17
 * Time: 18:02
 */

namespace app\components;


use Yii;
use yii\db\Connection;
use yii\base\InvalidConfigException;
use yii\di\Instance;
use yii\helpers\VarDumper;
use yii\log\Target;

/**
 * DbTarget stores log messages in a database table.
 *
 * The database connection is specified by [[db]]. Database schema could be initialized by applying migration:
 *
 * ```
 * yii migrate --migrationPath=@yii/log/migrations/
 * ```
 *
 * If you don't want to use migration and need SQL instead, files for all databases are in migrations directory.
 *
 * You may change the name of the table used to store the data by setting [[logTable]].
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class DbLogger extends Target
{
	public $db = 'db';
	public $logTable = 'urg_log';


	/**
	 * Initializes the DbTarget component.
	 * This method will initialize the [[db]] property to make sure it refers to a valid DB connection.
	 * @throws InvalidConfigException if [[db]] is invalid.
	 */
	public function init()
	{
		parent::init();
		$this->db = Instance::ensure($this->db, Connection::className());
	}

	/**
	 * Stores log messages to DB.
	 */
	public function export()
	{
		$tableName = $this->db->quoteTableName($this->logTable);
//		$sql = "INSERT INTO $tableName ([[level]], [[category]], [[log_time]], [[prefix]], [[message]])
//                VALUES (:level, :category, :log_time, :prefix, :message)";

				$sql = "INSERT INTO $tableName ([[sql]], [[route]], [[user]])
               			 VALUES (:message, :route, :user)";

		$command = $this->db->createCommand($sql);
		foreach ($this->messages as $message) {
			list($text, $level, $category, $timestamp) = $message;
			if (!is_string($text)) {
				// exceptions may not be serializable if in the call stack somewhere is a Closure
				if ($text instanceof \Exception) {
					$text = (string) $text;
				} else {
					$text = VarDumper::export($text);
				}
			}

			if (preg_match('/(insert|update|CALL|delete |trunc)/i', $text)>0) {
				//print_r(\Yii::$app->requestedRoute);
				$route = \Yii::$app->requestedRoute;
				$command->bindValues([
					':message' => $text,
					':route' => print_r($route, true),
					':user' => urldecode(Yii::$app->request->getHeaders()->get('user')),
				])->execute();
			}
		}
	}

//	public function getContextMessage()
//	{
//		$message = parent::getContextMessage();
//
//		return $message;
//	}
}
