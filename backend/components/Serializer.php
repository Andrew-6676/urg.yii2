<?php

namespace app\components;

class Serializer extends \yii\rest\Serializer
{

	public $totalCount = -1;

	public function init() {
		parent::init();
		$this->response
			->getHeaders()
			->set('client-version', \Yii::$app->params['client_version']);

		$this->response
			->getHeaders()
			->set('Access-Control-Allow-Credentials', 'true');
//		$this->response
//			->getHeaders()
//			->set('access-control-expose-headers', 'client-version');
	}

	protected function addPaginationHeaders($pagination)
	{
		parent::addPaginationHeaders($pagination);

		$eh = [
			$this->totalCountHeader,
			$this->pageCountHeader,
			$this->currentPageHeader,
			$this->perPageHeader,
			'Link',
			'client-version'
		];
		$this->response
			->getHeaders()
            ->set('access-control-expose-headers', implode(',', $eh));
	}
	/*-----------------------------------------------------------------*/
	protected function serializeDataProvider($dataProvider) {
		//$dp = parent::serializeDataProvider($dataProvider);

		//$dataProvider->query->odrerBy('id_obj');
		if (isset($_GET['webix']) && $_GET['webix']) {
			$dataProvider->query->offset($_GET['start'])->limit($_GET['count']);
			$this->totalCount = $dataProvider->getTotalCount();
			$pagination = $dataProvider->getPagination();
			if (isset($_GET['count'])) {
				$pagination->pageSize = $_GET['count'];
				$pagination->page = round($_GET['start'] / $_GET['count'], 0);
			} else {
				$pagination->pageSize = $_GET['per-page'];
				$pagination->page = 0;
			}
			//print_r($pagination);
		}

		return parent::serializeDataProvider($dataProvider);
	}
	/*-----------------------------------------------------------------*/
	public function  serializeModels(array $models)
	{
		if (isset($_GET['webix']) && $_GET['webix']) {
			$models = parent::serializeModels($models);
			return [
				'data' => $models,
				"pos" => $_GET['start'],
				"get" => $_GET,
                "total_count" => $this->totalCount,
				'page' => $_GET['start'] ? round($_GET['start'] / $_GET['count'], 0) + 1 : 1,
			];

		}

		$models =  parent::serializeModels($models);
		return $models;
	}
}