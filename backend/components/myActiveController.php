<?php

namespace app\components;

use yii\db\Query;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\rest\ActiveController;

/**
 * ActiveController implements a common set of actions for supporting RESTful access to ActiveRecord.
 *
 * The class of the ActiveRecord should be specified via [[modelClass]], which must implement [[\yii\db\ActiveRecordInterface]].
 * By default, the following actions are supported:
 *
 * - `index`: list of models
 * - `view`: return the details of a model
 * - `create`: create a new model
 * - `update`: update an existing model
 * - `delete`: delete an existing model
 * - `options`: return the allowed HTTP methods
 *
 * You may disable some of these actions by overriding [[actions()]] and unsetting the corresponding actions.
 *
 * To add a new action, either override [[actions()]] by appending a new action class or write a new action method.
 * Make sure you also override [[verbs()]] to properly declare what HTTP methods are allowed by the new action.
 *
 * You should usually override [[checkAccess()]] to check whether the current user has the privilege to perform
 * the specified action against the specified model.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class myActiveController extends ActiveController
{
	public $with = array();
	public $serializer = [
		'class' => 'app\components\Serializer',
		//'collectionEnvelope' => 'items',
	];


	public function behaviors()	{
		return
			\yii\helpers\ArrayHelper::merge(parent::behaviors(), [
				[
					'class' => \yii\filters\ContentNegotiator::className(),
					'formats' => [
						'application/json' => \yii\web\Response::FORMAT_JSON,
					],
				],
				'corsFilter' => [
					'class' => \yii\filters\Cors::className(),
				],
			]);
	}

	public function actions()
	{
		// переопределяем index action (добавляем возможность фильтрации)
//		return ArrayHelper::merge(
//			parent::actions(),  // то, что не переопределяем - берём от родительсеого класса
//			[
//				'index' => [
//					'class' => 'yii\rest\IndexAction',
//					'modelClass' => $this->modelClass,
//					'checkAccess' => [$this, 'checkAccess'],
//					'prepareDataProvider' =>  function ($action) {
//						$modelClass = $this->modelClass;
//							// если задан filter в параметрах запроса
//						if (isset($_GET['filter'])) {
//							$query = $modelClass::filter($_GET['filter']);
//						} else {
//								// если надо вернуть все данные
//							$query = $modelClass::find();
//						}
//						//return $query;
//						return new ActiveDataProvider([
//								// добавляем сортировку
//							'query' => $query->orderBy($_GET['order']),
//						]);
//					}
//				]
//			]);
		$actions = parent::actions();
		$actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];
		//$actions['view']['prepareDataProvider']  = [$this, 'prepareDataProvider'];
			// переопределяем удаление
		unset($actions['delete']);

		$actions['delete'] = [
			'class' => 'app\components\DeleteAction',
			'modelClass' => $this->modelClass,
			'checkAccess' => [$this, 'checkAccess'],
		];

		return $actions;
	}

	/**
	 * Checks the privilege of the current user.
	 *
	 * This method should be overridden to check whether the current user has the privilege
	 * to run the specified action against the specified data model.
	 * If the user does not have access, a [[ForbiddenHttpException]] should be thrown.
	 *
	 * @param string $action the ID of the action to be executed
	 * @param object $model the model to be accessed. If null, it means no specific model is being accessed.
	 * @param array $params additional parameters
	 * @throws ForbiddenHttpException if the user does not have access
	 */
	public function checkAccess($action, $model = null, $params = [])
	{
		//return false;
	}
	/* --------------------------------------------------------------------------------- */
	//public function actionDelete($id) {

//		$res = Yii::$app->db->createCommand("select name_table 
//											from spr_type_obj left join obj on spr_type_obj.id_type_obj=obj.id_type_obj 
//											where obj.id_obj=$id")->queryAll();
//		return [$res];


		//$modelClass = $this->modelClass;

		//return Obj::delete($id);
		//return $modelClass::findOne($id);
		//$model = $this->findModel($id);

		//if ($this->checkAccess) {
		//	call_user_func($this->checkAccess, $this->id, $model);
		//}

		//if ($model->delete() === false) {
		//	throw new ServerErrorHttpException('Failed to delete the object for unknown reason.');
		//}

		//Yii::$app->getResponse()->setStatusCode(204);
	//}
	/* --------------------------------------------------------------------------------- */
	public function prepareDataProvider() {
		// подготовить и вернуть провайдер данных для действия "index"
		$modelClass = $this->modelClass;
		// если задан filter в параметрах запроса
		if (isset($_GET['filter'])) {
			$query = $modelClass::filter($_GET['filter']);
		} else {
				// если надо вернуть все данные
			$query = $modelClass::find();
		}
		//return $query;
		$adp = new ActiveDataProvider([
			// добавляем сортировку и жадную загрузку
			// TODO: если WITH-поле не встречается в expand - то и не надо добавлять это в выборку
			'query' => $query->with($this->with)->orderBy($_GET['order']),
			//'pagination' => false,
			'pagination' => [
				'defaultPageSize' => Yii::$app->params['defaultPageSize'],
				'pageSizeLimit' => [0, 1500000000],
			],
		]);

		return $adp;
	}
}
