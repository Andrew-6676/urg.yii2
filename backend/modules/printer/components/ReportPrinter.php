<?php

namespace app\modules\printer\components;
use PhpOffice\PhpWord\Settings;
use \PhpOffice\PhpWord;
use PhpOffice\PhpWord\TemplateProcessor;

class ReportPrinter
{
    private $phpWord;
    private $document;
    private $templateProcessor;
    private $filename;
    private $data ;

    public function __construct($template=null, $filename='print.docx')
    {
	    if ($template) {
		    $this->templateProcessor = new TemplateProcessor($template);
		    if ($filename) {
			    $this->filename = $filename;
		    }
	    } else {
	    	return null;
	    }
	    return $this;
    }

//    public function load($file_name)
//    {
//        $this->document = $this->phpWord->loadTemplate($file_name);
//
//        $properties = $this->phpWord->getDocInfo();
//        $properties->setCreator($_SERVER['HTTP_HOST']);
//        $properties->setCompany($_SERVER['HTTP_HOST']);
//        $properties->setTitle('Title');
//        $properties->setDescription('Description');
//        $properties->setLastModifiedBy($_SERVER['HTTP_HOST']);
//        $properties->setCreated(time());
//        $properties->setModified(time());
//        $properties->setSubject('My subject');
//
//    }
	/*---------------------------------------------------------------------------------------------------------*/
    public function set_values($data) {
	    // получаем список переменных в шаблоне и в цикле заполняем их из $data
	    $vars = $this->templateProcessor->getVariables();
	    //Utils::print_r($vars);
	    //exit;
	    //echo '<pre>';
	    foreach ($vars as $key => $var) {
		    //echo $key.') '.$var.' - ';
		    $fields = explode('.',$var);
		    //Utils::print_r($fields);
		    $value = $data;
		    foreach ($fields as $f) {
			    // если параметр отчёта надо заменить массивом
			    if (strpos($f, '[]')>0) {
				    $f=substr($f,0,-2);
				    //Utils::print_r($value->ring->urg);
				    foreach ($value['ring'] as $item) {
					    //print_r($item);
					    //echo "##\n";
				    }
				    $value = 'Array of '.$f;
				    break;
			    } else {
				    $value = $value[$f];
				    if ($value=='- не указано -') $value = '-';
			    };

		    };
		    //echo $value, "\n";
		    $this->templateProcessor->setValue($var, $value);
	    };
	    //echo '</pre>';
	    return $this;
    }
	/*---------------------------------------------------------------------------------------------------------*/
//	public function dowload($str)
//	{
//		header("Expires: Mon, 1 Apr 1974 05:00:00 GMT");
//		header("Last-Modified: ". gmdate("D,d M YH:i:s"). "GMT");
//		header("Cache-Control: no-cache, must-revalidate");
//		header("Pragma: no-cache");
//		header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
//		header("Content-Disposition: attachment; filename=".$str.$this->data['id'].".docx");
//
//		$this->document->saveAs('php://output');
//	}
	/*---------------------------------------------------------------------------------------------------------*/
	public function save($file, $pdf=false) {

	}
	/*---------------------------------------------------------------------------------------------------------*/
	public function download($pdf=false) {
		if ($pdf) {
			header('Content-Type: application/pdf');
			header('Content-Disposition: attachment;filename="'. $this->filename .'"');
			header('Cache-Control: max-age=0');

			// TODO: тут всё плохо - криво формирует PDF. или взять другой pdf-рендерер или ещё что
			Settings::setPdfRendererPath('../modules/printer/vendor/mpdf/mpdf/src');
			Settings::setPdfRendererName('MPDF');

			// не выводит русские буквы и криво
//			Settings::setPdfRendererPath('../modules/printer/vendor/tecnickcom/tcpdf');
//			Settings::setPdfRendererName('TCPDF');
			// не выводит русские буквы и криво
//			Settings::setPdfRendererPath('../modules/printer/vendor/dompdf/dompdf/src');
//			Settings::setPdfRendererName('DomPDF');

			$this->templateProcessor->saveAs('tmp.docx');
			$temp = \PhpOffice\PhpWord\IOFactory::load('tmp.docx');
			$xmlWriter = \PhpOffice\PhpWord\IOFactory::createWriter($temp , 'PDF');
			$xmlWriter->save('php://output');
			//echo "toPDF";
		} else {
			header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
			header('Content-Disposition: attachment;filename="'. $this->filename .'"');
			header('Cache-Control: max-age=0');

			$this->templateProcessor->saveAs('php://output');
		}
	}
}

/*--------------------------*/