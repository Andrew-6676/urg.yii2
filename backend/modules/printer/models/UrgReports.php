<?php

namespace app\modules\printer\models;

use Yii;

/**
 * This is the model class for table "urg_reports".
 *
 * @property integer $id
 * @property string $name
 * @property string $display_name
 * @property string $short_name
 * @property string $url
 * @property string $descr
 * @property string $params
 * @property boolean $disabled
 * @property integer $category
 */
class UrgReports extends \common\components\myModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'urg_reports';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['params'], 'string'],
            [['disabled'], 'boolean'],
            [['category'], 'integer'],
            [['name', 'short_name'], 'string', 'max' => 50],
            [['display_name'], 'string', 'max' => 100],
            [['url', 'descr'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя латиницей',
            'display_name' => 'Отображаемое наименование',
            'short_name' => 'Короткое наименование',
            'url' => 'url',
            'descr' => 'Описание',
            'params' => 'Параметрв отчёта',
            'disabled' => 'Отключен',
            'category' => 'вид отчёта',
        ];
    }

    /*-------------------------------------------------------------------------------------------*/

	public static function find() {
		return parent::find()
			->where(['disabled'=>0]);
	}
}
