<?php
namespace app\modules\printer\controllers;

use Yii;
use yii\web\Controller;

/**
 * Print  controller
 */
class PrintController extends Controller
{


	public function actions()
	{
		$actions['passportGrp']   = ['class' => 'app\modules\printer\controllers\printer\PassportGrpAction'];
		$actions['regimCard']     = ['class' => 'app\modules\printer\controllers\printer\RegimCardAction'];
		$actions['form1Grp']      = ['class' => 'app\modules\printer\controllers\printer\Form1GrpAction'];
		$actions['form2Grp']      = ['class' => 'app\modules\printer\controllers\printer\Form2GrpAction'];
		$actions['form3Grp']      = ['class' => 'app\modules\printer\controllers\printer\Form3GrpAction'];
		$actions['form4Grp']      = ['class' => 'app\modules\printer\controllers\printer\Form4GrpAction'];
		$actions['form5Grp']      = ['class' => 'app\modules\printer\controllers\printer\Form5GrpAction'];
		$actions['form6Grp']      = ['class' => 'app\modules\printer\controllers\printer\Form6GrpAction'];
		$actions['form7Grp']      = ['class' => 'app\modules\printer\controllers\printer\Form7GrpAction'];
		$actions['form8Grp']      = ['class' => 'app\modules\printer\controllers\printer\Form8GrpAction'];
		$actions['form9Grp']      = ['class' => 'app\modules\printer\controllers\printer\Form9GrpAction'];
		$actions['form10Grp']     = ['class' => 'app\modules\printer\controllers\printer\Form10GrpAction'];
		$actions['bookGrp']       = ['class' => 'app\modules\printer\controllers\printer\BookGrpAction'];


#		$actions['upload'] = ['class' => 'app\controllers\file\UploadAction'];
#		$actions['delete'] = ['class' => 'app\controllers\file\DeleteAction'];
		return $actions;
	}

}
