<?php
namespace app\modules\printer\controllers\printer;

use app\modules\printer\components\ReportPrinter;
use common\components\Utils;
use common\models\urg\Urg;  //где находится модель
use PhpOffice\PhpWord\Settings;
use PhpOffice\PhpWord\TemplateProcessor;
use Yii;
use yii\base\Action;

class passportGrpAction  extends Action
{
	public function run($id, $pdf=false) {
	  //  echo " $id";
//      $grp = Urg::findOne($id);
//      echo $grp->floorMaterial->name;
		// загружаем всё что мождно по УРГ
        $grp = Urg::find()
	        ->with([
	        	'floorMaterial',
		        'type',
		        'buildingType',
		        'manufacturer',
		        'address',
	        ])
	        ->where("id_obj=:id",["id"=>$id])
	        ->one();

		$grp_a = $grp->toArray($fields = [], $expand = [
			'owner',
			'building_type',
			'floorMaterial',
			'ventilation',
			'power_supply',
			'light',
			'lightingprotection',
			'communication',
			'manufacturer',
			'heating',
			'owner',
			'extinguishing',
			'telemetry',
			'rooms',
			'full_address',
			'ring2',
			'type'
		]);

		$arr = [];
		foreach ($grp_a['ring2'] as $item) {
			$arr[] = $item['urg'];
		}
		$grp_a['ring'] = implode(', ', $arr);;
		//Utils::print_r($grp_a);
        //echo date('H:i:s'), ' Creating new TemplateProcessor instance...', EOL;
		$grp_a['date_starting'] = Yii::$app->formatter->asDate($grp_a['date_starting']);

		$printer = new ReportPrinter('templates/Form1Grp.docx', $grp_a['type']['name'].'_'.$grp_a['num'].'_паспорт.docx');
		$printer
			->set_values($grp_a)
			->download();
    }

}
