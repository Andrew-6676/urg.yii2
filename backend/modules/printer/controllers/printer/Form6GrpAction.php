<?php
namespace app\modules\printer\controllers\printer;

use common\components\Utils;
use common\models\urg\Urg;  //где находится модель
use app\modules\printer\components\PrintReport;
use Yii;
use yii\base\Action;

class form6GrpAction  extends Action
{
	public function run($id) {

        $file_name = 'templates/Form6Grp.docx';
        $str="форма6_";

        $main_class = new PrintReport();

        $main_class->load($file_name);

        $data['id']= $id;
        $data['type']= "Meshkov";

        $main_class->set_value($data);

        $main_class->dowload($str);


    }
}
