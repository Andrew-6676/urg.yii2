<?php
namespace app\modules\printer;
use yii\base\BootstrapInterface;

class Bootstrap implements BootstrapInterface
{
	/**
	 * @inheritdoc
	 */
	public function bootstrap($app)
	{
		require_once('vendor/autoload.php');

/*
		1-ОФ наряд допуск на газоопасные работы
		3-ГРПО
		4-ГРП
		5-ГРП
		9-ГРП
		форма Гомельоблазовская перечня ГРП, которая в excel экспортируется
		Графики 4-ГРП и 5-ГРП в разных вариациях, где только обходы, где все обслуживания, без обходов и цветные
		-Резервуарные установки:
		3-РУ
		2-РУ с вариантами
*/

		$app->getUrlManager()->addRules(
			[
				['class' => 'yii\rest\UrlRule',
					'controller' => [
						'printer/report',
					],
				],
				// объявление правил здесь
				'print/passportGrp/<id>' => 'printer/print/passportGrp',   // печать данных по ГРП
				'print/regimCard/<id>'   => 'printer/print/regimCard',     // печать по ГРП
				'print/form1Grp/<id>'    => 'printer/print/form1Grp',      // ...
				'print/form2Grp/<id>'    => 'printer/print/form2Grp',      //  вахтенный журнал ГРП (ГРУ)
				'print/form3Grp/<id>'    => 'printer/print/form3Grp',      //  ЗАДАНИЕ на осмотр технического состояния ГРП (ГРУ)
				'print/form4Grp/<id>'    => 'printer/print/form4Grp',      //  ГРАФИК осмотра технического состояния ГРП (ГРУ)
				'print/form5Grp/<id>'    => 'printer/print/form5Grp',      //  ГРАФИК проверки параметров срабатывания предохранительных запорных и сбросных устройств, технического обслуживания, текущего ремонта ГРП (ГРУ)
				'print/form6Grp/<id>'    => 'printer/print/form6Grp',      //  ЭКСПЛУАТАЦИОННЫЙ ПАСПОРТ комбинированного  регулятора давления
				'print/form7Grp/<id>'    => 'printer/print/form7Grp',      //  ...
				'print/form8Grp/<id>'    => 'printer/print/form8Grp',      //  ГРАФИК технического обслуживания  и  текущего ремонта комбинированных  регуляторов давления
				//'print/form9Grp/<id>'    => 'printer/print/form9Grp',      //  график проверки параметров срабатывания предохранительных запорных и сбросных устройств
				'print/form10Grp/<id>'   => 'printer/print/form10Grp',     //  ЖУРНАЛ технического обслуживания телемеханики ГРП
				'print/bookGrp/<id>'     => 'printer/print/bookGrp',       //  книга ГРП(ШРП)

			]
		);
		//$app->getUrlManager()->suffix = '/';
		//$app->getUrlManager()->enableStrictParsing = true;
	}
}