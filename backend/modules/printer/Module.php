<?php

namespace app\modules\printer;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\printer\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
	    \Yii::configure($this, require(__DIR__ . '/printer_config.php'));
    }
}
