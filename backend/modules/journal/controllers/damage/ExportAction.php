<?php
namespace app\modules\journal\controllers\damage;

use yii\data\ActiveDataProvider;

class exportAction extends \yii\rest\Action {

	public function run($id_unit) {
		$_GET['mode'] = 'export';
		return $this->prepareExportProvider($id_unit);
	}

	public function prepareExportProvider($id_unit) {
		$modelClass = $this->modelClass;
		return new ActiveDataProvider([
			'query' => $modelClass::find()
				->select('damage.*, getFullAddress(id_obj1)  as address, id_obj1 as id_address')
				->with('typeDamage')
				->leftJoin('link_obj', 'link_obj.id_obj2=damage.id_obj and link_obj.id_type_obj1=5')
				->andWhere(['id_profycenter' => $id_unit])
				->andWhere(['date_fix'=>null])
				->andWhere(['>', 'time_message', 0]),
			'pagination' => false
		]);
	}

}
