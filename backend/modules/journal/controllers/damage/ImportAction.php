<?php
namespace app\modules\journal\controllers\damage;

use app\modules\journal\models\Damage;

class importAction extends \yii\rest\Action {

	public function run() {
		$res = null;
		$request = \Yii::$app->request;

		\Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Credentials', 'true');
		\Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Headers', \Yii::$app->request->getHeaders()->get('Access-Control-Request-Headers'));
		\Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Origin', \Yii::$app->request->getHeaders()->get('origin'));

		if ($request->isOptions) {
			\Yii::$app->response->format = \yii\web\Response::FORMAT_HTML;
			$options = ['POST'];
			\Yii::$app->getResponse()->getHeaders()->set('Allow', implode(',', $options));
			//Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
			\Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Methods', implode(',', $options));

			$res = implode(',', $options);
		}

		if ($request->isPost) {
			\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

			$rows = $request->bodyParams;
			//print_r($rows);
			if (!$rows[0]) {
				$rows = [$rows];
				//print_r($rows);
			}

			$res = [];
			foreach ($rows as $row) {
				//print_r($row);
				$damage = Damage::findOne($row['id']);
				if ($damage) {
					print_r($row);
					if ($damage->load($row, '') && $damage->save()) {
						$res[$row['id']] = 'ok';
					} else {
						$res[$row['id']] = $damage->errors;
					};
				} else {
					//echo "not exist\n";
					$res[$row['id']] = 'no exist';
				}
				//$damage->load($row);
			}
		} else {
			throw new \yii\web\BadRequestHttpException("Bad reques: use POST");
		}

		return $res;
	}
}
