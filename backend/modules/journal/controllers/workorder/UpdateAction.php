<?php
namespace app\modules\journal\controllers\workorder;

use app\modules\journal\models\WorkOrder;
use common\models\urg\Obj;
use common\models\urg\Urg;
use Yii;
use yii\base\Model;
use yii\rest\Action;
use yii\data\ActiveDataProvider;

class updateAction extends \yii\rest\UpdateAction {

	public $scenario = Model::SCENARIO_DEFAULT;

	public function run($id) {
		parent::run($id);
		//$model = null; //WorkOrder::findOne($id);
		$model = WorkOrder::findOne($id);
		return $model;
	}

}
