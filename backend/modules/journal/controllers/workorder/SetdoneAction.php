<?php
namespace app\modules\journal\controllers\workorder;

use app\modules\journal\models\WorkOrder;
use app\modules\journal\models\WorkOrderLine;
use app\modules\journal\models\Damage;
use common\models\urg\LinkObj;
use yii\base\Action;

class SetdoneAction extends Action
{

	public function run($id=null) {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$res = null;
		$request = \Yii::$app->request;

		if ($request->isOptions) {
			\Yii::$app->response->format = \yii\web\Response::FORMAT_HTML;
			$options = ['PUT', 'OPTIONS'];
			\Yii::$app->getResponse()->getHeaders()->set('Allow', implode(',', $options));
			//Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
			\Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Methods', implode(',', $options));

			$res = implode(',', $options);
			return $res;
		}

		if ($request->isPut) {
			$res = [];
				// для всех строк задания проставить факт и выполнение
			$upd_res = WorkOrderLine::updateAll(
				[
					'done'=>$request->bodyParams['done'],
					'timerate_fact'=>$request->bodyParams['timerate_fact'],
					'remark'=>$request->bodyParams['remark']
				],
				['id_work_order'=>$id]
			);
			//if ($upd_res>0) {
				//  обновить  damage - или заполнить или убрать инфу
				$wo = WorkOrder::find()
					->select(['date_work_order_end', 'brigade'])
					->where(['id_work_order'=>$id])
					->asArray()
					->one();
				//print_r($wo);
				$wols = WorkOrderLine::find()
					->select('work_order_line.id_link_obj, work_order_line.remark')
					->where(['id_work_order'=>$id])
					->indexBy('id_link_obj')
					->asArray()
					->all();
				//print_r($wols);
				//work_order_line.id_link_obj - id связи адреса (id_obj1) и повреждения (id_obj2)
				$ids_dmg_query = LinkObj::find()
					->select('id_obj2')
					->where(['in', 'id_link_obj', array_keys($wols)]);
				$dmgs = Damage::find()
					->select('id_obj, id_link_obj')
					->joinWith('linkObjAsChild')
					->where(['in', 'id_obj', $ids_dmg_query])
					->asArray()
					->all();
				//print_r($dmgs);

				// заполнить поля:
				// date_fix <- work_order.date_work_order_end
				// fixer <- work_order.brigade
				// work_fix <- "устранено"
				// remark <- work_order_line.remark

				foreach ($dmgs as $dmg) {
					//print_r($wols[$dmg['id_link_obj']]['remark']);
					//print_r($dmg);
					if ($request->bodyParams['done']==1) {
						$upd_res2 = Damage::updateAll(
							[
								'date_fix' => substr($wo['date_work_order_end'], 0, 10),
								'fixer' => $wo['brigade'],
								'work_fix' => 'устранено',
								'remark' => $wols[$dmg['id_link_obj']]['remark']
							],
							['id_obj' => $dmg['id_obj']]
						);
					} else {
						$upd_res2 = Damage::updateAll(
							[
								'date_fix' => null,
								'fixer' => ' ',
								'work_fix' => '',
								'remark' => ''
							],
							['id_obj' => $dmg['id_obj']]
						);
					}
				}


			//exit();

			$res = WorkOrderLine::find()
				//->select('*,id_status')
				//->joinWith('workOrder')
				->where(['id_work_order'=>$id])
				//->asArray()
				->all();

			//$res = WorkOrder::findOne($id);
				//$res = WorkOrder::find()->select('id_status')->where(['id_work_order'=>$id])->scalar();
			//} else {



			//};
			//echo $upd_res;
			return $res;
		}

		return ['wrong request'];
	}
}