<?php
namespace app\modules\journal\controllers;;

use app\components\myActiveController;

/**
 * Site controller
 */
class WorkorderlineController extends myActiveController
{
	public $modelClass = 'app\modules\journal\models\WorkOrderLine';
	//public $with = ['lines'];   // TODO: если это поле не встречается в expand - то и не надо добавлять это в выборку

	public function actions()
	{
		$actions = parent::actions();

		$actions['update']  = [
			'class' => 'app\modules\journal\controllers\workorderline\UpdateAction',
			'modelClass' => $this->modelClass,
		];
		$actions['create']  = [
			'class' => 'app\modules\journal\controllers\workorderline\CreateAction',
			'modelClass' => $this->modelClass,
		];

		return $actions;
	}

}
