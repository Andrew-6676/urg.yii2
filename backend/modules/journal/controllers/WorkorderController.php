<?php
namespace app\modules\journal\controllers;

use app\components\myActiveController;

/**
 * Site controller
 */
class WorkorderController extends myActiveController
{
	public $modelClass = 'app\modules\journal\models\WorkOrder';
	public $with = ['lines'];   // TODO: если это поле не встречается в expand - то и не надо добавлять это в выборку

	public function actions()
	{
		$actions = parent::actions();
		$actions['index']   = [
			'class' => 'yii\rest\IndexAction',
			'modelClass' => $this->modelClass,
			'prepareDataProvider' => [$this, 'prepareListProvider'],
		];
		$actions['setdone'] = ['class' => 'app\modules\journal\controllers\workorder\SetdoneAction'];
		$actions['update']  = [
			'class' => 'app\modules\journal\controllers\workorder\UpdateAction',
			'modelClass' => $this->modelClass,
		];

		return $actions;
	}


	public function prepareListProvider() {
		$adp = parent::prepareDataProvider();
//		if (Yii::$app->request->get('id_unit')) {
//			//$adp->query = $adp->query->joinWith('linkUnit')->andWhere('parent=:id_unit', ['id_unit' => $id_unit]);
		//$adp->query = $adp->query->where(['date_fix'=>null])->andWhere(['>', 'time_message', 0]);
		//$adp->pagination = false;
		//\Yii::$app->response->setStatusCode(500);
//		} else {
//
//		}
		return $adp;
	}
}
