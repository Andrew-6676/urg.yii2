<?php
namespace app\modules\journal\controllers\workorderline;

use app\modules\journal\models\WorkOrder;
use app\modules\journal\models\WorkOrderLine;
use common\models\urg\Obj;
use common\models\urg\Urg;
use Yii;
use yii\base\Model;
use yii\rest\Action;
use yii\data\ActiveDataProvider;

class updateAction extends \yii\rest\UpdateAction {

	public $scenario = Model::SCENARIO_DEFAULT;

	public function run($id) {
		parent::run($id);
		$model = WorkOrderLine::findOne($id);
		return $model;
	}

}
