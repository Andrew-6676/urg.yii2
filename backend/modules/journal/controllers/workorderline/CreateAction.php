<?php
namespace app\modules\journal\controllers\workorderline;

use app\modules\journal\models\WorkOrder;
use app\modules\journal\models\WorkOrderLine;
use common\models\urg\Obj;
use common\models\urg\Urg;
use Yii;
use yii\base\Model;
use yii\rest\Action;
use yii\data\ActiveDataProvider;

class createAction extends \yii\rest\UpdateAction {

	public $scenario = Model::SCENARIO_DEFAULT;

	public function run() {
		//parent::run();
		//$model = WorkOrderLine::findOne($id);
		//return $model;

		$request = \Yii::$app->request;
		//print_r($request->bodyParams);

		$addresses = explode(',', $request->bodyParams['id_link_obj']);
		$err = [];
		$new_ids = [];
		foreach ($addresses as $address) {
			//echo $address."\n";

			$model = new WorkOrderLine();
			$model->load($request->bodyParams, '');
			$model->id_link_obj = $address;
			//print_r($model);
			if ($model->save()) {
				$new_ids[] = $model->id_work_order_line;
			} else {
				$err[] = ($model->errors);
			}
		};

		if (count($err)==0) {
			$res = WorkOrderLine::find()->where(['in', 'id_work_order_line', $new_ids])->all();
		} else {
			$res = $err;
		}

		return $res;
	}

}
