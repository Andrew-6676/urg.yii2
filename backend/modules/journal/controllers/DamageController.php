<?php
namespace app\modules\journal\controllers;

use app\components\myActiveController;

/**
 * Site controller
 */
class DamageController extends myActiveController
{
	public $modelClass = 'app\modules\journal\models\Damage';

	public function actions()
	{
		$actions = parent::actions();

		//$actions['index'] = ['class' => 'app\modules\journal\controllers\damage\IndexAction'];
		$actions['export']  = [
			'class' => 'app\modules\journal\controllers\damage\ExportAction',
			'modelClass' => $this->modelClass,
		];
		$actions['import']  = [
			'class' => 'app\modules\journal\controllers\damage\ImportAction',
			'modelClass' => $this->modelClass,
		];
		$actions['index']   = [
			'class' => 'yii\rest\IndexAction',
			'modelClass' => $this->modelClass,
			'prepareDataProvider' => [$this, 'prepareListProvider'],
		];
		return $actions;
	}


	public function prepareListProvider() {
		$adp = parent::prepareDataProvider();
//		if (Yii::$app->request->get('id_unit')) {
//			//$adp->query = $adp->query->joinWith('linkUnit')->andWhere('parent=:id_unit', ['id_unit' => $id_unit]);
			//$adp->query = $adp->query->where(['date_fix'=>null])->andWhere(['>', 'time_message', 0]);
			$adp->pagination = false;
//		} else {
//
//		}
		return $adp;
	}

}
