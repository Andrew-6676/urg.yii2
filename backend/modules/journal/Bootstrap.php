<?php
namespace app\modules\journal;
use yii\base\BootstrapInterface;

class Bootstrap implements BootstrapInterface
{
	/**
	 * @inheritdoc
	 */
	public function bootstrap($app)
	{
		$app->getUrlManager()->addRules(
			[
				// объявление правил здесь
				//'' => 'site/default/index',
				//'<_a:(about|contacts)>' => 'site/default/<_a>'

				['class' => 'yii\rest\UrlRule',
					'controller' => [
						'journal/workorder',
						'journal/workorderline',
						'journal/damage'
					],
				],

				'journal/workorders/<id>/setdone'  => 'journal/workorder/setdone',
				'journal/damages/export/<id_unit>' => 'journal/damage/export',
				'journal/damages/import'           => 'journal/damage/import',

					// на случай, если клиент почему-то добавляет слеш в конце запроса (webix)
				[
					'verb'      => 'OPTIONS',
					'pattern'   => 'journal/workorders',
					'route'     => 'journal/workorder/options',
					'suffix'    => '/'
				],
				[
					'verb'      => 'POST',
					'pattern'   => 'journal/workorders',
					'route'     => 'journal/workorder/create',
					'suffix'    => '/'
				],
				[
					'verb'      => 'OPTIONS',
					'pattern'   => 'journal/workorderlines',
					'route'     => 'journal/workorderline/options',
					'suffix'    => '/'
				],
				[
					'verb'      => 'POST',
					'pattern'   => 'journal/workorderlines',
					'route'     => 'journal/workorderline/create',
					'suffix'    => '/'
				]

			]
		);
		//$app->getUrlManager()->suffix = '/';
		//$app->getUrlManager()->enableStrictParsing = true;
	}
}