<?php

namespace app\modules\journal;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\journal\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
	    \Yii::configure($this, require(__DIR__ . '/journal_config.php'));
    }
}
