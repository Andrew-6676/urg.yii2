<?php

namespace common\models\gasb;

use Yii;

/**
 * This is the model class for table "damage_spr_type_measure".
 *
 * @property integer $id_type_measure
 * @property string $type_measure
 */
class DamageSprTypeMeasure extends \common\components\myModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'damage_spr_type_measure';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_measure'], 'required'],
            [['type_measure'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_type_measure' => 'Id Type Measure',
            'type_measure' => 'Type Measure',
        ];
    }
}
