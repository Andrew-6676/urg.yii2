<?php

namespace app\modules\journal\models;

use Yii;

/**
 * This is the model class for table "spr_type_work_order".
 *
 * @property integer $id_type_work_order
 * @property string $type_work_order
 */
class SprTypeWorkOrder extends \common\components\myModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'spr_type_work_order';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_work_order'], 'required'],
            [['type_work_order'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_type_work_order' => 'Id Type Work Order',
            'type_work_order' => 'Type Work Order',
        ];
    }
}
