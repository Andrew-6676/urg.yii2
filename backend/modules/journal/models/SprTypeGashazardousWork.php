<?php

namespace common\models\gasb;

use Yii;

/**
 * This is the model class for table "spr_type_gashazardous_work".
 *
 * @property integer $id_type_gashazardous_work
 * @property string $type_gashazardous_work
 * @property string $jurnal
 */
class SprTypeGashazardousWork extends \common\components\myModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'spr_type_gashazardous_work';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_gashazardous_work', 'jurnal'], 'required'],
            [['type_gashazardous_work'], 'string', 'max' => 100],
            [['jurnal'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_type_gashazardous_work' => 'Id Type Gashazardous Work',
            'type_gashazardous_work' => 'Type Gashazardous Work',
            'jurnal' => 'Jurnal',
        ];
    }
}
