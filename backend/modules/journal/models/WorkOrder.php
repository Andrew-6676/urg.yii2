<?php

namespace app\modules\journal\models;

use Yii;
use yii\log\Logger;

/**
 * This is the model class for table "work_order".
 *
 * @property integer $id_work_order
 * @property string $type_obj
 * @property integer $id_type_work_order
 * @property integer $number
 * @property string $date_work_order_start
 * @property string $date_work_order_end
 * @property integer $id_unit
 * @property string $master
 * @property string $master_post
 * @property string $brigade
 * @property string $safety_requirement
 * @property string $prepare_requirement
 * @property integer $id_status
 * @property string $type
 * @property string $permissive
 * @property string $permissive_post
 *
 * @property WorkOrderLine[] $workOrderLines
 */
class WorkOrder extends \common\components\myModel
{
	public $lines_count;

	/**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'work_order';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_obj', 'id_type_work_order', 'date_work_order_start', 'date_work_order_end', 'id_unit', 'master', 'brigade', 'id_status'], 'required'],
            [['id_type_work_order', 'id_unit', 'id_status'], 'integer'],
            [['date_work_order_start', 'date_work_order_end'], 'safe'],
            [['safety_requirement', 'prepare_requirement', 'type' ], 'string'],
            [['type_obj'], 'string', 'max' => 20],
            [['number'], 'string', 'max' => 50],
            [['master', 'permissive', 'master_post', 'permissive_post'], 'string', 'max' => 100],
            [['brigade'], 'string', 'max' => 300],
            [['type'], 'string', 'max' => 1]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_work_order' => 'Id Work Order',
            'type_obj' => 'Type Obj',
            'id_type_work_order' => 'Id Type Work Order',
            'number' => 'Number',
            'date_work_order_start' => 'Date Work Order Start',
            'date_work_order_end' => 'Date Work Order End',
            'id_unit' => 'Id Unit',
            'master' => 'Ответственный',
            'master_post' => 'Должность',
            'brigade' => 'Brigade',
            'safety_requirement' => 'Safety Requirement',
            'prepare_requirement' => 'Prepare Requirement',
            'id_status' => 'Id Status',
            'type' => 'наряд или задание',
            'permissive' => 'выдал',
            'permissive_post' => 'должность',
        ];
    }
	/*------------------------------------------------------------------------------*/
	/*------------------------------------------------------------------------------*/
	// TODO: проверить, работает ли это
	// Будем использовать транзакции при указанных сценариях
	public function transactions() {
		return [
			self::SCENARIO_DEFAULT => self::OP_INSERT | self::OP_UPDATE,
		];
	}
	/*------------------------------------------------------------------------------*/
//	public function beforeSave($insert)
//	{
//		$tmp = explode(',', $this->master);
//		$tmp2 = explode(',', $this->permissive);
//
//		$this->master      = $tmp[0];
//		$this->master_post = $tmp[1];
//		$this->master_post = $tmp[0];
//		$this->master_post = $tmp[1];
//		return parent::beforeSave($insert);
//	}

	/*------------------------------------------------------------------------------*/
	public function afterSave($insert, $changedAttributes)
	{
		parent::afterSave($insert, $changedAttributes);

		$bodyParams = Yii::$app->request->bodyParams;
		// В afterSave мы сохраняем связанные модели
		// которые нужно сохранять после основной модели, т.к. нужен её ИД

		if (!array_key_exists('lines', Yii::$app->request->bodyParams)) return;

		// Получаем все связанные модели из запроса
		$lines = Yii::$app->request->bodyParams['lines'];
		//print_r($lines);
			// TODO: продумать и сделать правильнее и лучше (lines может быть массив, пустой массив или может отсутствовать)
		if (!isset($lines[0])) $lines = [$lines];
		//print_r($lines);
		//exit;

		if (count($lines)>0) {    // если есть хоть одна связанная модель
			if (isset($lines)) {
				foreach ($lines as $line) {
					$m_line = WorkOrderLine::findOne($line['id_work_order_line']);
					if (!$m_line) $m_line = new WorkOrderLine();
					if ($m_line->load($line, '')) {
						//print_r($m_line);
						$this->link('lines', $m_line);      // здесь происходит сохранение в БД
					} else {
						//print_r('ERROR LOAD LINES'); // сюда попадает если lines = пустой массив
					}
				}
			}
		}


		if (isset($bodyParams['employees']) && count($bodyParams['employees']) > 0) {

			$exist_workers = WorkOrderEmployee::find()
				->select('id_worker')
				->where(['id_work_order' => $this->id])
				->column();

			$new_workers = [];
			foreach ($bodyParams['employees'] as $e) {
				$new_workers[] = $e['id'];
			}
			foreach ($exist_workers as $ew) {
				if (array_search($ew, $new_workers)===false) {
					WorkOrderEmployee::deleteAll(['and', ['id_work_order' => $this->id], ['id_worker' => $ew]]);
				}
			}

			foreach ($bodyParams['employees'] as $e) {
				if (array_search($e['id'], $exist_workers)===false) {
					$w = new WorkOrderEmployee();
					$w->id_work_order = $this->id;
					$w->id_worker = $e['id'];
					$w->trade = $e['trade'];
					$w->save();
				}
			}
		} else {
			WorkOrderEmployee::deleteAll(['id_work_order' => $this->id]);
		}
//		$ws = Yii::$app->request->bodyParams['id_workers'];
//		if ($ws != '') {
//			$new_workers = explode(',', $ws);
//			if (isset(Yii::$app->request->bodyParams['id_master'])) {
//				$new_workers[] = Yii::$app->request->bodyParams['id_master'];
//			}
//			Yii::getLogger()->log(print_r($new_workers, true), Logger::LEVEL_WARNING);
//			$exist_workers = WorkOrderEmployee::find()
//				->select('id_worker')
//				->where(['id_work_order' => $this->id])
//				->column();
//			//print_r($exist_workers);
//			Yii::getLogger()->log(print_r($exist_workers, true), Logger::LEVEL_WARNING);
//			// проверяем, может ничего и не изменилось
//			foreach ($exist_workers as $ew) {
//				if (array_search($ew, $new_workers)===false) {
//					WorkOrderEmployee::deleteAll(['and', ['id_work_order' => $this->id], ['id_worker' => $ew]]);
//				}
//			}
//
//			foreach ($new_workers as $id_worker) {
//				if (array_search($id_worker, $exist_workers)===false) {
//					$w = new WorkOrderEmployee();
//					$w->id_work_order = $this->id;
//					$w->id_worker = $id_worker;
//					$w->save();
//				}
//			}
//		} else {
//			WorkOrderEmployee::deleteAll(['id_work_order' => $this->id]);
//		}

	}
	/*------------------------------------------------------------------------------*/
    public function getId() {
    	return $this->id_work_order;
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLines()
    {
        return $this->hasMany(WorkOrderLine::className(), ['id_work_order' => 'id_work_order']);
    }
    /*-*/
	public function setLines($lines)
	{
		$this->populateRelation('lines', $lines);
		$this->lines_count = count($lines);
	}

	/*------------------------------------------------------------------------------*/
	public function getEmployees() {
		return $this->hasMany(WorkOrderEmployee::className(), ['id_work_order' => 'id_work_order']);
	}
	/*------------------------------------------------------------------------------*/
	public function getStatus()
	{
		return $this->hasOne(SprStatusWorkOrder::className(), ['id_status_work_order' => 'id_status']);
	}
	/*------------------------------------------------------------------------------*/
	public function getTypeWork()
	{
		return $this->hasOne(SprTypeWorkOrder::className(), ['id_type_work_order' => 'id_type_work_order']);
	}
    /*------------------------------------------------------------------------------*/
    public function fields()
    {
	    $fields = parent::fields();

	    $fields['id'] = function () {
		    return $this->id;
	    };

	    $fields['type_work_order'] = function () {
	        return $this->typeWork->type_work_order;
	    };

//	    $fields['brigade'] = function () {
//	        return explode(',', $this->brigade);
//	    };

	    $fields['status'] = function () {
		    return $this->status->status_work_order;
	    };

	    $fields['id_unit'] = function () {
		    return (string)$this->id_unit;
	    };

	    $empl = $this->employees;
	    $id_master = null;
	    $brigade = [];
	    foreach ($empl as $item) {
	    	switch ($item->trade) {
			    case 'master':
				    $id_master = $item->id_worker;
			    	break;
			    case 'brigade':
				    $brigade[] = $item->id_worker;
				    break;
		    }
	    }
	    if (count($brigade)>0) {
		    $fields['id_brigade'] = function () use ($brigade) {
			    return implode(',', $brigade);
		    };
	    }
	    if ($id_master) {
		    $fields['id_master'] = function () use ($id_master) {
			    return $id_master;
		    };
	    }
//	    $fields['id_brigade'] = function () {
//		    return $this->employees;
//	    };
//
//	    if ()

//	    $fields['date_work_order_start'] = function () {
//		    return explode(' ', $this->date_work_order_start)[0];
//	    };
//	    $fields['date_work_order_end'] = function () {
//		    return explode(' ', $this->date_work_order_end)[0];
//	    };

	    $fields['date_work_order_browse'] = function () {
		    return Yii::$app->formatter->asDate($this->date_work_order_end);
	    };
//	    $fields['lines'] = function () {
//		    return $this->lines;
//	    };
	    return $fields;
    }
	/*------------------------------------------------------------------------------*/
    public function extraFields()
    {
    	$fields = parent::extraFields();

	    $fields['safety_equipment'] = function () {
		    return $this->lines[0]->work->safety_equipment;
	    };
    	$fields['lines'] = function () {
    		return $this->lines;
	    };


	    return $fields;
    }
	/*------------------------------------------------------------------------------*/
	/*------------------------------------------------------------------------------*/

	/*------------------------------------------------------------------------------*/
	public static function find() {
		$sql = parent::find()->with(['typeWork', 'status', 'employees']);
		return $sql;
	}
	/*------------------------------------------------------------------------------*/
}


