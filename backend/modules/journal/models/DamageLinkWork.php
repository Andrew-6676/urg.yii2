<?php

namespace common\models\gasb;

use Yii;

/**
 * This is the model class for table "damage_link_work".
 *
 * @property integer $id_damage_link_work
 * @property integer $id_type_damage
 * @property integer $id_work
 */
class DamageLinkWork extends \common\components\myModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'damage_link_work';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_type_damage', 'id_work'], 'required'],
            [['id_type_damage', 'id_work'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_damage_link_work' => 'Id Damage Link Work',
            'id_type_damage' => 'Id Type Damage',
            'id_work' => 'Id Work',
        ];
    }
}
