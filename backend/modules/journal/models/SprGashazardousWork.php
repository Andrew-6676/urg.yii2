<?php

namespace app\modules\journal\models;

use Yii;

/**
 * This is the model class for table "spr_gashazardous_work".
 *
 * @property integer $id_gashazardous_work
 * @property string $gashazardous_work
 * @property integer $id_type_gashazardous_work
 * @property string $type_obj
 * @property string $requirement
 * @property string $safety_equipment
 * @property string $timerate_plan
 */
class SprGashazardousWork extends \common\components\myModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'spr_gashazardous_work';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['gashazardous_work', 'id_type_gashazardous_work', 'type_obj', 'requirement', 'safety_equipment', 'timerate_plan'], 'required'],
            [['id_type_gashazardous_work'], 'integer'],
            [['requirement'], 'string'],
            [['timerate_plan'], 'number'],
            [['gashazardous_work', 'safety_equipment'], 'string', 'max' => 300],
            [['type_obj'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_gashazardous_work' => 'Id Gashazardous Work',
            'gashazardous_work' => 'Gashazardous Work',
            'id_type_gashazardous_work' => 'Id Type Gashazardous Work',
            'type_obj' => 'Type Obj',
            'requirement' => 'Requirement',
            'safety_equipment' => 'Safety Equipment',
            'timerate_plan' => 'Timerate Plan',
        ];
    }


	/*------------------------------------------------------------------------------*/
	/*------------------------------------------------------------------------------*/
	/*------------------------------------------------------------------------------*/
	/*------------------------------------------------------------------------------*/

}
