<?php

namespace app\modules\journal\models;

use Yii;

/**
 * This is the model class for table "spr_status_work_order".
 *
 * @property integer $id_status_work_order
 * @property string $status_work_order
 */
class SprStatusWorkOrder extends \common\components\myModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'spr_status_work_order';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status_work_order'], 'required'],
            [['status_work_order'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_status_work_order' => 'Id Status Work Order',
            'status_work_order' => 'Status Work Order',
        ];
    }
}
