<?php

namespace app\modules\journal\models;

use Yii;

/**
 * This is the model class for table "work_order_employee".
 *
 * @property integer $id
 * @property integer $id_work_order
 * @property integer $id_worker
 * @property string $trade
 */
class WorkOrderEmployee extends \common\components\myModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'work_order_employee';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_work_order', 'id_worker', 'trade'], 'required'],
            [['id_work_order', 'id_worker'], 'integer'],
            [['trade'], 'string', 'max' => 20],
            [['id_work_order'], 'exist', 'skipOnError' => true, 'targetClass' => WorkOrder::className(), 'targetAttribute' => ['id_work_order' => 'id_work_order']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_work_order' => 'Id Work Order',
            'id_worker' => 'Id Worker',
            'trade' => 'Trade',
        ];
    }
}
