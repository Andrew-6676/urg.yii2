<?php

namespace app\modules\journal\models;

use Yii;

/**
 * This is the model class for table "damage_spr_type".
 *
 * @property integer $id_type_damage
 * @property string $type_damage
 *
 * @property Damage[] $damages
 */
class DamageSprType extends \common\components\myModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'damage_spr_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_damage'], 'required'],
            [['type_damage'], 'string', 'max' => 300]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_type_damage' => 'Id Type Damage',
            'type_damage' => 'Type Damage',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDamages()
    {
        return $this->hasMany(Damage::className(), ['id_type_damage' => 'id_type_damage']);
    }
}
