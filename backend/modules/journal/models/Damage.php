<?php

namespace app\modules\journal\models;

use app\modules\journal\models\DamageSprType;
use common\models\urg\LinkObj;
use Yii;

/**
 * This is the model class for table "damage".
 *
 * @property integer $id_obj
 * @property string $name
 * @property integer $id_type_damage
 * @property string $date_determination
 * @property string $time_message
 * @property string $date_fix
 * @property string $determinant
 * @property string $fixer
 * @property string $work_fix
 * @property integer $id_profycenter
 * @property string $reason
 * @property string $quantity
 * @property integer $id_type_measure
 * @property string $remark
 *
 * @property DamageSprType $idTypeDamage
 * @property DamageSprType $idTypeDamage0
 */
class Damage extends \common\components\myModel
{
	const SCENARIO_EXPORT = 'export';
	public $address = '-';
	public $id_address = -1;
//	public function scenarios()	{
//		$scenarios = parent::scenarios();
//		$scenarios[self::SCENARIO_EXPORT] = ['id_obj', 'date_determination'];
//		//$scenarios[self::SCENARIO_REGISTER] = ['username', 'email', 'password'];
//		return $scenarios;
//	}
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'damage';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['id_obj', 'id_type_damage', 'time_message', 'determinant', 'fixer', 'work_fix', 'id_profycenter', 'quantity', 'id_type_measure'], 'required'],
			[['id_obj', 'id_type_damage', 'id_profycenter', 'id_type_measure'], 'integer'],
			[['date_determination', 'time_message', 'date_fix'], 'safe'],
			[['quantity'], 'number'],
			[['name', 'determinant', 'fixer', 'reason', 'remark'], 'string', 'max' => 200],
			[['work_fix'], 'string', 'max' => 250]
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id_obj' => 'Id Obj',
			'name' => 'Name',
			'id_type_damage' => 'Id Type Damage',
			'date_determination' => 'Date Determination',
			'time_message' => 'Time Message',
			'date_fix' => 'Date Fix',
			'determinant' => 'кто обнаружил',
			'fixer' => 'кто устранил',
			'work_fix' => 'работы по устранению',
			'id_profycenter' => 'Id Profycenter',
			'reason' => 'Reason',
			'quantity' => 'Quantity',
			'id_type_measure' => 'Id Type Measure',
			'remark' => 'Remark',
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getTypeDamage()
	{
		return $this->hasOne(DamageSprType::className(), ['id_type_damage' => 'id_type_damage']);
	}

	/*----------------------------------------------------------------------------------*/
	// link_obj - если Damage чей-то дети
	public function getLinkObjAsChild() {
		return $this->hasOne(LinkObj::className(), ['id_obj2' => 'id_obj']);
	}
	/*----------------------------------------------------------------------------------*/
	public function fields()
	{
		$fields = [];
		if (isset($_GET['mode']) && $_GET['mode']=='export') {
			$fields['id_obj'] = 'id_obj';
			$fields['date_determination'] = 'date_determination';
			$fields['time_message'] = 'time_message';
			$fields['id_address'] = function () {
				return $this->id_address;
			};
			$fields['address'] = function () {
				return $this->address;
			};
			$fields['typeDamage'] = function () {
				return $this->typeDamage->type_damage;
			};
		} else {
			$fields = parent::fields();
		}

		return $fields;
	}
	/*----------------------------------------------------------------------------------*/
	public function extraFields()
	{
		$fields = parent::fields();

		return $fields;
	}
}

