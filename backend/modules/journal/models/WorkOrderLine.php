<?php

namespace app\modules\journal\models;

use Yii;

/**
 * This is the model class for table "work_order_line".
 *
 * @property integer $id_work_order_line
 * @property integer $id_work_order
 * @property integer $id_link_obj
 * @property integer $id_gashazardous_work
 * @property boolean $done
 * @property string $timerate_fact
 * @property string $remark
 *
 * @property WorkOrder $idWorkOrder
 */
class WorkOrderLine extends \common\components\myModel
{

	public $obj = '-';
	public $quantity = -1;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'work_order_line';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_work_order', 'id_link_obj', 'id_gashazardous_work'], 'required'],
	        [['id_work_order', 'id_link_obj', 'id_gashazardous_work'], 'integer'],
	        [['done'], 'boolean'],
            [['timerate_fact'], 'number'],
            [['remark'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_work_order_line' => 'Id Work Order Line',
            'id_work_order' => 'Id Work Order',
            'id_link_obj' => 'Id Link Obj',
            'id_gashazardous_work' => 'Id Gashazardous Work',
            'done' => 'Done',
            'timerate_fact' => 'Timerate Fact',
            'remark' => 'Remark',
        ];
    }
	/*----------------------------------------------------------------------------------*/
	public function init()
	{
		parent::init();

		$this->timerate_fact   = 0;
		$this->done            = 0;
	}
	/*------------------------------------------------------------------------------*/
	public function getId() {
		return $this->id_work_order_line;
	}
	/*----------------------------------------------------------------------------------*/
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkOrder()
    {
        return $this->hasOne(WorkOrder::className(), ['id_work_order' => 'id_work_order']);
    }

	/*----------------------------------------------------------------------------------*/
	// link_obj - если УРГ чей-то дети
	public function getLinkObjAsChild() {
		return $this->hasOne(LinkObj::className(), ['id_obj2' => 'id_link_obj']);
	}
	/*----------------------------------------------------------------------------------*/
	// link_obj - если УРГ чей-то родитель
	public function getLinkObjAsParent() {
		return $this->hasMany(LinkObj::className(), ['id_obj1' => 'id_link_obj']);
	}
	/*----------------------------------------------------------------------------------*/
	// адрес
	public function getAddress() {
		return $this->hasOne(Address::className(), ['id_link_obj' => 'id_obj2'])
			->via('linkObjAsParent');
	}
	/*----------------------------------------------------------------------------------*/
	public function getWork()
    {
        return $this->hasOne(SprGashazardousWork::className(), ['id_gashazardous_work' => 'id_gashazardous_work']);
    }
	/*------------------------------------------------------------------------------*/
	public function fields()
	{
		$fields = parent::fields();

		$fields['id'] = function () {
			return $this->id;
		};
		$fields['id_status'] = function () {
			return $this->workOrder->id_status;
		};
		$fields['id_link_obj'] = function () {
			return (string)$this->id_link_obj;
		};
		$fields['timerate_plan'] = function () {
			return $this->work->timerate_plan;//*damage.quantity;
		};
		$fields['timerate_fact'] = function () {
			if ($this->timerate_fact)
				return $this->timerate_fact;
			else
				return $this->work->timerate_plan*$this->quantity;
		};
		$fields['quantity'] = function () {
			return $this->quantity;
		};
		$fields['timerate'] = function () {
			return $this->work->timerate_plan*$this->quantity;
		};
		$fields['timerate_str'] = function () {
			//return $this->work->timerate_plan*$this->quantity;
			return $this->quantity .' x '. $this->work->timerate_plan .' = '.$this->work->timerate_plan*$this->quantity;
		};
		$fields['obj'] = function () {
			return $this->obj;
		};
		$fields['gashazardous_work'] = function () {
			return $this->work->gashazardous_work;
		};
		$fields['safety_equipment'] = function () {
			return $this->work->safety_equipment;
		};
		return $fields;
	}
	/*------------------------------------------------------------------------------*/
	public function extraFields()
	{
		$fields = parent::extraFields();
		return $fields;
	}
	/*------------------------------------------------------------------------------*/
	/*------------------------------------------------------------------------------*/
	/*------------------------------------------------------------------------------*/
	/*------------------------------------------------------------------------------*/
	public static function find() {
		$sql = parent::find()
			->with(['work', 'workOrder.status'])
			->select('work_order_line.*, getFullAddress(id_obj1)  as obj, quantity, id_obj2')
			->leftJoin('link_obj', 'link_obj.id_link_obj=work_order_line.id_link_obj')
			->leftJoin('damage', 'damage.id_obj=link_obj.id_obj2');
		return $sql;
	}
	/*------------------------------------------------------------------------------*/

}
