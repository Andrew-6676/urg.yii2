<?php

namespace app\models\drupal;

//use app\models\drupal\drupal_Sessions;

class drupal_User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
//    public $id;
//    public $username;
//    public $password;
	public $authKey;
	public $accessToken;

	public static function tableName()
	{
		return 'users';
	}

	public static function getDb()
	{
		return \Yii::$app->get('db_drupal');
	}

	/**
	 * @inheritdoc
	 */
	public static function findIdentity($id)
	{
		return static::findOne($id);
	}

	/**
	 * @inheritdoc
	 */
	public static function findIdentityByAccessToken($token, $type = null)
	{
		return static::findOne(['access_token' => $token]);
	}

	/*---------------------------------------*/
	public static function findBySid($sid, $type = null)
	{
		return static::find()->joinWith('sessions')->where('sessions.sid=:sid', ['sid'=>$sid])->one();
	}

	/**
	 * Finds user by username
	 *
	 * @param  string      $username
	 * @return static|null
	 */

	public static function findByUsername($username)
	{
		return static::findOne(['name' => $username]);
	}

	/**
	 * @inheritdoc
	 */
	public function getId()
	{
		return $this->uid;
	}

	/**
	 * @inheritdoc
	 */
	public function getAuthKey()
	{
		return $this->authKey;
	}

	/**
	 * @inheritdoc
	 */
	public function validateAuthKey($authKey)
	{
		return $this->authKey === $authKey;
	}

	/**
	 * Validates password
	 *
	 * @param  string  $password password to validate
	 * @return boolean if password provided is valid for current user
	 */
	public function validatePassword($password)
	{
		return $this->pass === $password;
	}


	public function getSessions()
	{
		return $this->hasMany(drupal_Sessions::className(), ['uid' => 'uid']);
	}
}
