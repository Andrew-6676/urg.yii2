<?php

namespace app\models\drupal;

use Faker\Provider\bn_BD\Utils;
use Yii;

/**
 * This is the model class for table "sessions".
 *
 * @property integer $uid
 * @property string $sid
 * @property string $hostname
 * @property integer $timestamp
 * @property integer $cache
 * @property string $session
 */
class drupal_Sessions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sessions';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
	    //if (strlen(Yii::$app->session['sid']) == 32 || strlen(Yii::$app->session['sid']) == 0) {
		   // echo '---------32--------------';
		    return Yii::$app->get('db_drupal');
	    //} else {
		   // echo '---------58--------------';
		   // return Yii::$app->get('db_drupal2');
	   // }
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['uid', 'timestamp', 'cache'], 'integer'],
            [['sid'], 'required'],
            [['session'], 'string'],
            [['sid'], 'string', 'max' => 32],
            [['hostname'], 'string', 'max' => 128]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'uid' => 'Uid',
            'sid' => 'Sid',
            'hostname' => 'Hostname',
            'timestamp' => 'Timestamp',
            'cache' => 'Cache',
            'session' => 'Session',
        ];
    }


    /*------------------------------------------*/

	public function getUser()
	{
		return $this->hasOne(drupal_Users::className(), ['uid' => 'uid']);
	}

	/*-----------------------------------------*/
	public static function UserData($sid) {
		$res = self::find()->with('user')->where('sid=:sid', array('sid'=>$sid))->one();
		//Utils::print_r($res->user->name);
		return unserialize($res->user->data);
	}
}
