<?php
namespace backend\controllers;

use app\components\myActiveController;
use Yii;


class TypeobjectController extends myActiveController
{
	public $modelClass = 'common\models\urg\SprTypeObj';

	public function behaviors()	{
		return
			\yii\helpers\ArrayHelper::merge(parent::behaviors(), [
				'corsFilter' => [
					'class' => \yii\filters\Cors::className(),
				],
			]);
	}

	public function actions()
	{
		$actions = parent::actions();

		$actions['index']['prepareDataProvider'] = [$this, 'prepareListProvider'];

		// отключить действия "delete" и "create"
		//unset($actions['delete'], $actions['create']);

		return $actions;
	}

	public function prepareListProvider() {
		$adp = parent::prepareDataProvider();
		$adp->pagination->defaultPageSize = 99999999;
		return $adp;
	}
}
