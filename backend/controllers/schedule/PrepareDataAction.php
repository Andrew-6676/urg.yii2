<?php
namespace app\controllers\schedule;

use common\models\urg\Obj;
use common\models\urg\Urg;
use common\models\urg\WorksSchedule;
use Yii;
use yii\base\Model;
use yii\rest\Action;
use yii\data\ActiveDataProvider;

class prepareDataAction extends Action {

//	public $prepareDataProvider;
//	public $scenario = Model::SCENARIO_DEFAULT;

	public function run() {

		$request = Yii::$app->request;
		if ($request->isOptions) {
			$options = ['POST', 'PUT', 'OPTOINS'];
			Yii::$app->getResponse()->getHeaders()->set('Allow', implode(',', $options));
			Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Methods', implode(',', $options));

			$res = implode(',', $options);
			return $res;
		}


		if ($request->isPost) {
			$res = [
				'status' => 'unknown',
				'message'=> '-',
			];

//			this.groups = {
//				1: {name: 'Виноградов В.О.'},
//			    2: {name: 'Корнеев А.А.'},
//		};
//		/*---------------------------------------*/
//		this.obj_headers = [
//			'ФИО',
//			'Номер КРД',
//			'Адрес',
//			'Регулятор',
//			'Выходное давление',
//		];
//		/*---------------------------------------*/
//		this.objects['4440892'] = {
//				id: 4440892,
//			id_group: 1,
//			id_type_urg: 1005,
//			name: 'КРД-1',
//			data: [
//					'г. Витебк, ул.Уличная, 22',
//					'РДНК-400',
//					'низкое',
//				],
//			// по нужным видам работ 3-5 последних дат выполнения работ, отсортированных по убыванию - {id_work : [last_date, pre-last_date,...]}
//			// 1184 - id работы из справочника
//			'works': {
//					1185: [new Date('2017-12-15'), new Date('2017-11-16')],
//			}
//		};

			$res['data'] = $this->controller->prepareData($request->bodyParams['objs']);
			// должен вернуть objects, obj_headers,  groups
//			$res['objects'] = [];
//			$res['obj_headers'] = [
//				'ФИО',
//				'Номер КРД',
//				'Адрес',
//				'Регулятор',
//				'Выходное давление'
//			];
//			$res['groups'] = [];
//
//			$tmp_objects = Urg::find()
//				->where(['in', 'id_obj', $request->bodyParams['objs']])
//				->with(['type', 'address', 'master'])
//				->indexBy('id_obj')
//				->all();
//
//				// формируем группы
//			$tmp_groups = [];
//			foreach ($tmp_objects as $id_obj => $obj) {
//				$tmp_groups[$obj->master->name] = 1;
//			}
//
//			$i = 1;
//			$tmp_groups2 = [];
//			foreach ($tmp_groups as $gr => $none) {
//				$tmp_groups2[$gr] = $i++;
//			}
//
//			foreach ($tmp_objects as $id_obj => $obj) {
//				$o = $obj->toArray([], ['type', 'master', 'full_address']);
//				$res['objects'][] = [
//					'id' => $id_obj,
//					'id_group' => $tmp_groups2[$o['master']['name']],
//					'id_type_urg' => $o['id_type_urg'],
//					'name' => $o['type']['name'].'-'.$o['num'],
//					'data' => [
//							$o['full_address'],
//							'<регулятор>',
//							'<давление>',
//					]
//				// по нужным видам работ 3-5 последних дат выполнения работ, отсортированных по убыванию - {id_work : [last_date, pre-last_date,...]}
//		    	// 1184 - id работы из справочника
////				'works': {
////						1185: [new Date('2017-12-15'), new Date('2017-11-16')],
////				}
//				];
//			}
//
//			foreach ($tmp_groups2 as $name => $gid) {
//				$res['groups'][$gid] = ['name'=>$name];
//			}

				// должен вернуть
			//$res['groups']     = [];
			//$res['objects']     = [];
			//$res['obj_headers'] = [];
				// objects отсортировать по мастеру и номеру
			$res['status']  = 'ok';
			$res['message'] = 'Данные для графика подготовлены';

			return $res;
		}

		//Yii::$app->getResponse()->setStatusCode(405);
		return 'wrong request';
	}

}
