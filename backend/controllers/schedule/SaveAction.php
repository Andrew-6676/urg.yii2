<?php
namespace app\controllers\schedule;

use common\models\urg\Obj;
use common\models\urg\Urg;
use common\models\urg\WorksSchedule;
use Yii;
use yii\base\Model;
use yii\rest\Action;
use yii\data\ActiveDataProvider;

class saveAction extends Action {

//	public $prepareDataProvider;
//	public $scenario = Model::SCENARIO_DEFAULT;

	public function run($id=-1) {

		$request = Yii::$app->request;
		if ($request->isOptions) {
			$options = ['POST', 'PUT', 'OPTOINS'];
			Yii::$app->getResponse()->getHeaders()->set('Allow', implode(',', $options));
			Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Methods', implode(',', $options));

			$res = implode(',', $options);
			return $res;
		}


		if ($request->isPost || $request->isPut) {
			$res = [
				'id' => $id,
				'status' => 'unknown',
				'message'=> '-',
			];

			if ($id<0) {
				$model = new WorksSchedule();
				$res['action'] = 'insert';
			} else {
				$model = WorksSchedule::find()->where(['id'=>$id])->one();
				$id_unit = $model->id_unit;
				$res['params1'] = $model->id_unit;
				$res['action'] = 'update';
			}

			//$res['model1'] = print_r($model, true);
			$model->load(Yii::$app->request->bodyParams, '');
			if ($id>0) {
				$model->id_unit = $id_unit;
			}
			//$res['model2'] = $model;
			//$res['params'] = Yii::$app->request->bodyParams;
			if ($model->save() !== false) {
				$res['status'] = 'ok';
				$res['message'] = 'График сохранён';
				$res['id'] = $model->id;
			} else {
				$res['status'] = 'error';
				$res['message'] = 'Во время сохранения графика возникла ошибка.';
				$res['errors'] = $model->errors;
			}

			return $res;
		}

		//Yii::$app->getResponse()->setStatusCode(405);
		return 'wrong request';
	}

}
