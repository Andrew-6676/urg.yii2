<?php
namespace app\controllers\schedule;

use common\models\urg\Obj;
use common\models\urg\Urg;
use common\models\urg\WorksSchedule;
use Yii;
use yii\base\Model;
use yii\rest\Action;
use yii\data\ActiveDataProvider;

class viewAction extends Action {

//	public $prepareDataProvider;
//	public $scenario = Model::SCENARIO_DEFAULT;

	public function run($id) {

		$request = Yii::$app->request;
		if ($request->isOptions) {
			$options = ['POST', 'PUT', 'OPTOINS'];
			Yii::$app->getResponse()->getHeaders()->set('Allow', implode(',', $options));
			Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Methods', implode(',', $options));

			$res = implode(',', $options);
			return $res;
		}


		if ($request->isGet) {
			$res = [
				'status' => 'unknown',
				'message'=> '-',
			];

			$schedule = WorksSchedule::findOne($id);

			$cells_data = json_decode($schedule->data);
//			if ($schedule->type=='y') {
//				foreach ($cells_data as $item) {
//
//				}
//			}

			$res['data'] = $this->controller->prepareData(json_decode($schedule->objects));
			$res['data']['cellsData'] = $cells_data;
			$res['data']['period']    = $schedule->period;
			$res['data']['type']      = $schedule->type;
			$res['data']['title']     = $schedule->title;
			$res['data']['author']    = $schedule->author;
			$res['data']['id_unit']   = $schedule->id_unit;
			$res['data']['post']      = $schedule->post;

			$res['status']  = 'ok';
			$res['id']      = $id;
			$res['message'] = 'Данные для графика подготовлены';

			return $res;
		}

		//Yii::$app->getResponse()->setStatusCode(405);
		return 'wrong request';
	}

}
