<?php
namespace app\controllers\schedule;

use common\models\urg\Obj;
use common\models\urg\Urg;
use common\models\urg\WorksCfg;
use common\models\urg\WorksSchedule;
use Yii;
use yii\base\Model;
use yii\rest\Action;
use yii\data\ActiveDataProvider;

class getWorksCfgAction extends Action {

//	public $prepareDataProvider;
//	public $scenario = Model::SCENARIO_DEFAULT;

	public function run($id=-1) {

		$request = Yii::$app->request;
		if ($request->isOptions) {
			$options = ['GET', 'OPTOINS'];
			Yii::$app->getResponse()->getHeaders()->set('Allow', implode(',', $options));
			Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Methods', implode(',', $options));

			$res = implode(',', $options);
			return $res;
		}


		if ($request->isGet) {

			$rows = WorksCfg::find()->asArray()->all();

			$worksCfg = [];
			foreach ($rows as $row) {
				$worksCfg[$row['id_type_urg']][$row['id_work']] = json_decode($row['config']);
			}
			$res['worksCfg'] = $worksCfg;
			$res['status']   = 'ok';

			return $res;
		}

		//Yii::$app->getResponse()->setStatusCode(405);
		return 'wrong request';
	}

}
