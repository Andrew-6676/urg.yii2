<?php
namespace app\controllers\schedule;

use common\models\urg\Obj;
use common\models\urg\Urg;
use common\models\urg\WorksCfg;
use common\models\urg\WorksSchedule;
use Yii;
use yii\base\Model;
use yii\rest\Action;
use yii\data\ActiveDataProvider;

class saveWorksCfgAction extends Action {

//	public $prepareDataProvider;
//	public $scenario = Model::SCENARIO_DEFAULT;

	public function run($id=-1) {

		$request = Yii::$app->request;
		if ($request->isOptions) {
			$options = ['POST', 'PUT', 'OPTOINS'];
			Yii::$app->getResponse()->getHeaders()->set('Allow', implode(',', $options));
			Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Methods', implode(',', $options));

			$res = implode(',', $options);
			return $res;
		}


		if ($request->isPost || $request->isPut) {
			$res = [
				'status' => 'unknown',
				'message'=> '-',
			];

			$rows = WorksCfg::deleteAll();

			$res['rows'] = [];
			foreach ($request->bodyParams as $urg=>$works) {
				foreach ($works as $work=>$cfg) {
					$res['rows'][] = [$work, $urg, json_encode($cfg)];

				}
			}
			$r = Yii::$app->db->createCommand()->batchInsert('works_cfg', ['id_work','id_type_urg','config'], $res['rows'])->execute();

			if ($r>0) {
				$res['status'] = 'ok';
				$res['message'] = 'Настройки работ сохранены';
			} else {
				$res['status'] = 'error';
				$res['message'] = 'Ошибка сохранения настроек';
			}
			$res['worksCfg'] = $request->bodyParams;
			return $res;
		}

		//Yii::$app->getResponse()->setStatusCode(405);
		return 'wrong request';
	}

}
