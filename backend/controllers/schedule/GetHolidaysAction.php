<?php
namespace app\controllers\schedule;

use common\models\urg\Obj;
use common\models\urg\SprPrazd;
use common\models\urg\SprZamen;
use common\models\urg\Urg;
use common\models\urg\UrgRefbook;
use common\models\urg\WorksSchedule;
use Yii;
use yii\base\Model;
use yii\helpers\Json;
use yii\rest\Action;
use yii\data\ActiveDataProvider;

class getHolidaysAction extends Action {

//	public $prepareDataProvider;
//	public $scenario = Model::SCENARIO_DEFAULT;

	public function run() {

		$request = Yii::$app->request;
		if ($request->isOptions) {
			$options = ['GET', 'OPTOINS'];
			Yii::$app->getResponse()->getHeaders()->set('Allow', implode(',', $options));
			Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Methods', implode(',', $options));

			$res = implode(',', $options);
			return $res;
		}


		if ($request->isGet) {
			$res = [
				'status' => 'ok',
				'message'=> '-',
			];

			// забираем праздники и переносы из базы защиты газопроводов
			$h_url = "http://protection.topgas.by/models/holydays.php";

			$hd = file_get_contents($h_url);
			if (!$hd) {
				$res = [
					'status' => 'error',
					'message'=> 'Не удалось получить список праздничных дней',
					'workdays' => [],
					'holidays' => []
				];
				return $res;
			}

			$data = Json::decode($hd);
			$holydays = [];
			$workdays = [];
			foreach ($data as $date) {
				if ($date['type_holyday']==0)
					$holydays[] = $date['date_holyday'];
				else
					$workdays[] = $date['date_holyday'];
			}

//			$prazd = SprPrazd::find()
//				->select('name_prazd')
//				->column();
//			$perenos = SprZamen::find()
//				->select(['day_r', 'day_z'])
//				->asArray()
//				->all();
//
//			$holydays = [];
//			$workdays = [];
//
//			foreach ($prazd as $date) {
//				$holydays[] = $date;
//			}
//
//			// day_r - рабочий день, который перенесли на  day_z
//			foreach ($perenos as $dates) {
//				$holydays[] = $dates['day_r'];
//				$workdays[] = $dates['day_z'];
//			}

			$res['workdays'] = $workdays;
			$res['holidays'] = $holydays;

			return $res;
		}

		//Yii::$app->getResponse()->setStatusCode(405);
		return 'wrong request';
	}

}
