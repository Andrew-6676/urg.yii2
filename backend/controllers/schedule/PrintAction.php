<?php
namespace app\controllers\schedule;

use backend\controllers\RefbookController;
use common\models\urg\Obj;
use common\models\urg\Urg;
use common\models\urg\UrgRefbook;
use common\models\urg\WorksSchedule;
use PHPExcel_Cell;
use PHPExcel_IOFactory;
use PHPExcel_RichText;
use PHPExcel_Style_Alignment;
use PHPExcel_Style_Border;
use PHPExcel_Style_Fill;
use Yii;
use yii\base\Model;
use yii\helpers\Json;
use yii\rest\Action;
use yii\data\ActiveDataProvider;

class printAction extends Action {

//	public $prepareDataProvider;
//	public $scenario = Model::SCENARIO_DEFAULT;
	private $reader;


	private $styles = [
		'bordered_thin' => [
			'borders' => [
				'allborders' => [
					'style' => PHPExcel_Style_Border::BORDER_THIN
				]
			]
		],
		'align_center' => [
			'alignment' => [
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
			]
		],
		'align_right' => [
			'alignment' => [
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
				'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
			]
		],
		'bordered_bold' => [
			'font' => [
				'bold' => true
			],
			'borders' => [
				'outline' => [
					'style' => PHPExcel_Style_Border::BORDER_MEDIUM
				]
			]
		],
		'border_bottom' => [
			'borders' => [
				'bottom' => [
					'style' => PHPExcel_Style_Border::BORDER_THIN
				]
			]
		],
		'bold' => [
			'font' => [
				'bold' => true
			]
		],
//		'fill' => [
//			'fill' =>  [
//				'type' => PHPExcel_Style_Fill::FILL_SOLID,
//				'color' => ['rgb' => 'FFFF00']
//			]
//		],
		'border_medium' => [
			'borders' => [
				'outline' => [
					'style' => PHPExcel_Style_Border::BORDER_MEDIUM
				]
			],
		],
		'with_telemetry' => [
			'fill' =>  [
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => ['rgb' => 'd9efe8']
			]
		],
		'holidays' => [
			'fill' =>  [
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => ['rgb' => 'ffdbdb']
			]
		]
	];

	private $works = [
		1185 => ['color' => 'ffff00', 'name'=>''],
		1186 => ['color' => '3ccd00', 'name'=>''],
		1187 => ['color' => '0000ff', 'name'=>''],
		1188 => ['color' => 'ff0000', 'name'=>'']
	];
	private $holydays = [];
	private $workdays = [];

	/*-----------------------------------------------------------------*/
	public function run($id) {

		$request = Yii::$app->request;
		if ($request->isOptions) {
			$options = ['GET', 'OPTOINS'];
			Yii::$app->getResponse()->getHeaders()->set('Allow', implode(',', $options));
			Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Methods', implode(',', $options));

			$res = implode(',', $options);
			return $res;
		}

		if ($request->isPost) {
			$res = [
				'status' => 'unknown',
				'message'=> '-',
				'params' => $request->bodyParams
			];
			/*----------------------*/
			$h_url = "http://protection.topgas.by/models/holydays.php";
			$hd = file_get_contents($h_url);
			if (!$hd) {
				$res = [
					'status' => 'error',
					'message'=> 'Не удалось получить список праздничных дней',
				];
			}
			$data = Json::decode($hd);

			$holydays = [];
			$workdays = [];

			foreach ($data as $date) {
				if ($date['type_holyday']==0)
					$this->holydays[] = $date['date_holyday'];
				else
					$this->workdays[] = $date['date_holyday'];
			}

			/*----------------------*/
			$tmp_works = UrgRefbook::find()
				->select(['id', 'name', 'descr'])
				->where(['id_parent'=>61])
				->asArray()
				->all();

			$this->works = [];
			foreach ($tmp_works as $work) {
				$this->works[$work['id']] = [
					'name'  => $work['name'],
					'color' => substr($work['descr'],-6)
				];
			}
			//print_r($works);
			$suffix = '';
			$unit = $request->bodyParams['unit'];

			$schedule = WorksSchedule::find()->where(['id' => $request->bodyParams['id']])->one();
			$objects = \yii\helpers\Json::decode($schedule->objects);
			$rows_data = $this->controller->prepareData($objects);

			$f = explode(' ', $schedule['author']);
			$names = [
				'author' => [
					'post' => $schedule['post'],
					'fio' => $f[0].' '.mb_substr($f[1], 0,1).'. '.mb_substr($f[2], 0, 1).'.',
				],
				'approve' => [
					'post' => $request->bodyParams['appr_post'],
					'fio' => $request->bodyParams['appr_fio'],
				]
			];

			$cells_data = [];
			$cells = \yii\helpers\Json::decode($schedule->data);
			foreach ($cells as $cell => $cell_data) {
				$rc = explode('_', $cell);
				$cells_data[$rc[0]][$rc[1]] = $cell_data;
			}

			//print_r($cells_data);
			//exit;
			$suffix = $schedule->id_unit;

			$this->reader = PHPExcel_IOFactory::createReader('Excel5');

			if ($schedule->type=='m') {
				$filename = 'ГРП-4_'.$suffix.'.xls';
				$excel = $this->print_4grp($cells_data, $rows_data, $schedule, $names);
			} else {
				$filename = 'ГРП-5_'.$suffix.'.xls';
				$excel = $this->print_5grp($cells_data, $rows_data, $schedule, $names);
			}

			$excel->setActiveSheetIndex(0);
			$sheet = $excel->getActiveSheet();
			$sheet->setCellValue("A1", $unit);

			$writer = PHPExcel_IOFactory::createWriter($excel, 'Excel5');
			$writer->save(Yii::getAlias('@webroot/templates/tmp/').$filename);

			$res['status']  = 'ok';
			$res['link']    = '/templates/tmp/'.$filename;
			//$res['message'] = 'Ссылка на excel';
//			$res['test'] = [
//				Yii::getAlias('@web/templates/tmp/'),
//				Yii::getAlias('@webroot/templates/tmp/'),
//			];

			return $res;
		}

		//Yii::$app->getResponse()->setStatusCode(405);
		return 'wrong request';
	}

	/*--------------------------------------------------------------------------------------------------------------------------------*/
	/*--------------------------------------------------------------------------------------------------------------------------------*/

	private function print_4grp($cells_data, $rows_data, $schedule, $names) {

		/*
			 * ГРП-4 (данные со строки 11)
			 столбец A - Наименование УРГ
			 столбец B - адрес
			 столбец С-AG - дни месяца
			 */

		//$this->$reader = PHPExcel_IOFactory::createReader('Excel5');
		$excel = $this->reader->load(Yii::getAlias('@webroot/templates/').'tpl_Form4Grp.xls');

		$excel->setActiveSheetIndex(0);
		$sheet = $excel->getActiveSheet();

		$r=10;
		foreach ($rows_data['objects'] as $obj) {
			$sheet->setCellValue("A$r", $obj['name']);
			$sheet->setCellValue("B$r", $obj['data'][0]);

			foreach ($cells_data[$obj['id']] as $date => $works) {
				$col = explode('-', $date)[2] * 1 + 1;
				$this->makeCell($sheet, $col, $r, $works);
			}

			if ($obj['telemetry']) {
				$sheet->getStyle("A$r:AG$r")->applyFromArray($this->styles['with_telemetry']);
			}

			$r++;
		}

		// находим и красим выходные
		$hd = [];
		for ($i=1; $i<=31; $i++) {
			if ($this->checkHoliday($i, $schedule['period'])) {
				$hd[] = $i;
			}
		}
		//print_r($hd);
		foreach ($hd as $d) {
			$col = PHPExcel_Cell::stringFromColumnIndex($d+1);
			$sheet->getStyle($col.'9:'.$col.($r-1))->applyFromArray($this->styles['holidays']);
		}

		$r--;
		$sheet->getStyle("A8:AG$r")->applyFromArray($this->styles['bordered_thin']);
		$sheet->getStyle("A8:AG$r")->applyFromArray($this->styles['align_center']);;

		foreach(range(8,$r) as $rowID) {
			$sheet->getRowDimension($rowID)->setRowHeight(23);
		}


		//$this->writeFooter($sheet, $r+2);
		$r +=2;
		$sheet->setCellValue("B$r"," - осмотр технического состояния путём обхода;");
		$sheet->mergeCells("B$r:W$r");
		$this->makeCell($sheet, 0, $r, [1185]);
		$r++;

		$sheet->setCellValue("B$r"," - проверка параметров срабатывания предохранительных запорных и сбросных устройств;");
		$sheet->mergeCells("B$r:W$r");
		$this->makeCell($sheet, 0, $r, [1186]);
		$r++;

		$sheet->setCellValue("B$r"," - техническое обслуживание;");
		$sheet->mergeCells("B$r:W$r");
		$this->makeCell($sheet, 0, $r, [1187]);
		$r++;

		$sheet->setCellValue("B$r"," - текущий ремонт");
		$sheet->mergeCells("B$r:W$r");
		$this->makeCell($sheet, 0, $r, [1188]);

		$sheet->getStyle("A".($r-3).":A".$r)->applyFromArray($this->styles['align_right']);

		$r +=2;
		$sheet->setCellValue("A$r", 'Составил');

		$sheet->setCellValue("B$r", $names['author']['post']);
		$sheet->mergeCells("B$r:F$r");
		$sheet->getStyle("B$r")->applyFromArray($this->styles['border_bottom']);

		$sheet->mergeCells("H$r:N$r");
		$sheet->getStyle("H$r")->applyFromArray($this->styles['border_bottom']);

		$sheet->setCellValue("P$r", $names['author']['fio']);
		$sheet->mergeCells("P$r:W$r");
		$sheet->getStyle("P$r")->applyFromArray($this->styles['border_bottom']);

		$r++;
		$sheet->mergeCells("B$r:F$r");
		$sheet->setCellValue("B$r", 'должность');

		$sheet->mergeCells("H$r:N$r");
		$sheet->setCellValue("H$r", 'подпись');

		$sheet->mergeCells("P$r:W$r");
		$sheet->setCellValue("P$r", 'инициалы, фамилия');

		$sheet->getStyle("B".($r-1).":W$r")->applyFromArray($this->styles['align_center']);

		foreach(range('A','B') as $columnID) {
			$sheet->getColumnDimension($columnID)->setAutoSize(true);
		}

		$sheet->setCellValue("AA3", $names['approve']['post']);
		$sheet->setCellValue("AA4", '__________________________'.$names['approve']['fio']);

		$sheet->mergeCells('AA3:AG3');
		$sheet->mergeCells('AA4:AG4');
		$sheet->mergeCells('AA5:AG5');
		$sheet->getStyle("AA3:AA5")->applyFromArray($this->styles['align_right']);

		$excel->setActiveSheetIndex(1);
		$sheet = $excel->getActiveSheet();
		$m = ['-', 'январь', 'февраль','март','аперль','май','июнь','июль','август','сентябрь','октябрь','ноябрь','декабрь'];
		$sheet->setCellValue("A1", $m[intval(substr($schedule->period, 5, 2))]);
		$sheet->setCellValue("B1", substr($schedule->period, 0, 4));

		// дней в месяце
		$d = explode('-', $schedule->period);
		$month = $d[1]*1;
		$year = $d[0];
		$day_in_month = ($month == 2) ? ($year % 4 ? 28 : ($year % 100 ? 29 : ($year %400 ? 28 : 29))) : (($month - 1) % 7 % 2 ? 30 : 31);;
		// скрываем лишние столбцы
		$cc = ['AG', 'AF', 'AE'];
		for ($c=0; $c<(31-$day_in_month); $c++) {
			$sheet->getColumnDimension($cc[$c])->setVisible(false);
		}

		return $excel;
	}
	/*---------------------------------------------------------------*/
	private function print_5grp($cells_data, $rows_data, $schedule, $names) {

		/*
			 * ГРП-5 (данные со строки 14)
			 столбец A - Наименование УРГ
			 столбец B - адрес
			 столбец C - регулятор
			 столбец D - ПСК
			 столбец E - ПЗК
			 столбец F - выходное давление
			 столбец G-R - месяцы года

			 ГРП-5

			 */

		//$this->reader = PHPExcel_IOFactory::createReader('Excel5');
		$excel = $this->reader->load(Yii::getAlias('@webroot/templates/').'tpl_Form5Grp.xls');

		$excel->setActiveSheetIndex(0);
		$sheet = $excel->getActiveSheet();

		$sheet->setCellValue("A1", 'ГРП-5');

		$r = 14;
		foreach ($rows_data['objects'] as $obj) {
			$sheet->setCellValue("A$r", $obj['name']);
			$sheet->setCellValue("B$r", $obj['data'][0]);

			$sheet->setCellValue("C$r", implode("\n",$obj['equipment']['pc']));
			$sheet->setCellValue("D$r", implode("\n",$obj['equipment']['psk']));
			$sheet->setCellValue("E$r", implode("\n",$obj['equipment']['pzk']));
			$sheet->setCellValue("F$r", $obj['equipment']['pressures'][1] ? $obj['equipment']['pressures'][1] : $obj['equipment']['pressures'][0]);

			foreach ($cells_data[$obj['id']] as $date => $works) {
				$col = explode('-', $date)[1] * 1 + 5;
				$this->makeCell($sheet, $col, $r, $works);
			}

			if ($obj['telemetry']) {
				$sheet->getStyle("A$r:R$r")->applyFromArray($this->styles['with_telemetry']);
			}

			$r++;
		}


		$r--;
		$sheet->getStyle("A14:R$r")->applyFromArray($this->styles['bordered_thin']);
		$sheet->getStyle("A14:R$r")->applyFromArray($this->styles['align_center']);;

		foreach(range(14,$r) as $rowID) {
			$sheet->getRowDimension($rowID)->setRowHeight(23);
		}

		$r +=2;
		$sheet->setCellValue("B$r"," - осмотр технического состояния путём обхода;");
		$sheet->mergeCells("B$r:W$r");
		$this->makeCell($sheet, 0, $r, [1185]);

		$r++;
		$sheet->setCellValue("B$r"," - проверка параметров срабатывания предохранительных запорных и сбросных устройств;");
		$sheet->mergeCells("B$r:F$r");
		$this->makeCell($sheet, 0, $r, [1186]);

		$r++;
		$sheet->setCellValue("B$r"," - техническое обслуживание;");
		$sheet->mergeCells("B$r:F$r");
		$this->makeCell($sheet, 0, $r, [1187]);

		$r++;
		$sheet->setCellValue("B$r"," - текущий ремонт");
		$sheet->mergeCells("B$r:F$r");
		$this->makeCell($sheet, 0, $r, [1188]);

		$sheet->getStyle("A".($r-3).":A".$r)->applyFromArray($this->styles['align_right']);

		//$this->writeFooter($sheet, $r+2);
		$r +=3;
		$sheet->setCellValue("A$r", 'Составил');

		$sheet->setCellValue("B$r", $names['author']['post']);
		$sheet->getStyle("B$r")->applyFromArray($this->styles['border_bottom']);

		$sheet->getStyle("D$r")->applyFromArray($this->styles['border_bottom']);

		$sheet->setCellValue("F$r", $names['author']['fio']);
		$sheet->mergeCells("F$r:J$r");
		$sheet->getStyle("F$r")->applyFromArray($this->styles['border_bottom']);

		$r++;
		$sheet->setCellValue("B$r", 'должность');

		$sheet->setCellValue("D$r", 'подпись');

		$sheet->mergeCells("F$r:J$r");
		$sheet->setCellValue("F$r", 'инициалы, фамилия');

		$sheet->getStyle("B".($r-1).":J$r")->applyFromArray($this->styles['align_center']);

		foreach(range('A','C') as $columnID) {
			$sheet->getColumnDimension($columnID)->setAutoSize(true);
		}

		$sheet->setCellValue("O4", $names['approve']['post']);
		$sheet->setCellValue("O5", '__________________________'.$names['approve']['fio']);

		$sheet->mergeCells('O4:R4');
		$sheet->mergeCells('O5:R5');
		$sheet->getStyle("O3:P5")->applyFromArray($this->styles['align_right']);

		$excel->setActiveSheetIndex(1);
		$sheet = $excel->getActiveSheet();
		$sheet->setCellValue("A1", ' '.substr($schedule->period, 0, 4));

		return $excel;
	}

	/*---------------------------------------------------------------*/
	private function makeCell($sheet, $col, $r, $works) {
		$sheet->setCellValueByColumnAndRow($col, $r, 'X-'.count($works));
		//$sheet->Shapes->add
		$run = [];
		$objRichText = new PHPExcel_RichText();
		foreach ($works as $work) {
			$run[$work] = $objRichText->createTextRun(mb_convert_encoding('&#9698;', 'UTF-8', 'HTML-ENTITIES'));
			$run[$work]->getFont()->applyFromArray(array( "bold" => true, "size" => 16, "color" => array("rgb" => $this->works[$work]['color'])));
		}
		$sheet->setCellValueByColumnAndRow($col, $r, $objRichText);
	}
	/*---------------------------------------------------------------*/
	private function writeFooter($sheet, $r) {

	}
	/*---------------------------------------------------------------*/
	private function checkHoliday($date, $period) {
		$d = date('Y-m-'.str_pad($date,2,'0',STR_PAD_LEFT), strtotime($period));
		$dow = date('N', strtotime($d));
		return (!array_search($d, $this->workdays)) && ($dow==6 || $dow==7 || array_search($d, $this->holydays));
	}
}

