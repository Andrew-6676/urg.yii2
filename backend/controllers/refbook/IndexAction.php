<?php
namespace app\controllers\refbook;

use common\models\urg\SprTypeObj;
use Yii;
use yii\base\Action;

class indexAction extends \yii\rest\IndexAction
{
	public function run(){
		$res = parent::run();
		$res = $res->getModels();
		//print_r($res);

		//$res = SprTypeObj::find()->select()->asArray()->all();
		//$res = array_merge($res,$res2);
		return $res;
	}
}
