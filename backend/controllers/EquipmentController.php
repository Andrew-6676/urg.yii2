<?php

namespace backend\controllers;
use app\components\myActiveController;
use Yii;

class EquipmentController extends myActiveController
{

	public $modelClass = 'common\models\urg\UrgEquipment';
	public $with = ['files'];

	public function behaviors()	{
		return
			\yii\helpers\ArrayHelper::merge(parent::behaviors(), [
				'corsFilter' => [
					'class' => \yii\filters\Cors::className(),
				],
			]);
	}

	public function actions()
	{
		$actions = parent::actions();

		$actions['create'] = [
			'class' => 'app\controllers\equipment\SaveAction',
			'modelClass' => $this->modelClass,
		];
		$actions['update'] = [
			'class' => 'app\controllers\equipment\SaveAction',
			'modelClass' => $this->modelClass,
		];
		$actions['saveOrder'] = [
			'class' => 'app\controllers\equipment\SaveOrderAction',
			'modelClass' => $this->modelClass,
		];
		$actions['saveRegimCard'] = [
			'class' => 'app\controllers\equipment\SaveRegimCardAction',
			'modelClass' => $this->modelClass,
		];
		$actions['setSeal'] = [
			'class' => 'app\controllers\equipment\SetSealAction',
			'modelClass' => $this->modelClass,
		];
		$actions['list']   = [
			'class' => 'app\controllers\equipment\ListAction',
			'modelClass' => $this->modelClass,
			'prepareDataProvider' => [$this, 'prepareListProvider'],
		];
		$actions['listForRegimCard']   = [
			'class' => 'app\controllers\equipment\ListAction',
			'modelClass' => $this->modelClass,
			'prepareDataProvider' => [$this, 'prepareListRegimCardProvider'],
		];
//		$actions['listData']   = [
//			'class' => 'app\controllers\equipment\ListAction',
//			'modelClass' => $this->modelClass,
//			'dataOnly' => true,
//			'prepareDataProvider' => [$this, 'prepareListDataProvider'],
//		];

		// отключить действия "delete" и "create"
		//unset($actions['delete'], $actions['create']);

		return $actions;
	}

		// TODO: нужно переделать! при такой выборке делается от 200 запросов за раз
    public function prepareListProvider() {
    	$adp = parent::prepareDataProvider();
	    //$adp->query->with('');
	    if (Yii::$app->request->get('id_urg')) {
		    $adp->query
			    ->select('*, link_obj.id_obj1')
		        ->joinWith('linkObjAsChild')
			    ->where('id_obj1=:id_urg', ['id_urg'=>Yii::$app->request->get('id_urg')]);
	    } else {

	    }
	    $adp->pagination->defaultPageSize = 99999999;
	    return $adp;
	}

	public function prepareListRegimCardProvider() {
		$adp = parent::prepareDataProvider();
		if (Yii::$app->request->get('id_urg')) {
			$adp->query = $adp
				->query
				->select('*, link_obj.id_obj1')
				->joinWith('linkObjAsChild')
				->where('id_obj1=:id_urg', ['id_urg'=>Yii::$app->request->get('id_urg')])
				->andWhere(['in', 'id_type_obj2', [32,81,82]]);
		} else {

		}
		$adp->pagination->defaultPageSize = 99999999;
		return $adp;
	}

	public function prepareListDataProvider() {
		$this->with = [];
		$adp = $this->prepareListProvider();
		$adp->query->select('id_obj, id_eq_type');
		return $adp;
	}
}

