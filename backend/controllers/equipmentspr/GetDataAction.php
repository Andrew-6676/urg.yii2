<?php
namespace app\controllers\equipmentspr;

use common\components\Utils;
use common\models\urg\FilterSprMake;
use common\models\urg\FilterSprModel;
use common\models\urg\FilterSprType;
use common\models\urg\SprTypeObj;
use common\models\urg\Urg;  //где находится модель
use Yii;
use yii\base\Action;

class GetDataAction  extends Action
{
	public function run($id_eq_type) {


		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$request = Yii::$app->request;

		Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Credentials', 'true');
		Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Headers', $request->getHeaders()->get('Access-Control-Request-Headers'));
		Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Origin', $request->getHeaders()->get('origin'));


		if ($request->isOptions) {

			$options = ['GET', 'HEAD', 'OPTOINS'];
			Yii::$app->getResponse()->getHeaders()->set('Allow', implode(',', $options));
			Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Methods', implode(',', $options));
			$res = implode(',', $options);

			return $res;
		}

		if ($request->isGet) {
			$table = SprTypeObj::findOne($id_eq_type)->name_table;
			if (!$table) {
				return [];
			}

			$make = 'common\models\urg\\'.upperFirst($table).'SprMake';
			$model = 'common\models\urg\\'.upperFirst($table).'SprModel';
			$type = 'common\models\urg\\'.upperFirst($table).'SprType';


			$make  =  $make::find()->select('id_make as id, make as name')->orderBy('make')->indexBy('id')->asArray()->all();
			$model =  $model::find()->select('id_model as id, model as name, id_make')->orderBy('model')->indexBy('id')->asArray()->all();
			if ($table=='locks') {
				$type  =  $type::find()->select('id_type as id, title as name')->orderBy('type')->indexBy('id')->asArray()->all();
			} else {
				$type  =  $type::find()->select('id_type as id, type as name')->orderBy('type')->indexBy('id')->asArray()->all();
			}

			return [
				'make'=>$make,
				'model'=>$model,
				'type'=>$type
			];


		}

		return 'bad request';
    }
}

function upperFirst($str) {

	$first = mb_substr($str,0,1, 'UTF-8');//первая буква
	$last = mb_substr($str,1);//все кроме первой буквы
	$first = mb_strtoupper($first, 'UTF-8');
	$last = mb_strtolower($last, 'UTF-8');
	return $first.$last;

}