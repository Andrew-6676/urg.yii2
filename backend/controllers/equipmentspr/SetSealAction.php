<?php
namespace app\controllers\equipmentspr;

use common\components\Utils;
use common\models\urg\FilterSprMake;
use common\models\urg\FilterSprModel;
use common\models\urg\FilterSprType;
use common\models\urg\SprTypeObj;
use common\models\urg\Urg;  //где находится модель
use Yii;
use yii\base\Action;

class SetSealAction  extends Action
{
	public function run() {


		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$request = Yii::$app->request;

		Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Credentials', 'true');
		Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Headers', $request->getHeaders()->get('Access-Control-Request-Headers'));
		Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Origin', $request->getHeaders()->get('origin'));


		if ($request->isOptions) {

			$options = ['GET', 'HEAD', 'OPTOINS'];
			Yii::$app->getResponse()->getHeaders()->set('Allow', implode(',', $options));
			Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Methods', implode(',', $options));
			$res = implode(',', $options);

			return $res;
		}

		if ($request->isPost || $request->isPut) {


			return [
				'status'=>'ok',
				'message'=>'message'
			];


		}

		return 'bad request';
    }
}

function upperFirst($str) {

	$first = mb_substr($str,0,1, 'UTF-8');//первая буква
	$last = mb_substr($str,1);//все кроме первой буквы
	$first = mb_strtoupper($first, 'UTF-8');
	$last = mb_strtolower($last, 'UTF-8');
	return $first.$last;

}