<?php

namespace backend\controllers;
use app\components\myActiveController;

class StreetController extends myActiveController
{

	public $with = ['type'];
	public $modelClass = 'common\models\urg\SprStreet';

	public function behaviors()	{
		return
			\yii\helpers\ArrayHelper::merge(parent::behaviors(), [
				'corsFilter' => [
					'class' => \yii\filters\Cors::className(),
				],
			]);
	}
}

