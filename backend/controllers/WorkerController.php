<?php

namespace backend\controllers;
use app\components\myActiveController;

class WorkerController extends myActiveController
{

	public $modelClass = 'common\models\urg\Workers';

	public function behaviors()	{
		return
			\yii\helpers\ArrayHelper::merge(parent::behaviors(), [
				'corsFilter' => [
					'class' => \yii\filters\Cors::className(),
				],
			]);
	}

	public function actions()
	{
		$actions = parent::actions();

//		$actions['create'] = [
//			'class' => 'app\controllers\workorder\SaveAction',
//			'modelClass' => $this->modelClass,
//		];
//		$actions['update'] = [
//			'class' => 'app\controllers\workorder\SaveAction',
//			'modelClass' => $this->modelClass,
//		];


		return $actions;
	}


}

