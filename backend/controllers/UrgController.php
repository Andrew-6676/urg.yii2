<?php

namespace backend\controllers;

use Yii;
use app\components\myActiveController;
use common\models\urg\SprTypeObj;
use common\models\urg\UrgMaster;
use common\models\urg\UrgRefbook;
use yii\data\ActiveDataProvider;

class UrgController extends myActiveController
{

	public $modelClass = 'common\models\urg\Urg';
	//public $with = ['manufacturer','type','buildingType','address','heating','power_supply','owner', 'linkObjAsParent'];
	public $with = ['address.region', 'type', 'address.np', 'address.street', 'address.street.type'];
	/*-------------------------------------------------------------------------------------------*/
	public function behaviors()	{
		return
			\yii\helpers\ArrayHelper::merge(parent::behaviors(), [
				'corsFilter' => [
					'class' => \yii\filters\Cors::className(),
				],
			]);
	}
	/*-------------------------------------------------------------------------------------------*/
	public function actions()
	{
		$actions = parent::actions();
//		print_r($_COOKIE);
//		print_r($_SESSION);

		//$actions['statistics'] = 	['class' => 'app\controllers\urg\StatisticsAction'];
		$actions['test']           = ['class' => 'app\controllers\urg\TestAction'];
		$actions['addLastwork']    = ['class' => 'app\controllers\urg\AddLastworkAction'];
		$actions['getStat']        = ['class' => 'app\controllers\urg\GetStatAction'];
		$actions['clearAddress']   = ['class' => 'app\controllers\urg\ClearAddressAction'];
		$actions['setPeriodicity'] = ['class' => 'app\controllers\urg\SetPeriodicityAction'];

//		$actions['view']   = [
//			'class' => 'app\controllers\urg\ViewAction',
//			'modelClass' => $this->modelClass,
//		];
		$actions['create'] = [
			'class' => 'app\controllers\urg\SaveAction',
			'modelClass' => $this->modelClass,
		];
		$actions['update'] = [
			'class' => 'app\controllers\urg\SaveAction',
			'modelClass' => $this->modelClass,
		];
		$actions['list']   = [
			'class' => 'app\controllers\urg\ListAction',
			'modelClass' => $this->modelClass,
			'prepareDataProvider' => [$this, 'prepareListProvider'],
		];

        $actions['search'] = [
            'class' => 'yii\rest\IndexAction',
            'modelClass' => $this->modelClass,
            'prepareDataProvider' => [$this, 'prepareSearchProvider'],
        ];

		// отключить действия "delete" и "create"
		//unset($actions['delete'], $actions['create']);

		return $actions;
	}
	/*-------------------------------------------------------------------------------------------*/
	/*-------------------------------------------------------------------------------------------*/
    public function prepareListProvider() {
    	$adp = parent::prepareDataProvider();
	    if (Yii::$app->request->get('id_unit')) {
		    //$adp->query = $adp->query->joinWith('linkUnit')->andWhere('parent=:id_unit', ['id_unit' => $id_unit]);
		    $adp->query = $adp->query->where('id_unit=:id_unit', ['id_unit'=>Yii::$app->request->get('id_unit')]);
	    } else {

	    }
	    return $adp;
	}
	/*-------------------------------------------------------------------------------------------*/
    public function prepareSearchProvider() {
    	//print_r(Yii::$app->request->bodyParams['id_type_urg']);
		$filter = Yii::$app->request->bodyParams;

	    $modelClass = $this->modelClass;
	    $query = $modelClass::find()->with($this->with)->orderBy($_GET['order']);
	    $adp = new ActiveDataProvider([
		    // добавляем сортировку и жадную загрузку
		    // TODO: если WITH-поле не встречается в expand - то и не надо добавлять это в выборку
		    'query' => $query,
		    //'pagination' => false,
		    'pagination' => [
			    'defaultPageSize' => Yii::$app->params['defaultPageSize'],
			    'pageSizeLimit' => [0, 1500000000],
		    ],
	    ]);

        //$adp = parent::prepareDataProvider();
	    $adp->query = $adp->query
		                  ->joinWith([
						        'type' => function ($q) {
						            $q->from(['type' => UrgRefbook::tableName()]);
							    }
						    ])
						    ->joinWith([
							    'master' => function ($q) {
								    $q->from(['master' => UrgMaster::tableName()]);
							    }
						    ])
		                    ->joinWith('power_supply');

	    if ($filter['id_unit'] && $filter['id_unit']>0) {
		    $query->where(['id_unit' => $filter['id_unit']]);
	    }

	    if (is_array($filter['id_type_urg']) && count($filter['id_type_urg'])>0) {
		    $query->andWhere(['in', 'id_type_urg', $filter['id_type_urg']]);
	    }

	    if ($filter['master'] && trim($filter['master']) != '' && $filter['master'] != '- все -') {
		    // TODO: выбирать только по последнему мастеру (сейчас выбирает по любому)
		    $master = trim($filter['master']);
		    $query->/*joinWith('master')->*/andWhere(['master.name' => $master]);
	    }

	    if ($filter['num'] && trim($filter['num']) != '') {
	    	if (strpos($filter['num'], '_') !== false) {
	    		$like = ['or'];
			    foreach (explode(',', $filter['num']) as $n) {
			    	$like[] = ['like', 'num', trim($n), false];
			    }
			    $query->andWhere($like);
		    } else {
			    $nums = [];
			    foreach (explode(',', $filter['num']) as $n) {
			    	$nums[] = trim($n);
			    }
			    $query->andWhere(['in', 'num', $nums]);
		    }
	    }
	    // фильтр по оборудованию
	    if (is_array($filter['equipment']['data']) && count($filter['equipment']['data'])>0) {
	    	$eq_filter = [$filter['equipment']['operator']];
	    	$first = true;
			foreach ($filter['equipment']['data'] as $eq) {
				//print_r($eq);
				// определяем имена таблиц по типу оборудования
				$table = SprTypeObj::findOne($eq['type'])->name_table;
				if (!$table) {
					return 'error';
				}

				//exists (select * from link_obj lo inner join `filter` f on f.id_obj=lo.id_obj2 where lo.id_obj1=urg.id_obj and id_model=1192)
				$subquery = (new \yii\db\Query())
					//->select('COUNT(*)')
					->from('link_obj')
					->leftJoin($table, $table.'.id_obj = link_obj.id_obj2')
					->where(['id_model'=>$eq['model']])
					->andWhere('link_obj.id_obj1 = urg.id_obj');
				//echo $table;
				if ($first) {
					$first = false;
				}

				$eq_filter[] = ['exists', $subquery];
				// TODO: предусмотреть И/ИЛИ
			}
		    $query->andWhere($eq_filter);
	    }

        return $adp;
    }
	/*-------------------------------------------------------------------------------------------*/
	/*-------------------------------------------------------------------------------------------*/
}

