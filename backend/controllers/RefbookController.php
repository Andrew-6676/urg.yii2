<?php
namespace backend\controllers;

use app\components\myActiveController;
use common\models\urg\SprTypeObj;
use Yii;
use yii\db\Query;


/**
 * Site controller
 */
class RefbookController extends myActiveController
{
	public $modelClass = 'common\models\urg\UrgRefbook';

	public function behaviors()	{
		return
			\yii\helpers\ArrayHelper::merge(parent::behaviors(), [
				'corsFilter' => [
					'class' => \yii\filters\Cors::className(),
				],
			]);
	}

//	public function actions()
//	{
//		$actions = parent::actions();
//
//		$actions['index']   = [
//			'class' => 'app\controllers\refbook\IndexAction',
//			'modelClass' => $this->modelClass,
//			//'prepareDataProvider' => [$this, 'prepareListProvider'],
//		];
////		$actions['index']['prepareDataProvider'] = [$this, 'prepareListProvider'];
//
//
//		// отключить действия "delete" и "create"
//		//unset($actions['delete'], $actions['create']);
//
//		return $actions;
//	}

//	public function prepareListProvider() {
//		$adp = parent::prepareDataProvider();
//		$query = $adp->query;
//
//		$query1 = new Query();
//		$query1->select(['id'=>'id_type_obj',
//					    "id_parent"=>'(0)',
//					    "name" => 'type_obj',
//					    "descr" => '(null)',
//					    "deleted" => '(false)',
//					    "id_group"=>'(null)',
//					    "dependence"=>'(NULL)'
//			])
//			->from(SprTypeObj::tableName());
//		$query1->union($query);
//
//		$adp->query = $query1;
//		//$adp->query = $adp->query->select('*, link_obj.id_obj1');
//		//$adp->query = $adp->query->joinWith('linkObjAsChild');
//		//$adp->query = $adp->query->where('id_obj1=:id_urg', ['id_urg'=>Yii::$app->request->get('id_urg')]);
//
//		$adp->pagination->defaultPageSize = 99999999;
//		return $adp;
//	}
}
