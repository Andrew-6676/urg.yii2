<?php
namespace app\controllers\instrumentation;

use common\models\urg\Filter;
use common\models\urg\Urg;
use common\models\urg\UrgEquipment;
use Yii;
use yii\helpers\ArrayHelper;
use yii\rest\Action;
use yii\data\ActiveDataProvider;

class listAction extends Action {

	public $prepareDataProvider;
	public $dataOnly=false;

	public function run() {

		$request = Yii::$app->request;
		if ($request->isOptions) {

			$options = ['GET', 'HEAD', 'OPTOINS'];
			Yii::$app->getResponse()->getHeaders()->set('Allow', implode(',', $options));
			//Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
			Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Methods', implode(',', $options));

			$res = implode(',', $options);

			return $res;
		}


		if ($request->isGet) {

			if ($this->checkAccess) {
				call_user_func($this->checkAccess, $this->id);
			}
				// получаем список оборудования для текущего УРГ
			$res =  $this->prepareDataProvider();
			if ($this->dataOnly) {
				$res =  $res->getModels();
			}

			return $res;
		}

		return 'Wrong request';
	}

	/* ------------------------------------------------------------------------------------ */

	protected function prepareDataProvider() {
		if ($this->prepareDataProvider !== null) {
			return call_user_func($this->prepareDataProvider);
		}

		/* @var $modelClass \yii\db\BaseActiveRecord */
		$modelClass = $this->modelClass;

		return new ActiveDataProvider([
			'query' => $modelClass::find(),
		]);
	}
}
