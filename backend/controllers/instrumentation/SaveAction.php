<?php
namespace app\controllers\instrumentation;

use common\models\urg\Instrumentation;
use common\models\urg\LinkObj;
use common\models\urg\Obj;
use common\models\urg\SprTypeObj;
use common\models\urg\UrgEquipment;
use Yii;
use yii\base\Model;
use yii\rest\Action;
use yii\data\ActiveDataProvider;

class saveAction extends Action {

	public $prepareDataProvider;
	public $scenario = Model::SCENARIO_DEFAULT;

	public function run() {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$request = Yii::$app->request;
		Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Credentials', 'true');
		Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Headers', $request->getHeaders()->get('Access-Control-Request-Headers'));
		Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Origin', $request->getHeaders()->get('Origin'));

		if ($request->isOptions) {

			$options = ['POST', 'PUT', 'OPTOINS'];
			Yii::$app->getResponse()->getHeaders()->set('Allow', implode(',', $options));
			//Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
			Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Methods', implode(',', $options));

			$res = implode(',', $options);

			return $res;
		}


		if ($request->isPost || $request->isPut) {
			$res = [];
				// добавляем КИП в УРГ или обновляем уже сохранённый КИП
			$params = $request->bodyParams;

				// заполняем модели данными
			$id_obj = $params['id'];
			if (!$params['id'] || $params['id']<0) {
					// добавление КИП - нужно создать связь с УРГ
				$id_obj = Obj::addInstrumentation();
				if ($id_obj) {
					if (!LinkObj::add($params['id_urg'], $id_obj)) {
						$res['status'] = 'error';
						$res['message'] = 'Во время сохранения связи с КИП возникла ошибка.';
						Obj::del($id_obj);
						return $res;
					}
				} else {
					$res['status'] = 'error';
					$res['message'] = 'Возникла ошибка при создании объекта КИП.';
					return $res;
				}

				$urg_instr = new Instrumentation();
				$urg_instr->id_obj = $id_obj;

				$res['id'] = (string)$id_obj;
			} else {
					// изменение оборудования
				$urg_instr = Instrumentation::findOne($params['id']);

				if (!$urg_instr) {
					$res['status'] = 'error';
					$res['message'] = 'Редактируемый объект отсутствует в БД.';
					return $res;
				}
			}

			$urg_instr->load($params, '');

				// сохранение
				// TODO: отловить ошибку при непраильном запросе (внешний ключ, например, несуществующий)
			$del = false;
			if ($urg_instr->save()) {
					$res['status'] = 'ok';
					$res['message'] = 'КИП сохранено успешно';
					$res['id'] = (string)$urg_instr->id;

			} else {
				$res['id'] = (string)$urg_instr->id;
				$res['status'] = 'error';
				$res['message'] = 'Во время сохранения urg_instrumentation возникла ошибка.';
				$res['errors'] = $urg_instr->errors;
				$del = true;
			}


				// ошибка добавления нового оборудования
			if ($del && $params['id']<0) {
				Obj::del($id_obj);
			}
			//}
			//if (LinkObj::add($urg, $id_obj)) {}
			//return UrgEquipment::add(Yii::$app->request->bodyParams['id_urg'], Yii::$app->request->bodyParams);

			return $res;
//			return [
//				'id' => (string)$id_obj,
//				'status' => $res['status'],
//				'message' => $res['message'],
//				'errors' => $res['errors']
//			];
		}

		//Yii::$app->getResponse()->setStatusCode(405);
		return 'wrong request';
	}

	/* ------------------------------------------------------------------------------------ */

	protected function prepareDataProvider() {
		if ($this->prepareDataProvider !== null) {
			return call_user_func($this->prepareDataProvider);
		}

		/* @var $modelClass \yii\db\BaseActiveRecord */
		$modelClass = $this->modelClass;

		return new ActiveDataProvider([
			'query' => $modelClass::find(),
		]);
	}
}


function upperFirst($str) {

	$first = mb_substr($str,0,1, 'UTF-8');//первая буква
	$last = mb_substr($str,1);//все кроме первой буквы
	$first = mb_strtoupper($first, 'UTF-8');
	$last = mb_strtolower($last, 'UTF-8');
	return $first.$last;

}