<?php

namespace backend\controllers;
use app\models\SprNp;
use yii\helpers\ArrayHelper;
//use yii\rest\ActiveController;
use app\components\myActiveController;
use yii\data\ActiveDataProvider;

class UrgmasterController extends myActiveController
{

	public $modelClass = 'common\models\urg\UrgMaster';

	public function behaviors()	{
		return
			\yii\helpers\ArrayHelper::merge(parent::behaviors(), [
				'corsFilter' => [
					'class' => \yii\filters\Cors::className(),
				],
			]);
	}

	public function actionTest() {
		return '3';
	}

	public function actions()
	{
		$actions = parent::actions();
		$actions['search'] = [
			'class' => 'yii\rest\IndexAction',
			'modelClass' => $this->modelClass,
			'prepareDataProvider' => [$this, 'prepareSearchProvider'],
		];
		return $actions;
	}

//select distinct name
//from urg_master m
//inner join urg u on u.id_obj=m.id_urg
//where id_unit=1

    public function prepareSearchProvider() {
        $adp = parent::prepareDataProvider();
        $fields = 'name, post';
        if (isset($_GET['fields'])) $fields = $_GET['fields'];

        $adp->query->select($fields)->distinct();
        if (\Yii::$app->request->get('id_unit')) {
            $id_unit = \Yii::$app->request->get('id_unit');
            $adp->query = $adp->query->joinWith('urg')->andWhere(['id_unit' => $id_unit]);
        }
        return $adp;
    }
}

