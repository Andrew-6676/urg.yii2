<?php
namespace backend\controllers;

use app\components\myActiveController;
use common\models\urg\SprTypeObj;
use Yii;
use yii\db\Query;


/**
 * Site controller
 */
class InstrumentationController extends myActiveController
{
	public $modelClass = 'common\models\urg\Instrumentation';

	public function behaviors()	{
		return
			\yii\helpers\ArrayHelper::merge(parent::behaviors(), [
				'corsFilter' => [
					'class' => \yii\filters\Cors::className(),
				],
			]);
	}

	public function actions()
	{
		$actions = parent::actions();

		$actions['create'] = [
			'class' => 'app\controllers\instrumentation\SaveAction',
			'modelClass' => $this->modelClass,
		];
//		$actions['update'] = [
//			'class' => 'app\controllers\equipment\SaveAction',
//			'modelClass' => $this->modelClass,
//		];
		$actions['list']   = [
			'class' => 'app\controllers\instrumentation\ListAction',
			'modelClass' => $this->modelClass,
			'prepareDataProvider' => [$this, 'prepareListProvider'],
		];
//		$actions['listData']   = [
//			'class' => 'app\controllers\equipment\ListAction',
//			'modelClass' => $this->modelClass,
//			'dataOnly' => true,
//			'prepareDataProvider' => [$this, 'prepareListDataProvider'],
//		];

		// отключить действия "delete" и "create"
		//unset($actions['delete'], $actions['create']);

		return $actions;
	}

	public function prepareListProvider() {
		$adp = parent::prepareDataProvider();
		if (Yii::$app->request->get('id_urg')) {
			$adp->query = $adp->query->select('*, link_obj.id_obj1');
			$adp->query = $adp->query->joinWith('linkObjAsChild');
			$adp->query = $adp->query->where('id_obj1=:id_urg', ['id_urg'=>Yii::$app->request->get('id_urg')]);
		} else {

		}
		$adp->pagination->defaultPageSize = 99999999;
		return $adp;
	}

//	public function prepareListDataProvider() {
//		$this->with = [];
//		$adp = $this->prepareListProvider();
//		$adp->query->select('id_obj, id_eq_type');
//		return $adp;
//	}
}
