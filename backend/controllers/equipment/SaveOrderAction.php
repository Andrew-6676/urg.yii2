<?php
namespace app\controllers\equipment;

use common\models\urg\LinkObj;
use common\models\urg\Obj;
use common\models\urg\SprTypeObj;
use common\models\urg\UrgEquipment;
use Yii;
use yii\base\Model;
use yii\rest\Action;
use yii\data\ActiveDataProvider;

class saveOrderAction extends Action {

	public $scenario = Model::SCENARIO_DEFAULT;

	public function run() {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$request = Yii::$app->request;
		Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Credentials', 'true');
		Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Headers', $request->getHeaders()->get('Access-Control-Request-Headers'));
		Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Origin', $request->getHeaders()->get('Origin'));

		if ($request->isOptions) {

			$options = ['POST', 'PUT', 'OPTOINS'];
			Yii::$app->getResponse()->getHeaders()->set('Allow', implode(',', $options));
			//Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
			Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Methods', implode(',', $options));

			$res = implode(',', $options);

			return $res;
		}


		if ($request->isPost || $request->isPut) {
			$res = [];
			$params = $request->bodyParams;

			$r = 0;
			foreach ($params as $param) {
				$r += UrgEquipment::updateAll(['order'=>$param['order'], 'mount_point'=>$param['mount_point']], ['id_obj'=>$param['id']]);
			}
			//$res['status'] = ($r == count($params)) ? 'ok' : 'error';
			$res['status'] = 'ok';
			$res['message'] = 'Очерёдность оборудования обновлена';
			$res['r'] = $r;
			return $res;
		}

		//Yii::$app->getResponse()->setStatusCode(405);
		return 'wrong request';
	}
}

