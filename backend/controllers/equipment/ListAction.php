<?php
namespace app\controllers\equipment;

use common\models\urg\Filter;
use common\models\urg\Urg;
use common\models\urg\UrgEquipment;
use Yii;
use yii\helpers\ArrayHelper;
use yii\rest\Action;
use yii\data\ActiveDataProvider;

class listAction extends Action {

	public $prepareDataProvider;
	public $dataOnly=false;

	public function run() {

		$request = Yii::$app->request;
		if ($request->isOptions) {

			$options = ['GET', 'HEAD', 'OPTOINS'];
			Yii::$app->getResponse()->getHeaders()->set('Allow', implode(',', $options));
			//Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
			Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Methods', implode(',', $options));

			$res = implode(',', $options);

			return $res;
		}


		if ($request->isGet) {

			if ($this->checkAccess) {
				call_user_func($this->checkAccess, $this->id);
			}
				// получаем список оборудования для текущего УРГ
			$res =  $this->prepareDataProvider();
			if ($this->dataOnly) {
				$res =  $res->getModels();
			}


			if ($this->dataOnly) {
				// формируем список id_obj оборудования, сгруппированное по типу
				$res2 = [];
				foreach ($res as $eq) {
					$res2[$eq['id_eq_type']][] = $eq['id'];
				}

				// выбираем данные по каждому оборудованию из соответствующей таблицы
				$res3 = [];
				$res4 = [];
				foreach ($res2 as $id_type_obj => $ids) {
					$table = UrgEquipment::$spr_type_obj[$id_type_obj]['name_table'];
					$fc = mb_strtoupper(mb_substr($table, 0, 1));
					$class_name = 'common\models\urg\\' . $fc . mb_substr($table, 1);
					$res3[] = $table;
					$w = [];
					if (method_exists($class_name, 'getType')) {
						//print_r([$ex, $class_name, 'getType']);
						$w[] = 'type';
					}
					if (method_exists($class_name, 'getManufacturer')) {
						//print_r([$ex, $class_name, 'getType']);
						$w[] = 'manufacturer';
					}
					if (method_exists($class_name, 'getModel')) {
						//print_r([$ex, $class_name, 'getType']);
						$w[] = 'model';
					}
					if (method_exists($class_name, 'getModel2')) {
						//print_r([$ex, $class_name, 'getType']);
						$w[] = 'model2';
					}
					$data = $class_name::find()->with($w)->where(['in', 'id_obj', $ids])->all();

					$res4 = ArrayHelper::merge($res4, ArrayHelper::index($data, 'id_obj'));
				}
			}
//
//				// соединяем всё в кучу - формируем массив для клиента
//			$res5 = [];
//			foreach ($res as $eq) {
//				$res5[$eq->id] = ArrayHelper::toArray($eq);
//				$res5[$eq->id]['data'] = $res4[$eq->id];
//			}
//
//			return $res5;
			if ($this->dataOnly)
				$res = $res4;
			return $res;
		}

		return 'Wrong request';
	}

	/* ------------------------------------------------------------------------------------ */

	protected function prepareDataProvider() {
		if ($this->prepareDataProvider !== null) {
			return call_user_func($this->prepareDataProvider);
		}

		/* @var $modelClass \yii\db\BaseActiveRecord */
		$modelClass = $this->modelClass;

		return new ActiveDataProvider([
			'query' => $modelClass::find(),
		]);
	}
}
