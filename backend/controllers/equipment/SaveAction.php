<?php
namespace app\controllers\equipment;

use common\models\urg\LinkObj;
use common\models\urg\Obj;
use common\models\urg\SprTypeObj;
use common\models\urg\UrgEquipment;
use Yii;
use yii\base\Model;
use yii\rest\Action;
use yii\data\ActiveDataProvider;

class saveAction extends Action {

	public $prepareDataProvider;
	public $scenario = Model::SCENARIO_DEFAULT;

	public function run() {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$request = Yii::$app->request;
		Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Credentials', 'true');
		Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Headers', $request->getHeaders()->get('Access-Control-Request-Headers'));
		Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Origin', $request->getHeaders()->get('Origin'));

		if ($request->isOptions) {

			$options = ['POST', 'PUT', 'OPTOINS'];
			Yii::$app->getResponse()->getHeaders()->set('Allow', implode(',', $options));
			//Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
			Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Methods', implode(',', $options));

			$res = implode(',', $options);

			return $res;
		}


		if ($request->isPost || $request->isPut) {
			$res = [];
				// добавляем оборудование в УРГ или обновляем уже сохранённое оборудование
			$params = $request->bodyParams;
				// разделяем данные - часть пойдёт в urg_equipment, а часть в соответствующее оборудование: locks, pc, boiler, filter, psk, pzk, meter
				// определяем куда сохранять оборудование
			$table = SprTypeObj::findOne($params['id_eq_type'])->name_table;
			if (!$table) {
				return [
					'status' => 'err',
					'message' => 'Ошибка сохранения',
					'errors' => 'Невозможно определить оборудование.',
					'table' => $table
				];
			}
			$params['eq_table'] = $table;
			$equipment_class = 'common\models\urg\\'.upperFirst($table);


				// заполняем модели данными
			$id_obj = $params['id'];
			if (!$params['id'] || $params['id']<0) {
					// добавление оборудования - нужно создать связь с УРГ
				$id_obj = Obj::createObjByIdType($params['id_eq_type']);
				if ($id_obj) {
					if (!LinkObj::add($params['id_urg'], $id_obj)) {
						$res['status'] = 'error';
						$res['message'] = 'Во время сохранения связи с оборудованием возникла ошибка.';
						Obj::del($id_obj);
						return $res;
					}
				} else {
					$res['status'] = 'error';
					$res['message'] = 'Возникла ошибка при создании объекта.';
					return $res;
				}

				$urg_eq = new UrgEquipment();
				$urg_eq->id_obj = $id_obj;

				$equipment = new $equipment_class();
				$equipment->id_obj = $id_obj;

				$res['id'] = (string)$id_obj;
			} else {
					// изменение оборудования
				$urg_eq = UrgEquipment::findOne($params['id']);
				$equipment = $equipment_class::findOne($params['id']);
				if (!$urg_eq || !$equipment) {
					$res['status'] = 'error';
					$res['message'] = 'Редактируемый объект отсутствует в БД.';
					return $res;
				}
			}

			$urg_eq->load($params, '');
			$equipment->load($params['data'], '');
			if ($table=='locks') {
				$equipment->model='-';
				$equipment->date_install = substr($params['data']['date_starting'], 0, 10);
				$equipment->year_production = $params['data']['year_release'];
				$equipment->number = $params['data']['serial_number'];
				$equipment->placement = $params['mount_point'];
			}

				// сохранение
				// TODO: отловить ошибку при непраильном запросе (внешний ключ, например, несуществующий)
			$del = false;
			if ($urg_eq->save()) {
				if ($equipment->save()) {
					$res['status'] = 'ok';
					$res['message'] = 'Оборудование сохранено успешно';
					$res['id'] = (string)$equipment->id;
				} else {
					$res['status'] = 'error';
					$res['message'] = 'Во время сохранения '.$table.' возникла ошибка.';
					$res['errors'] = $equipment->errors;
					$del = true;
				}
			} else {
				$res['id'] = (string)$urg_eq->id;
				$res['status'] = 'error';
				$res['message'] = 'Во время сохранения urg_equipment возникла ошибка.';
				$res['errors'] = $urg_eq->errors;
				$del = true;
			}


				// ошибка добавления нового оборудования
			if ($del && $params['id']<0) {
				Obj::del($id_obj);
			}
			//}
			//if (LinkObj::add($urg, $id_obj)) {}
			//return UrgEquipment::add(Yii::$app->request->bodyParams['id_urg'], Yii::$app->request->bodyParams);

			return [
				'save_res' => $res,
				'id' => (string)$id_obj,
				'id_urg' => $params['id_urg'],
				'status' => $res['status'],
				'message' => $res['message'],
				'errors' => $res['errors'],
				'table' => $equipment_class,
				'action' => $params['id']<0 ? 'add' : 'update',
				'urg_eq' => $urg_eq,
				'equipment' => $equipment,
				//'request' => $request->bodyParams,
			];
		}

		//Yii::$app->getResponse()->setStatusCode(405);
		return 'wrong request';
	}

	/* ------------------------------------------------------------------------------------ */

	protected function prepareDataProvider() {
		if ($this->prepareDataProvider !== null) {
			return call_user_func($this->prepareDataProvider);
		}

		/* @var $modelClass \yii\db\BaseActiveRecord */
		$modelClass = $this->modelClass;

		return new ActiveDataProvider([
			'query' => $modelClass::find(),
		]);
	}
}


function upperFirst($str) {

	$first = mb_substr($str,0,1, 'UTF-8');//первая буква
	$last = mb_substr($str,1);//все кроме первой буквы
	$first = mb_strtoupper($first, 'UTF-8');
	$last = mb_strtolower($last, 'UTF-8');
	return $first.$last;

}