<?php
namespace app\controllers\equipment;

use common\models\urg\LinkObj;
use common\models\urg\Obj;
use common\models\urg\SprTypeObj;
use common\models\urg\UrgEquipment;
use Yii;
use yii\base\Model;
use yii\rest\Action;
use yii\data\ActiveDataProvider;

class saveRegimCardAction extends Action {

	public $scenario = Model::SCENARIO_DEFAULT;

	public function run() {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$request = Yii::$app->request;
		Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Credentials', 'true');
		Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Headers', $request->getHeaders()->get('Access-Control-Request-Headers'));
		Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Origin', $request->getHeaders()->get('Origin'));

		if ($request->isOptions) {

			$options = ['POST', 'PUT', 'OPTOINS'];
			Yii::$app->getResponse()->getHeaders()->set('Allow', implode(',', $options));
			//Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
			Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Methods', implode(',', $options));

			$res = implode(',', $options);

			return $res;
		}


		if ($request->isPost || $request->isPut) {
			$res = [];
			$params = $request->bodyParams;

			$r = 0;
			$ok = true;
			foreach ($params as $param) {
			//	$r += UrgEquipment::updateAll(['order'=>$param['order'], 'mount_point'=>$param['mount_point']], ['id_obj'=>$param['id']]);
				$res['data'][] = $param;
				$table = UrgEquipment::$spr_type_obj[$param['id_eq_type']]['name_table'];
				if ($table) {
					$fc = mb_strtoupper(mb_substr($table, 0, 1));
					$class_name = 'common\models\urg\\' . $fc . mb_substr($table, 1);
					$eq = $class_name::findOne($param['id_obj']*1);
					$eq->load($param, '');
					if ($eq->save()) {

					} else {
						$ok = false;
					}
				} else {
					//$res['status'] = 'error'
				}

			}
			//$res['status'] = ($r == count($params)) ? 'ok' : 'error';
			$res['status'] = $ok ? 'ok' : 'error';
			$res['message'] = $ok ? 'Режимная карта сохранена' : 'Возникли проблемы при сохраеннии режимной карты';
			$res['r'] = $r;
			return $res;
		}

		//Yii::$app->getResponse()->setStatusCode(405);
		return 'wrong request';
	}


}

