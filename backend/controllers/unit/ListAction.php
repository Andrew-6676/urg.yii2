<?php
namespace app\controllers\unit;

use common\models\urg\Urg;
use Yii;
use yii\base\Action;
use yii\helpers\ArrayHelper;


class listAction extends Action {

	public $prepareDataProvider;

	public function run($id=null) {

		$request = Yii::$app->request;
		if ($request->isOptions) {

			$options = ['GET', 'HEAD', 'OPTOINS'];
			Yii::$app->getResponse()->getHeaders()->set('Allow', implode(',', $options));
			//Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
			Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Methods', implode(',', $options));

			$res = implode(',', $options);

			return $res;
		}


		if ($request->isGet) {

			$content = "";
			$url_id = '';
			if ($id) {
				$url_id = '?id='.$id;
			}

			//echo Yii::$app->params['company_API'] .$url_id;
			//exit;

//			if (!file_exists('../cache')) {
//				mkdir('../cache',0777,true);
//			}
//			if (file_exists('../cache/companies_'.$id)) {
//				$fp = fopen('../cache/companies_'.$id, 'r');
//			} else {
//				// получаем дерево подразделений от сервиса
//				$fp = fopen(Yii::$app->params['company_API'] . $url_id , 'r');
//			}
//
//			while (!feof($fp)) {
//				$content .= fread($fp, 1024);
//			}
//			fclose($fp);
//			file_put_contents('../cache/companies_'.$id, $content);


			if (file_exists('../cache/companies_'.$id)) {
				// получаем дерево подразделений из кэша
				$content = file_get_contents('../cache/companies_'.$id);
			} else {
				if (!file_exists('../cache')) {
					mkdir('../cache',0777,true);
				}
				// получаем дерево подразделений от сервиса
				$content = file_get_contents(Yii::$app->params['company_API'].$url_id);
				file_put_contents('../cache/companies_'.$id, $content);
			}


			// преобразуем дерево подразделений в массив
			//$companies = (\yii\helpers\Json::decode($content));
			$companies = json_decode($content, true, 512, JSON_BIGINT_AS_STRING);
				// счиатем кол-во объектов по подразделениям
			$counter = Urg::find()->select('id_unit, name, count(*) as count')->joinWith('type')->groupBy(['id_unit','name'])->asArray()->all();
				// индексируем массив
			$counter = ArrayHelper::map($counter, 'name', 'count', 'id_unit');

			//print_r($companies);
			//return;
			//$count = Urg::find()->select('count(*) as count, id_unit')->groupBy('id_unit')->indexBy('id_unit')->column();
			//$companies['count'] = $count;

				// проходиммся рекурсией по подразделениям и вставляем каждому кол-во объектов
			$this->scanChilds($companies, $counter);
			//print_r($companies);
			return $companies;
		}

		return 'Wrong request';
	}

	/* ------------------------------------------------------------------------------------ */

	private function scanChilds(&$companies, $counter) {
		if (array_key_exists(0, $companies)) {
				// если есть дети - проходимся по всем
			foreach ($companies as &$company) {
				//echo $company['name'] . "-\n";
				$c = [];
				$total = 0;
					// формируем строку с кол-вом объектов по видам
				foreach ($counter[$company['id_gasb']] as $key=>$count) {
					$c[] = $key.': '.$count;
					$total += $count;
				}
				if ($total>0) array_unshift($c, 'всего объектов: '.$total.'');
				//$company['count'] = implode('&nbsp;&nbsp;&nbsp;&nbsp;  ', $c);
				$company['count'] = $c;
				//$company['id'] = (string)$company['id'];
				$company['id_gasb'] = (string)$company['id_gasb'];
				if ($company['id_gasb'])
					$company['id'] = (string)$company['id_gasb'];
				//$company['id'] = (string)$company['id_gasb'];
				//print_r($c);
				if (count($company['childs'])) {
					$this->scanChilds($company['childs'], $counter);
				}
				//$child['count'] = 0;
			}
		} else {
				// если корневой элемент
			$c = [];
				// формируем строку с кол-вом объектов по видам
			foreach ($counter[$companies['id_gasb']] as $key=>$item) {
				$c[] = $key.': '.$item;
			}
			$companies['count'] = implode(';&nbsp;&nbsp;&nbsp;&nbsp;  ', $c);
			//$companies['id'] = (string)$companies['id'];
			$companies['id_gasb'] = (string)$companies['id_gasb'];
			if ($companies['id_gasb'])
				$companies['id'] = (string)$companies['id_gasb'];
			if (count($companies['childs'])) {
				$this->scanChilds($companies['childs'], $counter);
			}
		}
	}
}
