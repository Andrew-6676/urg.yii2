<?php
namespace backend\controllers;

use app\components\myActiveController;
use common\models\urg\SprTypeObj;
use Yii;
use yii\db\Query;


/**
 * Site controller
 */
class HelpController extends myActiveController
{
	public $modelClass = 'common\models\urg\Help';

	public function behaviors()	{
		return
			\yii\helpers\ArrayHelper::merge(parent::behaviors(), [
				'corsFilter' => [
					'class' => \yii\filters\Cors::className(),
				],
			]);
	}
}
