<?php
namespace backend\controllers;

use app\components\myActiveController;

/**
 * Site controller
 */
class InstrumentationsprController extends myActiveController
{
	public $modelClass = 'common\models\urg\UrgInstrumentationSpr';

	public function behaviors()	{
		return
			\yii\helpers\ArrayHelper::merge(parent::behaviors(), [
				'corsFilter' => [
					'class' => \yii\filters\Cors::className(),
				],
			]);
	}
}
