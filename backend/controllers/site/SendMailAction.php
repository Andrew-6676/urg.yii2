<?php
namespace app\controllers\site;

use Yii;
use yii\base\Action;

class SendMailAction extends Action
{
	public function run(){

		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$res = ['status'=>'ok'];
		$request = Yii::$app->request;
		Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Credentials', 'true');
		Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Headers', Yii::$app->request->getHeaders()->get('Access-Control-Request-Headers'));
		Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Origin', Yii::$app->request->getHeaders()->get('origin'));
//
		if ($request->isOptions) {
			\Yii::$app->response->format = \yii\web\Response::FORMAT_HTML;
			$options = ['HEAD', 'POST'];
			Yii::$app->getResponse()->getHeaders()->set('Allow', implode(',', $options));
			//Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
			Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Methods', implode(',', $options));

			$res = implode(',', $options);
			return $res;
		} else {
			$res = ['status'=>'ok'];

			$filial_short_name = $request->bodyParams['unit'];
			$body = '';

			$body .= 'Кто:   '.$request->bodyParams['user'].', '.$request->bodyParams['unit'].''."\n";
			$body .= 'Когда: '.date('d.m.Y H:i:s').''."\n\n";
			$body .= 'Текст: '.''."\n\n";
			$body .= $request->bodyParams['text'];

			Yii::$app->mailer->compose()
				//->setTo(Yii::$app->params['annul_mail'])
				->setTo(Yii::$app->params['supportEmail'])
				->setFrom(['no-reply@urg.oblgaz' => 'urg']) // [ '.utf8_decode($request->bodyParams['unit']).' ]'])
				->setSubject('urg: '.$request->bodyParams['subject'])
				->setTextBody($body)
				->send();
		}


		return $res;
	}

}
