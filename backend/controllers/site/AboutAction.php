<?php
namespace app\controllers\site;

use Yii;
use yii\base\Action;

class aboutAction extends Action
{
	public function run(){


		$var = 'var';
		return $this->controller->render('about', array(
			'var' => $var,
		));
	}
}
