<?php
namespace app\controllers\site;

use Yii;
use yii\base\Action;

class thumbAction extends Action
{
	public function run($path) {



		//print_r($path);
		Yii::$app->getResponse()->getHeaders()->set('Content-Type','image/jpeg');
		if (file_exists($path)) {
			if (!$this->imageresize(null,$path,320,75)) {
				echo $path, " error resize";
			}
		} else {
			echo $path, " not found";
		}
	}

	private function open_image ($file) {
		// Получаем информацию об расширении файла
		$im = false;
		$res = ['info' => getimagesize($file)];
		$type = explode('/',$res['info']['mime'])[1];

		switch($type) {
			case 'jpg':
			case 'jpeg':
				$im = imagecreatefromjpeg($file);
				break;
			case 'png':
				$im = imagecreatefrompng($file);
				break;
			case 'gif':
				$im = imagecreatefromgif($file);
				break;
			case 'bmp':
				$im = imagecreatefromwbmp($file);
				break;
			case 'x-ms-bmp':
				//$im = imagecreatefromwbmp($file);
				//$im = imagecreatefromxbm($file);
				break;
			default:
				$im = false;
				break;
		}
		//var_dump($im);
		if (!$im) {
//			$res['info'] = getimagesize("img/no_preview.jpeg");
//			$res['type'] = explode('/',$res['info']['mime'])[1];
//			$res['image'] = imagecreatefromgd($file);;
//			return $res;
			return false;
		}else {
			$res['image'] = $im;
			return $res;
		}

	}

	private function imageresize($outfile, $infile, $new_h, $quality) {

		$image = $this->open_image($infile);
		//var_dump($image);
		//return 110;
		if ($image) {
			$im = $image['image'];
			$new_w = $image['info'][0]/($image['info'][1]/$new_h);

			header ('Content-Type: image/jpeg');

			$im1 = imagecreatetruecolor($new_w, $new_h);
			imagecopyresized($im1, $im, 0, 0, 0, 0, $new_w, $new_h, imagesx($im), imagesy($im));
			imagejpeg($im1, $outfile, $quality);
			imagedestroy($im);
			imagedestroy($im1);

			return true;
		} else {
			return false;
		}
	}
}
