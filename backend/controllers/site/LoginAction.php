<?php
namespace app\controllers\site;

use app\models\drupal\drupal_User;
use common\components\ldap;
use common\models\LoginForm;
use common\models\urg\Workers;
use Yii;
use yii\base\Action;

class loginAction extends Action
{
	public function run(){
		$request = Yii::$app->request;
		//\Yii::$app->db->schema->refresh();
		Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Credentials', 'true');
		Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Headers', $request->getHeaders()->get('Access-Control-Request-Headers'));
		Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Origin', $request->getHeaders()->get('origin'));

		if ($request->isOptions) {
			$options = ['POST'];
			Yii::$app->getResponse()->getHeaders()->set('Allow', implode(',', $options));
			//Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
			Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Methods', implode(',', $options));

			$res = implode(',', $options);

			return $res;
		}




		if ($request->isGet) {
//			if (!\Yii::$app->user->isGuest) {
//				return $this->goHome();
//			}

			$model = new LoginForm();
			//if ($model->load(Yii::$app->request->post()) && $model->login()) {
			//	return $this->goBack();
			//} else {
				return $this->controller->render('login', [
					'model' => $model,
				]);
			//}
		}

			// авторизация по ajax
			// возвращаем данные о пользователе
		if ($request->isPost) {
			Yii::$app->session->open();
			\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
			$res = [];
			$params = Yii::$app->request->bodyParams;

			$app_params = [
				'email' => Yii::$app->params['adminEmail'],
				'defaultPageSize' => Yii::$app->params['defaultPageSize'],
				'panorama_Url' => Yii::$app->params['panorama_Url']
			];

			if (isset($params['token'])) {
				$user = Workers::findIdentityByAccessToken($params['token']);
				if ($user && trim($user->roles->confvalue_urg)!='') {
					if ($user->token_created > (mktime() - Yii::$app->params['token_life_time'])) {
						$res = [
							'status' => 'ok',
							'message' => 'token auth',
							'user' => $user,
							'params' =>$app_params,
						];
					} else {
						$res = [
							'status' => 'error',
							'message' => 'token is too old',
							//'t' => $user->token_created.' - '.(mktime()-60*60*24*5),
							'user' => false
						];
					}
				} else {
					$res = [
						'status' => 'error',
						'message' => 'access denied',
						'user' => false
					];
				}
				$res['action'] = 'by token';
			}

			if (isset($params['sid'])) {

				$ldap_unit = [
					28 => '22',
					25 => '22',
					1  => '1',
                    2  => '8',
                    3  => '12',
                    6  => '2',
                    7  => '10',
                    9  => '18',
                    16 => '3',
                    17 => '6',
                    18 => '16',
                    14 => '19',
                    4  => '21',
                    22 => '5',
                    23 => '14',
                    24 => '20',
                    21 => '7',
                    11 => '9',
                    20 => '15',
                    13 => '13',
                    12 => '4',
                    10 => '11',
                    8  => '17',
//                    32 => '0',
//                    5  => '0',
//                    30 => '0'
				];


				$res['action'] = 'by sid';
				$sid = $params['sid'];

				$ident = drupal_User::findBySid($sid);
				$res['ident'] = $ident;

				if ($ident) {
					$mas = ldap_explode_dn(unserialize($ident->data)['ldap_dn'], true);
					$uid = $mas[0];
					//Utils::print_r($ident->data);
					//$res['mas'] = $mas;
					$ldap = new ldap();

					$ldap->authfromldap($uid, Yii::$app->params['confValue']);
					if ($ldap->user) {

						$res['ldap'] = $ldap->user;
						$res = [
							"status" => "ok",
							"message" => "Всё хорошо!",
							"user" => [
								'fio'     => $ldap->user->username,
								'id'      => 0,
								'id_post' => 0,
								'id_unit' => $ldap_unit[$ldap->user->area],
								'login'   => $ldap->user->name,
								'roles'   => $ldap->user->roles,
								'info'    =>   $ldap->user->info,
							],
							'params' => $app_params,
						];



					} else {
						$res['status'] = 'error';
						$res['message'] = 'Ошибка аутентификации!';
					}
				} else {
					$res['status'] = 'error';
					$res['message'] = 'session missing';
				}
			}

			if (isset($params['name'], $params['pass'])) {
				// проверяем логин и пароль
				$user = Workers::findOne(['login' => $params['name'], 'password' => md5($params['pass'])]);
				if ($user && trim($user->roles->confvalue_urg)!='') {
						// если токен пустой или очень старый
					if (trim($user->token)=='' || ($user->token_created < (mktime() - Yii::$app->params['token_life_time']))) {
						$token = Yii::$app->security->generateRandomString();
						$user->token = $token;
						$user->token_created = mktime();

						$user->save();
					} else {
						$token = $user->token;
					}



					Yii::$app->session['urg_user'] = [
						'id' => $user->id,
						'asses_token' => $token,
						'data' => $user
					];

					$res = [
						"status" => "ok",
						"message" => "Всё хорошо!",
						"user" => $user,
						"token" => $token,
						'params' => $app_params,
						'err' => $user->errors
					];
				} else {
					$res = [
						"status" => "error",
						"message" => "Неверный логин или пароль",
						"confvalue_urg" => trim($user->roles->confvalue_urg)
					];
				}
				//$res = $params;
				//$res = $user;
				$res['action'] = 'by name/pass';
			}

			$res['client_version'] = Yii::$app->params['client_version'];
			$res['sid'] = Yii::$app->session->getId();

			return $res;
		}


		return 'Wrong request';
	}
}

