<?php
namespace app\controllers\ring;


use common\models\urg\Ring;
use Yii;
use yii\base\Model;
use yii\db\Query;
use yii\rest\Action;

class listAllUrgAction extends Action {

	public $prepareDataProvider;
	public $scenario = Model::SCENARIO_DEFAULT;

	public function run() {
		$res = [];

		$request = Yii::$app->request;
		if ($request->isOptions) {

			$options = ['GET', 'HEAD', 'OPTOINS'];
			Yii::$app->getResponse()->getHeaders()->set('Allow', implode(',', $options));
			//Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
			Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Methods', implode(',', $options));

			$res = implode(',', $options);

			return $res;
		}


		if ($request->isGet) {
			$query1 = (new Query())
				->select([
					'id_ring',
					'GROUP_CONCAT(CONCAT(rb.name, \'-\', u.num) SEPARATOR \', \') AS urgs',
					'GROUP_CONCAT(ri.id_urg SEPARATOR \',\') AS id_urgs',
					//'GROUP_CONCAT(u.id_unit SEPARATOR \',\') AS id_unit'
					'(0) as id_unit'
				])
				->from('urg_ring ri')
				->innerJoin('urg u', 'u.id_obj=ri.id_urg')
				->innerJoin('urg_refbook rb', 'rb.id=u.id_type_urg')
				->where(['id_unit' => $_GET['id_unit']])
				->groupBy('id_ring');

			//$query1->where('false');

			$query2 = (new Query())
				->select([
					'(-1) as id_ring',
					'CONCAT(rb.name, \'-\', u.num) AS urgs',
					'u.id_obj as id_urgs',
					'id_unit'
				])
				->from('urg u')
				->innerJoin('urg_refbook rb', 'rb.id=u.id_type_urg')
				->where([
					'not in',
					'id_obj',
					(new Query())->select('id_urg')->distinct()->from('urg_ring')
				]);
			if (isset($_GET['urg_type']) && $_GET['urg_type'] !=0) {
				$query2->andWhere(['id_type_urg' => $_GET['urg_type']]);
			}
			if (isset($_GET['id_unit']) && $_GET['id_unit'] !=0) {
				$query2->andWhere(['id_unit' => $_GET['id_unit']]);
			}

			$query3 = (new Query())->select('*')->from(['u'=>$query1->union($query2)]);
			$res = $query3->orderBy('id_unit')->all();

			return $res;
		}

//		if ($request->isPost || $request->isPut) {
//			return $res;
//		}

		//Yii::$app->getResponse()->setStatusCode(405);
		return 'wrong request';
	}

	/* ------------------------------------------------------------------------------------ */

}
