<?php
namespace app\controllers\ring;


use common\models\urg\Ring;
use Yii;
use yii\base\Model;
use yii\db\Query;
use yii\rest\Action;

class indexAction extends Action {

	public $prepareDataProvider;
	public $scenario = Model::SCENARIO_DEFAULT;

	public function run() {
		$res = [];

		$request = Yii::$app->request;
		if ($request->isOptions) {

			$options = ['GET', 'HEAD', 'OPTOINS'];
			Yii::$app->getResponse()->getHeaders()->set('Allow', implode(',', $options));
			//Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
			Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Methods', implode(',', $options));

			$res = implode(',', $options);

			return $res;
		}


		if ($request->isGet) {

			$res =(new Query)->select([
				'u.id_obj as id_urg',
				'CONCAT(rb.name, \'-\', u.num) AS urg',
				'id_unit',
				'id_ring'
			])
				->from('urg_ring ri')
				->innerJoin('urg u', 'u.id_obj=ri.id_urg')
				->innerJoin('urg_refbook rb', 'rb.id=u.id_type_urg')
				//->where(['id_ring'=>$id_ring])
				//->asArray()
				->all();

			return $res;
		}

//		if ($request->isPost || $request->isPut) {
//			return $res;
//		}

		//Yii::$app->getResponse()->setStatusCode(405);
		return 'wrong request';
	}

	/* ------------------------------------------------------------------------------------ */

}
