<?php
namespace app\controllers\ring;


use common\models\urg\Ring;
use Yii;
use yii\base\Model;
use yii\rest\Action;

class makeAction extends Action {

	//public $prepareDataProvider;
	//public $scenario = Model::SCENARIO_DEFAULT;

	public function run() {
		$res = [];
		$request = Yii::$app->request;
		if ($request->isOptions) {

			$options = ['POST', 'PUT', 'OPTOINS'];
			Yii::$app->getResponse()->getHeaders()->set('Allow', implode(',', $options));
			//Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
			Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Methods', implode(',', $options));

			$res = implode(',', $options);

			return $res;
		}


		if ($request->isPost || $request->isPut) {
			$params = Yii::$app->request->bodyParams;
			// объединить два УРГ в кольцо. при этом $id_other_urg может уже быть в кольце
			$res = Ring::makeRing($params['id_current_urg'], $params['id_other_urg']);

			return $res;
		}

		//Yii::$app->getResponse()->setStatusCode(405);
		return 'wrong request';
	}

	/* ------------------------------------------------------------------------------------ */

}
