<?php

namespace backend\controllers;

use app\components\myActiveController;

class RegionController extends myActiveController
{

	public $modelClass = 'common\models\urg\SprRegion';

	public function behaviors()	{
		return
			\yii\helpers\ArrayHelper::merge(parent::behaviors(), [
				'corsFilter' => [
					'class' => \yii\filters\Cors::className(),
				],
			]);
	}

}

