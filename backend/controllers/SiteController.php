<?php
namespace backend\controllers;

use common\models\urg\Workers;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use common\models\LoginForm;
use yii\filters\VerbFilter;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['logout', 'login', 'error', 'about', 'tests', 'thumb', 'sendMail'] ,
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
//            'verbs' => [
//                'class' => VerbFilter::className(),
//                'actions' => [
//                    'logout' => ['post'],
//                ],
//            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
	        'login' => ['class' => 'app\controllers\site\LoginAction'],
	        'thumb' => ['class' => 'app\controllers\site\ThumbAction'],
	        'tests' => ['class' => 'app\controllers\site\TestsAction'],
            'about' => ['class' => 'app\controllers\site\AboutAction'],
            'sendMail' => ['class' => 'app\controllers\site\SendMailAction'],
            'error' => [
                'class' => 'app\controllers\site\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionLogin_old()
    {
//        if (!\Yii::$app->user->isGuest) {
//            return $this->goHome();
//        }
//
//        $model = new LoginForm();
//        if ($model->load(Yii::$app->request->post()) && $model->login()) {
//            return $this->goBack();
//        } else {
//            return $this->render('login', [
//                'model' => $model,
//            ]);
//        }
    }

    public function actionLogout()
    {
    	$request = Yii::$app->request;

	    Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Credentials', 'true');
	    Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Headers', $request->getHeaders()->get('Access-Control-Request-Headers'));
	    Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Origin', $request->getHeaders()->get('origin'));

	    if ($request->isOptions) {
		    $options = ['POST'];
		    Yii::$app->getResponse()->getHeaders()->set('Allow', implode(',', $options));
		    //Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
		    Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Methods', implode(',', $options));

		    $res = implode(',', $options);

		    return $res;
	    }

    	if (/*$request->isAjax && */$request->isPost) {
		    Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    		$res = [];
    		$token = $request->headers->get('urg_token');
    		//$token = $_POST['token'];
		    Yii::$app->session['logout'] = $token;
    		$user = Workers::findIdentityByAccessToken($token);
    		//$res['user'] = $user;
		    $res['status']='ok';
		    $res['message']='';

		    if ($user) {
			    $user->token = null;
			    $user->save();
			    $res['message']='Разлогирован';
		    } else {
			    $res['message']='не нужно разлогиниваться';
		    }
		    Yii::$app->session->close();
		    Yii::$app->session->destroy();
		    return $res;
	    }

        Yii::$app->user->logout();

        return $this->goHome();
    }
}
