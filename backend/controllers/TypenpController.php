<?php

namespace backend\controllers;
use app\components\myActiveController;

class TypenpController extends myActiveController
{

	public $modelClass = 'common\models\urg\SprTypeNp';

	public function behaviors()	{
		return
			\yii\helpers\ArrayHelper::merge(parent::behaviors(), [
				'corsFilter' => [
					'class' => \yii\filters\Cors::className(),
				],
			]);
	}
}

