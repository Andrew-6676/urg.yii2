<?php
namespace backend\controllers;

use app\components\myActiveController;
use Yii;


/**
 * Site controller
 */
class FileController extends myActiveController
{
	public $modelClass = 'common\models\urg\File';

	public function behaviors()	{
		return
			\yii\helpers\ArrayHelper::merge(parent::behaviors(), [
				'corsFilter' => [
					'class' => \yii\filters\Cors::className(),
				],
			]);
	}

	public function actions()
	{
		$actions = parent::actions();

		$actions['list']   = ['class' => 'app\controllers\file\ListAction'];
		$actions['upload'] = ['class' => 'app\controllers\file\UploadAction'];
		$actions['delete'] = ['class' => 'app\controllers\file\DeleteAction'];
		//$actions['view']   = $actions['list'];
		return $actions;
	}
//    public function actions()
//    {
//        return [
//            //'index'  => ['class' => 'app\controllers\file\IndexAction'],
//            'delete' => ['class' => 'app\controllers\file\DeleteAction'],
//            'upload' => ['class' => 'app\controllers\file\UploadAction'],
//            'list'   => ['class' => 'app\controllers\file\ListAction'],
//            'error' => [
//                'class' => 'app\controllers\site\ErrorAction',
//            ],
//        ];
//    }

	public function verbs()
	{
		$verbs = parent::verbs();

//		return [
//			'index' => ['GET', 'HEAD'],
//			'view' => ['GET', 'HEAD'],
//			'create' => ['POST'],
//			'update' => ['PUT', 'PATCH'],
//			'delete' => ['DELETE'],
//		];

		$verbs['upload'] = ['POST', 'OPTIONS'];
		$verbs['list'] = ['GET', 'OPTIONS'];
		$verbs['delete'] = ['DELETE', 'OPTIONS'];

		return $verbs;
	}

}
