<?php

namespace backend\controllers;
use app\models\SprNp;
use yii\helpers\ArrayHelper;
//use yii\rest\ActiveController;
use app\components\myActiveController;
use yii\data\ActiveDataProvider;

class SelsovController extends myActiveController
{

	public $modelClass = 'common\models\urg\SprSelsov';

	public function behaviors()	{
		return
			\yii\helpers\ArrayHelper::merge(parent::behaviors(), [
				'corsFilter' => [
					'class' => \yii\filters\Cors::className(),
				],
			]);
	}
}

