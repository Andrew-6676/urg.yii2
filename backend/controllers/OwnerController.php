<?php

namespace backend\controllers;
use app\components\myActiveController;

class OwnerController extends myActiveController
{

	public $modelClass = 'common\models\urg\SprOwner';

	public function behaviors()	{
		return
			\yii\helpers\ArrayHelper::merge(parent::behaviors(), [
				'corsFilter' => [
					'class' => \yii\filters\Cors::className(),
				],
			]);
	}
}

