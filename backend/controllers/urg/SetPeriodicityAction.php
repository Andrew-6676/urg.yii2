<?php
namespace app\controllers\urg;

use common\models\urg\Urg;
use common\models\urg\WorksReport;
use Yii;
use yii\base\Action;

class setPeriodicityAction extends Action
{
	public function run(){

		$res = null;
		$request = Yii::$app->request;

		$params = Yii::$app->params;

		Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Credentials', 'true');
		Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Headers', $request->getHeaders()->get('Access-Control-Request-Headers'));
		Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Origin', $request->getHeaders()->get('origin'));


		if ($request->isOptions) {
			\Yii::$app->response->format = \yii\web\Response::FORMAT_HTML;
			$options = ['POST', 'PUT'];
			Yii::$app->getResponse()->getHeaders()->set('Allow', implode(',', $options));
			//Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
			Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Methods', implode(',', $options));

			$res = implode(',', $options);
			return $res;
		}

		// запрос результатов синхрнизации
		if ($request->isPost || $request->isPut) {
			$res = [];
			$model = Urg::findOne($request->bodyParams['id']);
			if (!$model) {
				$res['status'] = 'error';
				$res['message'] = 'Не найден объект '.$request->bodyParams['id'];
				return $res;
			}

			$model->periodicity = json_encode($request->bodyParams['periodicity']);

			if ($model->save()) {
				$res['status'] = 'ok';
			} else {
				$res['status'] = 'error';
				$res['errors'] = $model->errors;
			};
		} else {
			$res = 'wrong request';
		}

		return $res;


	}
}
