<?php
namespace app\controllers\urg;

use common\models\urg\LinkObj;
use common\models\urg\Urg;
use common\models\urg\UrgRefbook;
use Yii;
use yii\base\Action;
use yii\helpers\ArrayHelper;

class clearAddressAction extends Action
{
	private $companiesFlat = [];

	public function run($id1, $id2){
		$res = null;
		$request = Yii::$app->request;

		$params = Yii::$app->params;

		Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Credentials', 'true');
		Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Headers', $request->getHeaders()->get('Access-Control-Request-Headers'));
		Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Origin', $request->getHeaders()->get('origin'));


		if ($request->isOptions) {
			\Yii::$app->response->format = \yii\web\Response::FORMAT_HTML;
			$options = ['DELETE'];
			Yii::$app->getResponse()->getHeaders()->set('Allow', implode(',', $options));
			//Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
			Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Methods', implode(',', $options));

			$res = implode(',', $options);
		}

		// запрос результатов синхрнизации
		if ($request->isDelete) {
			if (LinkObj::remove($id1, $id2) >0) {
				return ['status' => 'ok', 'message' => 'Адрес очищен'];
			} else {
				return ['status'=>'error', 'message'=>'При удалении адреса возникла ошибка'];
			}
		} else {
			$res = 'wrong request';
		}

		return $res;
	}

	private function getFlatCompanies($arr) {
		foreach ($arr as $item) {
			$this->companiesFlat[$item['id_gasb']] = [
				'id' => $item['id'],
				//'id_gasb' => $item['id_gasb'],
				'name'=> $item['name'],
				'parent' => $item['parent']
			];

			$this->getFlatCompanies($item['childs']);
		}
	}
}
