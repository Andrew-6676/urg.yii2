<?php
namespace app\controllers\urg;

use Yii;
use yii\base\Action;

class testAction extends Action
{
	public function run(){

		return [
			'msg'=>'test action urg controller',
			'action'=>'test',
			'controller'=>'urg'
		];
	}
}
