<?php
namespace app\controllers\urg;

use common\models\urg\Obj;
use common\models\urg\Urg;
use Yii;
use yii\base\Model;
use yii\rest\Action;
use yii\data\ActiveDataProvider;

class saveAction extends Action {

	public $prepareDataProvider;
	public $scenario = Model::SCENARIO_DEFAULT;

	public function run($id=-1) {

		$request = Yii::$app->request;
		if ($request->isOptions) {
			$options = ['POST', 'PUT', 'OPTOINS'];
			Yii::$app->getResponse()->getHeaders()->set('Allow', implode(',', $options));
			Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Methods', implode(',', $options));

			$res = implode(',', $options);
			return $res;
		}


		if ($request->isPost || $request->isPut) {
			$res = [
				'id' => $id,
				'status' => 'unknown',
				'message'=> '-',
			];

			$new_id = -1;
			if ($id>0) {    // Обновление записи
				$model = Urg::find()->where('id_obj=' . $id)->one();
				$res['action'] = 'update';
			} else {    // Добавление новой записи
					// создаём новый obj и получаем его id
				$new_id = Obj::addUrg();

				$model = new $this->modelClass([
					'scenario' => $this->scenario,
				]);
				$model->id_obj = $new_id;
				$res['action'] = 'create = '.$new_id;
			}

				// загружаем данные в модель из запроса
			$model->load(Yii::$app->request->bodyParams, '');
			$res['model'] = $model;

			if ($model->save() !== false) {
				$res['status'] = 'ok';
				$res['message'] = 'УРГ сохранен успешно';
				$res['id'] = $model->id;
					// сохраняем адрес
				$sa = $model->setAddress(Yii::$app->request->bodyParams['address']);
				$res['res_adr'] = $sa;
				if ($sa['status']!='ok') {
					$res['message'] = 'УРГ сохранён. '.$sa['message'];
					$res['errors'] = $sa['errors'];
				};

			} else {
					// если новый УРГ не создался - удаляем объект из БД
				if ($id<0)	Obj::del($new_id);
				$res['status'] = 'error';
				$res['message'] = 'Во время сохранения УРГ возникла ошибка.';
				$res['errors'] = $model->errors;
			}


			return $res;
		}

		//Yii::$app->getResponse()->setStatusCode(405);
		return 'wrong request';
	}

	/* ------------------------------------------------------------------------------------ */

	protected function prepareDataProvider() {
		if ($this->prepareDataProvider !== null) {
			return call_user_func($this->prepareDataProvider);
		}

		/* @var $modelClass \yii\db\BaseActiveRecord */
		$modelClass = $this->modelClass;

		return new ActiveDataProvider([
			'query' => $modelClass::find(),
		]);
	}
}
