<?php
namespace app\controllers\urg;

use common\models\urg\Urg;
use common\models\urg\UrgRefbook;
use Yii;
use yii\base\Action;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

class getStatAction extends Action
{
	private $companiesFlat = [];

	public function run($id=null){
		$res = null;
		$request = Yii::$app->request;

		$params = Yii::$app->params;

		Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Credentials', 'true');
		Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Headers', $request->getHeaders()->get('Access-Control-Request-Headers'));
		Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Origin', $request->getHeaders()->get('origin'));


		if ($request->isOptions) {
			\Yii::$app->response->format = \yii\web\Response::FORMAT_HTML;
			$options = ['GET'];
			Yii::$app->getResponse()->getHeaders()->set('Allow', implode(',', $options));
			//Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
			Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Methods', implode(',', $options));

			$res = implode(',', $options);
		}

		// запрос результатов синхрнизации
		if ($request->isGet) {
			$res = [];

			if (file_exists('../cache/companies_')) {
				// получаем дерево подразделений из кэша
				$content = file_get_contents('../cache/companies_');
			} else {
				if (!file_exists('../cache')) {
					mkdir('../cache',0777,true);
				}
				// получаем дерево подразделений от сервиса
				echo Yii::$app->params['company_API'];
				$content = file_get_contents(Yii::$app->params['company_API']);
				file_put_contents('../cache/companies_', $content);
			}

			// преобразуем дерево подразделений в массив
			//$companies = (\yii\helpers\Json::decode($content));
			$companies = json_decode($content, true, 512, JSON_BIGINT_AS_STRING);

			$this->getFlatCompanies($companies);
			//echo '<pre>'.print_r($this->companiesFlat, true).'</pre>';
			//echo '<pre>'.print_r($content, true).'</pre>';
			//exit;

			$urg = Urg::find()
				->select('id_unit, name, count(*) as count')
				->joinWith('type')
				->groupBy(['id_unit','name'])
				->asArray()
				->all();
			// индексируем массив
			$arr = ArrayHelper::map($urg, 'name', 'count', 'id_unit');

			/*----------------------------------------------------------*/
			// подсчёт кол-ва файлов к УРГ
			$sql_f = "select u.id_unit, count(lo.id_obj2) as count
					from urg u
						inner join link_obj lo on lo.id_obj1=u.id_obj and lo.id_type_obj2=2
					group by u.id_unit, lo.id_type_obj2";
			$u_files = Yii::$app->db->createCommand($sql_f)->queryAll();
			$u_files = ArrayHelper::index($u_files, 'id_unit');
			//print_r($u_files);
			// подсчёт кол-ва оборудования
			$sql = 'select u.id_unit, so.type_obj, count(lo.id_obj2) as count
					from urg u
						inner join link_obj lo on lo.id_obj1=u.id_obj and lo.id_type_obj2<>5 and lo.id_type_obj2<>2
						inner join spr_type_obj so on so.id_type_obj=lo.id_type_obj2
					group by u.id_unit, so.type_obj';

			$eqs = Yii::$app->db->createCommand($sql)->queryAll();
			//print_r($eqs);

			$total_eq = [];
			$equipment = [];
			foreach ($eqs as $eq) {
				$unit = $eq['id_unit'];
				$eq_type = $eq['type_obj'];
				$equipment[$unit][$eq_type] = $eq['count'];

				if (!array_key_exists($eq_type, $total_eq)) $total_eq[$eq_type] = 0;
				$total_eq[$eq_type] = $total_eq[$eq_type] ? $total_eq[$eq_type]+$eq['count'] : $eq['count'];
			}
			//print_r($total_eq);
			//print_r($equipment);

			/*----------------------------------------------------------*/


			$byType = [];
			$byTypeEq = [];
			$byUnit = [];
			$byUnitAndType = [];
			$total = 0;
			$total_e = 0;
			$total_files = 0;

			foreach ($arr as $unit => $rs) {
				$u = $this->companiesFlat[$unit]['name'];

				if (!array_key_exists($u, $byUnit))
					$byUnit[$u] = 0;
				$subtotal = 0;
				foreach ($rs as $type => $r) {
					$subtotal += $r;
					$byUnit[$u] = $byUnit[$u] + $r;
					$t = $type;
					if (!array_key_exists($t, $byType))
						$byType[$t] = 0;

					$byType[$t] = $byType[$t] + $r;

					$byUnitAndType[$u]['urg']['objects'][$t] = $r;
					$byUnitAndType[$u]['urg']['files'] = $u_files[$unit]['count'];
					$total_files += $u_files[$unit]['count'];
					$total += $r;
				}
				$byUnitAndType[$u]['urg']['objects_count'] = $subtotal;
				$byUnitAndType[$u]['equipment']['objects'] = $equipment[$unit];
				$byUnitAndType[$u]['equipment']['objects_count'] = 0;
				foreach ($equipment[$unit] as $key => $e) {
					$byUnitAndType[$u]['equipment']['objects_count'] += $e;
					$total_e += $e;

					$byTypeEq[$key] = $byTypeEq[$key] ? $byTypeEq[$key] + $e : $e;
				}
				$byUnitAndType[$u]['equipment']['files'] = 0;
			}

			$res = [
				//'raw' => $arr,
				'total_urg' => $total,
				'total_equipment' => $total_e,
				'total_files' => $total_files,
				'countByUnitAndType' => $byUnitAndType,
				'countByType' => $byType,
				'countByTypeEq' => $byTypeEq,
				'countByUnit' => $byUnit,
			];
		} else {
			$res = 'wrong request';
		}

		return $res;
	}

	private function getFlatCompanies($arr) {
		foreach ($arr as $item) {
			$this->companiesFlat[$item['id_gasb']] = [
				'id' => $item['id'],
				//'id_gasb' => $item['id_gasb'],
				'name'=> $item['name'],
				'parent' => $item['parent']
			];

			$this->getFlatCompanies($item['childs']);
		}
	}
}
