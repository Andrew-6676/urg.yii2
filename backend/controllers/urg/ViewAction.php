<?php
namespace app\controllers\urg;

use common\models\urg\Urg;
use Yii;
use yii\rest\Action;
use yii\data\ActiveDataProvider;

class viewAction extends Action {

	public $prepareDataProvider;

	public function run($id) {

		$request = Yii::$app->request;
		if ($request->isOptions) {

			$options = ['GET', 'HEAD', 'OPTOINS'];
			Yii::$app->getResponse()->getHeaders()->set('Allow', implode(',', $options));
			//Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
			Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Methods', implode(',', $options));

			$res = implode(',', $options);

			return $res;
		}


		if ($request->isGet) {

			if ($this->checkAccess) {
				call_user_func($this->checkAccess, $this->id);
			}
			return $this->prepareDataProvider($id);
		}

		return 'Wrong request';
	}

	/* ------------------------------------------------------------------------------------ */

	protected function prepareDataProvider($id) {
//		if ($this->prepareDataProvider !== null) {
//			return call_user_func($this->prepareDataProvider);
//		}

		/* @var $modelClass \yii\db\BaseActiveRecord */
		$modelClass = $this->modelClass;

		return new ActiveDataProvider([
			'query' => $modelClass::find(),
		]);
	}
}
