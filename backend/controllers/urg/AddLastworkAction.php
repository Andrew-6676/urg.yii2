<?php
namespace app\controllers\urg;

use common\models\urg\WorksReport;
use Yii;
use yii\base\Action;

class addLastworkAction extends Action
{
	public function run(){

		$res = null;
		$request = Yii::$app->request;

		$params = Yii::$app->params;

		Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Credentials', 'true');
		Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Headers', $request->getHeaders()->get('Access-Control-Request-Headers'));
		Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Origin', $request->getHeaders()->get('origin'));


		if ($request->isOptions) {
			\Yii::$app->response->format = \yii\web\Response::FORMAT_HTML;
			$options = ['GET'];
			Yii::$app->getResponse()->getHeaders()->set('Allow', implode(',', $options));
			//Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
			Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Methods', implode(',', $options));

			$res = implode(',', $options);
			return $res;
		}

		// запрос результатов синхрнизации
		if ($request->isGet) {
			$res = [];
			$model = WorksReport::find()->where(['id_obj'=>$_GET['id_urg']])->one();
			if (!$model) {
				$model = new WorksReport();
				$model->id_obj = $_GET['id_urg'];
			}

			$model->complete = true;
			$model->work_date = $_GET['date'];

			if ($model->save()) {
				$res['status'] = 'ok';
			} else {
				$res['status'] = 'error';
			};
		} else {
			$res = 'wrong request';
		}

		return $res;


	}
}
