<?php

namespace backend\controllers;
use app\components\myActiveController;
use common\models\urg\Urg;
use common\models\urg\UrgEquipment;

class ScheduleController extends myActiveController
{

	public $modelClass = 'common\models\urg\WorksSchedule';

	public function behaviors()	{
		return
			\yii\helpers\ArrayHelper::merge(parent::behaviors(), [
				'corsFilter' => [
					'class' => \yii\filters\Cors::className(),
				],
			]);
	}

	public function actions()
	{
		$actions = parent::actions();

		$actions['view']         = ['class' => 'app\controllers\schedule\ViewAction',        'modelClass' => $this->modelClass,];
		$actions['print']        = ['class' => 'app\controllers\schedule\PrintAction',       'modelClass' => $this->modelClass,];
		$actions['create']       = ['class' => 'app\controllers\schedule\SaveAction',        'modelClass' => $this->modelClass,];
		$actions['update']       = ['class' => 'app\controllers\schedule\SaveAction',        'modelClass' => $this->modelClass,];
		$actions['prepareData']  = ['class' => 'app\controllers\schedule\PrepareDataAction', 'modelClass' => $this->modelClass,];
		$actions['getWorksCfg']  = ['class' => 'app\controllers\schedule\GetWorksCfgAction', 'modelClass' => $this->modelClass,];
		$actions['saveWorksCfg'] = ['class' => 'app\controllers\schedule\SaveWorksCfgAction','modelClass' => $this->modelClass,];
		$actions['getHolidays']  = ['class' => 'app\controllers\schedule\GetHolidaysAction', 'modelClass' => $this->modelClass,];

		return $actions;
	}

	/*------------------------------------------------------------------------------------------------------------*/
	public function prepareData($ids) {
		// должен вернуть objects, obj_headers,  groups
		$res['objects'] = [];
		$res['obj_headers'] = [
			//'ФИО',
			'Номер',
			'Адрес',
			//'Регулятор',
			//'Выходное давление'
		];
		$res['groups'] = [];

		$tmp_objects = Urg::find()
			->where(['in', 'id_obj', $ids])
			->with(['type', 'address', 'master', 'works'])
			->orderBy('id_type_urg, num_sort')
			->indexBy('id_obj')
			->all();

		// формируем группы
		$tmp_groups = [];
		foreach ($tmp_objects as $id_obj => $obj) {
			$tmp_groups[$obj->master->name] = 1;
		}

		$i = 1;
		$tmp_groups2 = [];
		foreach ($tmp_groups as $gr => $none) {
			$tmp_groups2[$gr] = $i++;
		}

		foreach ($tmp_objects as $id_obj => $obj) {
			$o = $obj->toArray([], ['type', 'master', 'full_address', 'lastwork']);

			/*-----------------------------*/
			// выбрать PC, PZK, PSK
			$eqs = UrgEquipment::find()
				//->select('id_obj')
				->joinWith('linkObjAsChild')
				->where('id_obj1=:id_urg', ['id_urg'=>$id_obj])
				->andWhere(['in', 'id_type_obj2', [32,81,82]])
				->orderBy('order')
				//->asArray()
				->all();


			$eq_arr = [
				32 => [],
				81 => [],
				82 => []
			];
			foreach ($eqs as $eq) {
				//print_r($eq->id_eq_type.'_'.$eq->id_obj);
				//echo "\n";
				//print_r($eq->data->model->model);
				$eq_arr[$eq->id_eq_type][] = $eq->data->model->model;
				//echo "\n";
			}
			//print_r($eq_arr);

			/*-----------------------------*/

			$res['objects'][] = [
				'id' => $id_obj,
				//'id_group' => $tmp_groups2[$o['master']['name']],
				'id_type_urg' => $o['id_type_urg'],
				'num_sort' => $o['num_sort'],
				'name' => $o['type']['name'].'-'.$o['num'],
				'data' => [
					$o['full_address'],
					//'<регулятор>',
					//'<давление>',огородникова
				],
				'periodicity' => json_decode($o['periodicity']),
				'telemetry' => $o['id_telemetry']>0,
				'equipment' => [
					'pc' => $eq_arr[32],
					'pzk' => $eq_arr[81],
					'psk' => $eq_arr[82],
					'pressures' => [$o['pressure_out_1'], $o['pressure_out_2'], $o['pressure_out_3'], $o['pressure_out_4']]
				],
				// по нужным видам работ 3-5 последних дат выполнения работ, отсортированных по убыванию - {id_work : [last_date, pre-last_date,...]}
				// 1184 - id работы из справочника
				'works' => [
						1185 => [$o['lastwork']],
				]
			];
		}

//		foreach ($tmp_groups2 as $name => $gid) {
//			$res['groups'][$gid] = ['name'=>$name];
//		}

		return $res;
	}

}

