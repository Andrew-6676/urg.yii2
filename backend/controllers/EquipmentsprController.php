<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;

/**
 * Print  controller
 */
class EquipmentsprController extends Controller
{


	public function actions()
	{
		$actions['getData']   = ['class' => 'app\controllers\equipmentspr\GetDataAction'];
#		$actions['upload'] = ['class' => 'app\controllers\file\UploadAction'];
#		$actions['delete'] = ['class' => 'app\controllers\file\DeleteAction'];
		return $actions;
	}

}
