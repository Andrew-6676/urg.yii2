<?php
namespace app\controllers\file;

use Yii;
use yii\base\Action;
use common\models\urg\File;
use common\models\urg\LinkObj;

class uploadAction extends Action
{
	/** Загрузка файла на сервер	 */
	public function run(){
		$res = [];

			//получаем путь загрузки
		$base_path = Yii::$app->params['uploads_dir'].'/';

			//
		$request = Yii::$app->request;

		Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Credentials', 'true');
		Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Headers', Yii::$app->request->getHeaders()->get('Access-Control-Request-Headers'));
		Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Origin', Yii::$app->request->getHeaders()->get('origin'));

			// метод запроса OPTIONS
		if ($request->isOptions) {
			\Yii::$app->response->format = \yii\web\Response::FORMAT_HTML;
			$options = ['POST'];
			Yii::$app->getResponse()->getHeaders()->set('Allow', implode(',', $options));
			//Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
			Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Methods', implode(',', $options));

			$res = implode(',', $options);
		}

			// метод запроса POST
		if ($request->isPost) {
			\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
			$res['input_params'] = [
				'files' => $_FILES,
				'file' => isset($_FILES['file']),
				'id_urg' => Yii::$app->request->getHeaders()->get('id_urg'),    // id УРГ
				'metadata' => json_decode(urldecode(Yii::$app->request->getHeaders()->get('metadata'))),    // id профицентра
				'title' => Yii::$app->request->getHeaders()->get('title'),
				'category' => Yii::$app->request->getHeaders()->get('category')
			];
			$res['status'] = '--';

			if (isset($_FILES['file']) && $_FILES['file']['error']==0) {
				$res['status'] = '1';

				$metadata = json_decode(urldecode(Yii::$app->request->getHeaders()->get('metadata')));    // id профицентра
				$unit = $metadata->id_unit->id;
				$urg  = $metadata->id_urg;

				if (!$urg || $urg<0 || $urg=='') {
					$res['status'] = 'error';
					$res['message'] = 'Неверный ID объекта:"'.$urg.'"';
				}

				$subDir = $unit.'/'.substr(md5($urg),0,4);
					// если нету такой подпапки - создаём
				$this->checkDir($base_path.$subDir);
					// описание файла введённое пользователем
				$title  =  Yii::$app->request->getHeaders()->get('title');
				if (!$title)
					$title = 'файл';
				else
					$title = urldecode($title);

				$category  =  Yii::$app->request->getHeaders()->get('category');
				if (!$category)
					$category = 0;

					// имя файла от пользователя
				$originalName = $_FILES['file']['name'];
				$ext = '.'.pathinfo($originalName, PATHINFO_EXTENSION);
					// генерируем уникальное имя
				$generatedName = $urg.'_'.str_ireplace('.', '', microtime(true)).$ext;
					// полный путь к директории файла на сервере
				$filePath = $base_path.$subDir;

					// если нет прав на запись
				if (!is_writable($base_path)) {
					$res['status'] = 'error';
					$res['message'] = 'Destination directory not writable.';
					return $res;
				}

				$iuf = is_uploaded_file($_FILES['userfile']['tmp_name']);
				$res['iuf'] = $iuf;

					// высчитать md5 загружаемого файла
				$md5 = md5_file($_FILES['file']['tmp_name']);
				$file = File::findOne(['md5sum'=>$md5]);
					// если файл с таким хэшем уже есть в БД - просто добавляем ссылку
				if ($file) {
					// файл не копируем, создаём только link на существующую запись в files
					// проверяем уникальность title и category
					$file2 = File::findOne(['md5sum'=>$md5, 'title'=>$title, 'id_category'=>$category]);
						// если идентичная запись уже есть - добавляем только link
					if ($file2) {
						$fs = LinkObj::add($urg, $file2->id_obj);
					} else {    // иначе создаём новую запись в files и link
						$fs = File::add($urg, $file->path, $file->name, $title, $md5, $category);
					}

					if ($fs===false) {
						$res['status'] = 'error';
						$res['message'] = 'Ошибка записи в БД. (Файл уже существует.)';
						$res['data'] = $fs;
						//Yii::$app->response->setStatusCode(500);
						return $res;
						//throw new \yii\web\ServerErrorHttpException("Error writing to database");
					} else {
						$res['status'] = 'ok';
						$res['message'] = 'Запись добавлена. (Файл уже существует.)';
					}
				} else {
					// если файла ещё нету в БД
					// сохраняем файл
					$r = move_uploaded_file($_FILES['file']['tmp_name'], $filePath.'/'.$generatedName);
					if ($r===true) {
						$res['status'] = 'ok';
						$res['message'] = 'Файл "'.$originalName.'" загружен как "'.$generatedName.'"';

						//$md5 = md5_file($filePath.'/'.$generatedName);
						// Добавить новый файл к УРГ в БД
						$fs = File::add($urg, $filePath, $generatedName, $title, $md5, $category);
						if ($fs===false) {
							$res['status'] = 'error';
							$res['message'] = 'Ошибка записи в БД.';
							//$res['data'] = $fs;
							//Yii::$app->response->setStatusCode(500);
							//TODO: удалить загруженный файл или как?
							return $res;
							//throw new \yii\web\ServerErrorHttpException("Error writing to database");
						};
					} else {
						// ошибка загрузки файла
						$res['status'] = 'error';
						$res['message'] = 'Ошибкба копирования загруженного файла.';
						$res['data'] = $_FILES['file']['tmp_name'] . ' -> ' . $filePath.'/'.$generatedName;
						//Yii::$app->response->setStatusCode(500);
						//throw new \yii\web\HttpException(500, "Error move_uploaded_file");
					}
				}

			} else {
				$res['status'] = 'error';
				$res['message'] = 'Файл не загружен.';
				//Yii::$app->response->setStatusCode(500);
				//throw new \yii\web\ServerErrorHttpException("No file uploaded.");
			}
		}

		return $res;
	}

	private function checkDir($dir) {
		if (!file_exists($dir)) {
			mkdir($dir, 0770, true);
		}
	}
}
