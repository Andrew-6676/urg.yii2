<?php
namespace app\controllers\file;

use Yii;
use yii\base\Action;

class indexAction extends Action
{
	public function run(){
		$res = ['response'=>'ok'];

		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

		return $res;
	}
}
