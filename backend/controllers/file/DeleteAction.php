<?php
namespace app\controllers\file;

use common\models\urg\File;
use Yii;
use yii\base\Action;

class deleteAction extends Action
{
	public function run($id=null) {
		$res = null;
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$request = Yii::$app->request;
		Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Credentials', 'true');
		Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Headers', Yii::$app->request->getHeaders()->get('Access-Control-Request-Headers'));
		Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Origin', Yii::$app->request->getHeaders()->get('origin'));

		if ($request->isOptions) {
			\Yii::$app->response->format = \yii\web\Response::FORMAT_HTML;
			$options = ['DELETE'];
			Yii::$app->getResponse()->getHeaders()->set('Allow', implode(',', $options));
			//Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
			Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Methods', implode(',', $options));

			$res = implode(',', $options);
		}

		if ($request->isDelete) {
			$res = null;
			if ($id===null) {
				Yii::$app->getResponse()->setStatusCode(404);
				//$res = ['response'=>'ok'];
			} else {
				$res = File::del($id);
			}
		}

		if ($res)
			return ['status'=>'ok', 'message'=>'Файл удалён'];
		else
			return ['status'=>'error', 'message'=>'Ошибка удаления файла'];
	}
}
