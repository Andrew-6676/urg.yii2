<?php
namespace app\controllers\file;

use common\models\urg\File;
use common\models\urg\Obj;
use common\models\urg\Urg;
use common\models\urg\UrgEquipment;
use Yii;
use yii\base\Action;
use yii\db\ActiveRecordInterface;

class listAction extends Action
{
	public function run($id=null){
		$res = null;
		$request = Yii::$app->request;

		if ($request->isOptions) {
			\Yii::$app->response->format = \yii\web\Response::FORMAT_HTML;
			$options = ['GET', 'OPTIONS'];
			Yii::$app->getResponse()->getHeaders()->set('Allow', implode(',', $options));
			//Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
			Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Methods', implode(',', $options));

			$res = implode(',', $options);
		}

		/* @var $modelClass ActiveRecordInterface */
		/* @var $r Urg */
		/* @var $r UrgEquipment */



		// отпределяем тип объёкта по его id
//		$type_obj = Obj::findOne($id)->typeObj->name_table;
//		$class_arr = [
//			'urg' => 'common\models\urg\Urg',
//			'irg_equipment' => 'common\models\urg\UrgEquipment'
//		];

		//$modelClass = $class_arr[$type_obj];
		//return $modelClass;

		if ($request->isGet) {
			$r = Obj::findOne($id);
			if (!$r) {
				Yii::$app->getResponse()->setStatusCode(404);
			} else {
				$res = $r->files;
			}

			\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		}

		//$res['class'] = $_GET['class'];
		return $res;
	}
}
