<?php

namespace backend\controllers;
//use common\models\SprNp;
//use yii\helpers\ArrayHelper;
//use yii\rest\ActiveController;
use app\components\myActiveController;
use yii\data\ActiveDataProvider;
use Yii;

class RingController extends myActiveController
{

	public $modelClass = 'common\models\urg\Ring';
	//public $with = ['manufacturer','type','buildingType','address','heating','power_supply','owner', 'linkObjAsParent'];
	
	public function behaviors()	{
		return
			\yii\helpers\ArrayHelper::merge(parent::behaviors(), [
				'corsFilter' => [
					'class' => \yii\filters\Cors::className(),
				],
			]);
	}

	public function actions()
	{
		$actions = parent::actions();


		unset($actions['index']);

		$actions['index'] = [
			'class' => 'app\controllers\ring\IndexAction',
			'modelClass' => $this->modelClass,
		];
		$actions['list'] = [
			'class' => 'app\controllers\ring\ListByIdAction',
			'modelClass' => $this->modelClass,
		];
		$actions['listAllUrg'] = [
			'class' => 'app\controllers\ring\ListAllUrgAction',
			'modelClass' => $this->modelClass,
		];
		$actions['make'] = [
			'class' => 'app\controllers\ring\MakeAction',
			'modelClass' => $this->modelClass,
			];
		$actions['create'] = [
			'class' => 'app\controllers\ring\CreateAction',
			'modelClass' => $this->modelClass,
		];

		return $actions;
	}
}

