<?php

namespace backend\controllers;

use app\components\myActiveController;

class UnitController extends myActiveController
{

	public $modelClass = 'common\models\urg\Units';
	public $with = ['subUnits', 'linkObjAsParent', 'linkUnitAsParent', 'obj'];

	public function behaviors()	{
		return
			\yii\helpers\ArrayHelper::merge(parent::behaviors(), [
				'corsFilter' => [
					'class' => \yii\filters\Cors::className(),
				],
			]);
	}

	public function actions()
	{
		$actions = parent::actions();

		$actions['list'] = ['class' => 'app\controllers\unit\ListAction'];

		return $actions;
	}




}

