<?php

namespace backend\controllers;
use app\components\myActiveController;

class RoomController extends myActiveController
{

	public $modelClass = 'common\models\urg\UrgRoom';

	public function behaviors()	{
		return
			\yii\helpers\ArrayHelper::merge(parent::behaviors(), [
				'corsFilter' => [
					'class' => \yii\filters\Cors::className(),
				],
			]);
	}
}

