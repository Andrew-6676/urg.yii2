<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $code; //$name;
?>
<div class="site-error">
	<img style="float: left;" src="/img/<?= $code ?>.png">
	<div style="float: left; margin-top: 100px;">

		Ошибка <?= $code ?>
	    <div class="alert alert-danger">
	        <?= nl2br(Html::encode($message)) ?>
	    </div>

		<br><br>
		<?= Html::a('На главную', '/') ?>
		<br>
		<br>
		<?= Html::a('Обратная связь', '/site/contact') ?>
	</div>
<!--    <p>-->
<!--	    Вышеупомянутая ошибка произошла, в то время как веб-сервер обрабатывал Ваш запрос-->
<!--    </p>-->
<!--    <p>-->
<!--        Please contact us if you think this is a server error. Thank you.-->
<!--    </p>-->

</div>
