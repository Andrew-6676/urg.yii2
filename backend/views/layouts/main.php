<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
//use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->params['appShortName'],
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
	    'encodeLabels' => false,
        'items' => [
            ['label' => '<span class="glyphicon glyphicon glyphicon-home"></span>&nbsp;'.'Главная', 'url' => ['/site/index']],
            ['label' => '<span class="glyphicon glyphicon-wrench"></span>&nbsp;'.'Тесты', 'url' => ['/site/tests']],
            ['label' => '<span class="glyphicon glyphicon glyphicon-info-sign"></span>&nbsp;'.'О приложении', 'url' => ['/site/about']],
            Yii::$app->user->isGuest ? (
                ['label' => '<span class="glyphicon glyphicon-log-in"></span>&nbsp;'.'Войти', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
	                '<span class="glyphicon glyphicon-log-out"></span>&nbsp;'.'Выйти (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; <?=  Yii::$app->params['myCompany'] ?>, <?= date('Y') ?></p>

        <p class="pull-right"><a href="https://angularjs.org/" target="_blank"><img width="16" src="/img/angular.ico"> Angular 2</a></p>
        <p class="pull-right">&nbsp;&nbsp;&nbsp;&nbsp;</p>
        <p class="pull-right"><a href="http://www.yiiframework.com/" target="_blank" rel="external"><img src="/img/yii2.ico"> Yii 2</a></p>
        <p class="pull-right">&nbsp;&nbsp;&nbsp;&nbsp;</p>
        <p class="pull-right">Работает на</p>

    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
