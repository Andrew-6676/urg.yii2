<?php


$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
	'language' => 'ru-RU',
	'sourceLanguage' => 'ru-RU',
	'id' => 'urg.backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => [
    	'log',
	    'app\modules\journal\Bootstrap',
	    'app\modules\printer\Bootstrap'
    ],
    'modules' => [
	    'journal' => [
		    'class' => 'app\modules\journal\Module',
		    // ... другие настройки модуля ...
	    ],
	    'printer' => [
		    'class' => 'app\modules\printer\Module',
	    ],
    ],


    'components' => [
	    'request' => [
		    'enableCsrfValidation' => false,
		    'enableCookieValidation' => false,
		    // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
		    //'cookieValidationKey' => 'vMYkjHeWOAo9qq1iBIUUXdqw-Z6qYyUv',
		    'parsers' => [
			    'application/json' => 'yii\web\JsonParser',
		    ],
	    ],
        'user' => [
            'identityClass' => 'common\models\urg\Workers',
            'enableAutoLogin' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
            	[
		            'class' => 'app\components\DbLogger',
		            'levels' => ['info'],
		            'categories' => ['yii\db\Command::execute'],
	            ],
	            YII_DEBUG ? [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ] : [
		            'class' => 'yii\log\FileTarget',
		            'levels' => ['error'],
	            ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
		    //'enableStrictParsing' => true,
	        'rules' => [
		        ['class' => 'yii\rest\UrlRule',
			        'controller' => [
				        'unit',
				        'region',
				        'np',
				        'street',
				        'worker',
				        'typenp',
				        'selsov',
				        'equipment',
				        'urg',
				        'file',
				        'refbook',
				        'ring',
				        'owner',
				        'typeobject',
				        'manufacturer',
				        'diameter',
				        'help',
				        'instrumentation',
				        'instrumentationspr',
				        'room',
				        'urgmaster',
				        'schedule'
		            ]
		        ],

		        //'catchAll' => ['site/offline'],


		        'schedules/print/<id>'   => 'schedule/print',
		        'schedules/prepareData'  => 'schedule/prepareData',
		        'schedules/getWorksCfg'  => 'schedule/getWorksCfg',
		        'schedules/saveWorksCfg' => 'schedule/saveWorksCfg',
		        'schedules/getHolidays'  => 'schedule/getHolidays',

		        'urgmasters/search'        => 'urgmaster/search',
		        'urgs/search'              => 'urg/search',
                'urgs/addLastwork'         => 'urg/addLastwork',
                'urgs/getStat'             => 'urg/getStat',
                'urgs/setPeriodicity/<id>' => 'urg/setPeriodicity',

		        'sendMail'          => 'site/sendMail',
                'filesList/<id>'    => 'file/list',
                'DELETE,OPTIONS urgs/clearAddress/<id1>/<id2>'   => 'urg/clearAddress',     // очистить адрес

                'GET,HEAD,OPTIONS equipmentspr/getData/<id_eq_type>'  => 'equipmentspr/getData',  // получить справочники по выбранному оборудованию

		        'GET,HEAD,OPTIONS urgs/list'                 => 'urg/list',             // полный список УРГ
		        'GET,HEAD,OPTIONS urgs/listByUnit/<id_unit>' => 'urg/list',             // список УРГ для указанного подразделения

		        'PUT,POST,OPTIONS equipments/saveOrder'                => 'equipment/saveOrder',    // обновить сотировку оборудования
		        'PUT,POST,OPTIONS equipments/saveRegimCard'            => 'equipment/saveRegimCard',    // обновить сотировку оборудования
		        'GET,HEAD,OPTIONS equipment/list'                      => 'equipment/list',    // список оборудования по всем УРГ
		        'GET,HEAD,OPTIONS equipment/listByUrg/<id_urg>'        => 'equipment/list',    // список оборудования для указанного УРГ (общие)
		        'GET,HEAD,OPTIONS equipment/listForRegimCard/<id_urg>' => 'equipment/listForRegimCard',    // список оборудования для указанного УРГ (общие)
		        'GET,HEAD,OPTIONS equipment/listDataByUrg/<id_urg>'    => 'equipment/listData',  // список оборудования для указанного УРГ (подробности)

		        'GET,HEAD,OPTIONS instrumentations/listByUrg/<id_urg>' => 'instrumentation/list',    // список КИП для указанного УРГ (общие)

		        'GET,HEAD,OPTIONS units/list'                => 'unit/list',             // список облгазов и филиалов
		        'GET,HEAD,OPTIONS units/list/<id>'           => 'unit/list',             // филиал по id

		        'GET,HEAD,OPTIONS,POST rings/make'           => 'ring/make',            // филиал по id
		        'rings/list/<id_ring>'                       => 'ring/list',            // список ург в указанном кольце
		        'rings/listAllUrg'                           => 'ring/listAllUrg',      // список всех ург с учётом колец


		        //'<controller:\w+>/<action:\w+>' => '<controller>/<action>',
		        ''       => 'site/index',
		        'login'  => 'site/login',
		        'logout' => 'site/logout',
		        'about'  => 'site/about',
		        'thumb/<path:.*>'=>'site/thumb',
		        //'plant/<id:\d+>'=>'plant/view',
	        ],
        ],
    ],
	// Обрезание обратного слэша в конце адресной строки
//	'on beforeRequest' => function () {
//		$pathInfo = Yii::$app->request->pathInfo;
//		$query = Yii::$app->request->queryString;
//		if (!empty($pathInfo) && substr($pathInfo, -1) === '/') {
//			$url = '/' . substr($pathInfo, 0, -1);
//			if ($query) {
//				$url .= '?' . $query;
//			}
//			Yii::$app->response->redirect($url, 301)->send();
//		}
//	},
//	'on beforeRequest' => function () {
//		$pathInfo = Yii::$app->request->pathInfo;
//		$query = Yii::$app->request->queryString;
//		if (!empty($pathInfo) && substr($pathInfo, -1) === '/') {
//			$url = '/' . substr($pathInfo, 0, -1);
//			if ($query) {
//				$url .= '?' . $query;
//			}
//			Yii::$app->response->redirect($url, 301);
//			Yii::$app->end();
//		}
//	},
    'params' => $params,
];
