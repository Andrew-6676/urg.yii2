<?php
return [
	'confValue'       => 'urg',
	'client_version'  => '0.8.0',       // версия клиента, которому можно работать
	'token_life_time' => 60*60*24*7,     // как долго помнить пользователя
	'company_cache_life_time' => 60*60*24*7,     // как часто обновлять кэш справочника компаний
    'adminEmail'      => 'shavneval@oblgas.by',
	'appShortName'    => 'УРГ',
	'uploads_dir'     => getUploadsDir(),
	'download_dir'    => 'files',
	'defaultPageSize' => 30,

	'user.passwordResetTokenExpire' => 3600,
	'supportEmail'   => 'shavneval@oblgas.by',
	'appFullName'    => 'Узлы редуцирования газа',

	'developer'      => 'Шавнёв А.Л.',
	'myCompany'      => 'АйТиГаз',

	//'company_API' => 'http://192.168.0.204:5001/company/gasb',
	'company_API' => 'http://192.168.101.245/get_company.php',    // для своей области создать такую же строку в params-local.php
	'panorama_Url' => getPanoramaUrl(),                           // для своей области создать такую же строку в params-local.php
];


function getUploadsDir() {
	if (file_exists('/etc/gasbase/uploaddir')) {
		return trim(file_get_contents('/etc/gasbase/uploaddir'));
	} else {
		return '/var/www/scheme';
	}
}

function getPanoramaUrl() {
	if (file_exists('/etc/gasbase/panorama')) {
		return trim(file_get_contents('/etc/gasbase/panorama'));
	} else {
		return 'null';
	}
}